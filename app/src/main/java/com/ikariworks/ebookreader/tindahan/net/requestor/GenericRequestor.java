package com.ikariworks.ebookreader.tindahan.net.requestor;

import android.util.Log;

import com.ikariworks.ebookreader.tindahan.util.Consts;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class GenericRequestor {
	private static AsyncHttpClient client;
	private final static String URL =  Consts.APIURL;
	
	public static AsyncHttpClient getClient() {
		
		return new AsyncHttpClient();
	}
	public void post(String action, RequestParams params, JsonHttpResponseHandler handler) {
		
		
		client = getClient();
		
		client.post(URL, params, handler);
		
	}
	/**
	 * http get
	 * @param action
	 * @param params
	 * @param handler
	 */
public void get(String action, String dataUrl, JsonHttpResponseHandler handler) {
		
		
		client = getClient();
		
		client.get(dataUrl, handler);
		
	}
}
