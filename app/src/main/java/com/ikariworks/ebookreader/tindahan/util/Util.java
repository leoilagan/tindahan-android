package com.ikariworks.ebookreader.tindahan.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class Util {

	private static Util instance;

	public static Util getInstance() {
		if (instance == null) {
			instance = new Util();
		}
		return instance;
	}

	public static GradientDrawable newGradient(int startColor, int endColor) {
		GradientDrawable gd = new GradientDrawable(
				GradientDrawable.Orientation.TOP_BOTTOM, new int[] {
						startColor, endColor });
		// gd.setCornerRadius(0f);
		gd.setStroke(1, startColor);
		// gd.setCornerRadius(3f);
		return gd;
	}

	public static void hideOnScreenKeyboard(Activity context) {
		InputMethodManager inputManager = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				(null == context.getCurrentFocus()) ? null : context
						.getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static boolean warnIfNetworkUnavailable(Context context) {
		try {
			ConnectivityManager manager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (manager != null) {
				NetworkInfo networkInfo = manager.getActiveNetworkInfo();
				boolean connected = networkInfo != null
						&& networkInfo.isConnected();
				if (!connected) {
					Toast.makeText(context, "No Internet Connection",
							Toast.LENGTH_SHORT).show();
				}
				return connected;
			}
			return false;

		} catch (Exception e) {
			return false;
		}

	}

	// reset all stored user info
	public void logout(PreferenceKeys prefs) {
		prefs.setData("registered", false);
		prefs.setData("rnd", "");
		prefs.setData("paypilNo", "");
		prefs.setData("otpKey", "");
	}

	public String getIMEI(Context context) {
		
		String identifier = null;
		TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		if (tm != null)
		      identifier = tm.getDeviceId();
		if (identifier == null || identifier .length() == 0)
		      identifier = Secure.getString(context.getContentResolver(),Secure.ANDROID_ID);
		return identifier;
	}

	
	
	public String[] getAllCountries(){
		  Locale[] locales = Locale.getAvailableLocales();
	        ArrayList<String> countries = new ArrayList<String>();
	        
	        for (Locale locale : locales) {
	            String country = locale.getDisplayCountry();
	            if (country.trim().length()>0 && !countries.contains(country)) {
	                countries.add(country);
	            }
	        }
	        Collections.sort(countries);
	       
	        String[] arryCountries = new String[countries.size()];
	        arryCountries = countries.toArray(arryCountries);
	        return arryCountries;
	}
	
	public boolean makeDirectory(Context context,String dirName) {
		boolean res;		
		String filePath = new String(context.getFilesDir().getAbsolutePath() + "/"+dirName);
		
		File file = new File(filePath);
		if (!file.exists()) {
			res = file.mkdirs();
		}else {
			res = false;		
		}
		return res;	
	}
	
	
	public static String removeExtention(String filePath) {
	    // These first few lines the same as Justin's
	    File f = new File(filePath);

	    // if it's a directory, don't remove the extention
	    if (f.isDirectory()) return filePath;

	    String name = f.getName();

	    // Now we know it's a file - don't need to do any special hidden
	    // checking or contains() checking because of:
	    final int lastPeriodPos = name.lastIndexOf('.');
	    if (lastPeriodPos <= 0)
	    {
	        // No period after first character - return name as it was passed in
	        return filePath;
	    }
	    else
	    {
	        // Remove the last period and everything after it
	        File renamed = new File(f.getParent(), name.substring(0, lastPeriodPos));
	        return renamed.getPath();
	    }
	}

	/*
	public static Bitmap getBitmapFromAsset(Context context, String strName) {
	    AssetManager assetManager = context.getAssets();

	    InputStream istr;
	    Bitmap bitmap = null;
	    try {
	        istr = assetManager.open(strName);
	        bitmap = BitmapFactory.decodeStream(istr);
	    } catch (IOException e) {
	        return null;
	    }

	    return bitmap;
	}
	*/
}
