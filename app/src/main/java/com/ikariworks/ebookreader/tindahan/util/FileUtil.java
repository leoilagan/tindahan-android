package com.ikariworks.ebookreader.tindahan.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;



import java.util.Observable;
import java.util.Observer;

import com.ikariworks.ebookreader.tindahan.db.SQLHelper;
import com.ikariworks.ebookreader.tindahan.skyreader.ContentHandler;
import com.skytree.epub.BookInformation;

import android.content.Context;
import android.util.Log;

public class FileUtil {
	
	String TAG = "Test";
	Context context;
	SQLHelper sqlHelper;
	public FileUtil(Context context) {
		this.context = context;
		sqlHelper = new SQLHelper(context);
	}
	

	
	
	public void installBook(String fileName) {
        this.copyToDevice(fileName);
        this.unzipBook(fileName);
    }
	
	public void copyToDevice(String fileName) {			      
		if (!this.fileExists(fileName)){
	          try
	          {
	        	  InputStream localInputStream =context.getAssets().open(fileName);
	        	  String path = context.getFilesDir().getAbsolutePath() + "/downloads/"+fileName;
	        	  FileOutputStream localFileOutputStream = new FileOutputStream(path);

	        	  byte[] arrayOfByte = new byte[1024];
	        	  int offset;
	        	  while ((offset = localInputStream.read(arrayOfByte))>0)
	        	  {
	        		  localFileOutputStream.write(arrayOfByte, 0, offset);	              
	        	  }
	        	  localFileOutputStream.close();
	        	  localInputStream.close();
	        	  Log.d(TAG, fileName+" copied to phone");	            
	          }
	          catch (IOException localIOException)
	          {
	              localIOException.printStackTrace();
	              Log.d(TAG, "failed to copy");
	              return;
	          }
	      }
	      else {
	          Log.d(TAG, fileName+" already exist");
	      }	         
	}
	
	public boolean  deleteFile(String fileName) {
		boolean res;
		File file = new File(context.getFilesDir() + "/downloads/"+fileName);
		res = file.delete();
		return res;		
	}
	
	public boolean fileExists(String fileName) {
		boolean res;
		File file = new File(context.getFilesDir() + "/downloads/"+fileName);
		//Log.i("Test",file.getAbsolutePath());
		
		if (file.exists()) res = true;
		else  res = false;
		return res;		
	}
	
	public boolean makeDirectory(String dirName) {
		boolean res;		
		String filePath = new String(context.getFilesDir().getAbsolutePath() + "/"+dirName);
	//	Log.i(TAG,filePath);
		File file = new File(filePath);
		if (!file.exists()) {
			res = file.mkdirs();
		}else {
			res = false;		
		}
		return res;	
	}
	public void unzipBook(String fileName) {
	  
		String targetDir = new String(context.getFilesDir().getAbsolutePath() + "/books/" + fileName);
		targetDir = this.removeExtention(targetDir);
		String filePath = new String(context.getFilesDir().getAbsolutePath() + "/downloads/");

	    Unzip unzip = new Unzip(fileName, filePath, targetDir);
	    unzip.addObserver(new UnzipHandler());
	    unzip.unzip();	    
	}
	public String removeExtention(String filePath) {
	    // These first few lines the same as Justin's
	    File f = new File(filePath);

	    // if it's a directory, don't remove the extention
	    if (f.isDirectory()) return filePath;

	    String name = f.getName();

	    // Now we know it's a file - don't need to do any special hidden
	    // checking or contains() checking because of:
	    final int lastPeriodPos = name.lastIndexOf('.');
	    if (lastPeriodPos <= 0)
	    {
	        // No period after first character - return name as it was passed in
	        return filePath;
	    }
	    else
	    {
	        // Remove the last period and everything after it
	        File renamed = new File(f.getParent(), name.substring(0, lastPeriodPos));
	        return renamed.getPath();
	    }
	}
	
	class UnzipHandler implements Observer {
		@Override
		public void update(Observable observable, Object data) {
			String fileName = (String)data;
	        String baseDirectory = context.getFilesDir() + "/books";
	        ContentHandler contentListener = new ContentHandler();
	        BookInformation bi = new BookInformation(fileName,baseDirectory,contentListener);		
	        sqlHelper.insertBook(bi);
	      

		}		
	}
	
	

	
}
