package com.ikariworks.ebookreader.tindahan.ui;

import java.util.Locale;

import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockActivity;
import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.net.command.APIRequestCallBack;
import com.ikariworks.ebookreader.tindahan.net.requestor.APIRequester;
import com.ikariworks.ebookreader.tindahan.util.Consts;
import com.ikariworks.ebookreader.tindahan.util.PreferenceKeys;
import com.ikariworks.ebookreader.tindahan.util.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class LoginActivity extends Activity implements OnClickListener,
		APIRequestCallBack {
	Locale locale = null;
	APIRequester apiRequester;
	// helper class that stores user data in the app
	PreferenceKeys prefs;

	// edittext obj of logn screen username and password
	EditText txtUserName, txtPassword;

	ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		prefs = PreferenceKeys.getInstance(this);
		// set the current set language of the app
		Configuration config = getBaseContext().getResources()
				.getConfiguration();
		String lang = prefs.getData("languagePref");

		if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
			locale = new Locale(lang);
			Locale.setDefault(locale);
			config.locale = locale;
			getResources().updateConfiguration(config,
					getResources().getDisplayMetrics());
		}
		txtPassword = (EditText) findViewById(R.id.txtPassword);
		txtUserName = (EditText) findViewById(R.id.txtUserName);
		// Initialise API Requester
		apiRequester = new APIRequester(this);

		setContentView(R.layout.activity_login);
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.btnRegister:

			// temporarily direct the registration to google site
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse(Consts.registration_url));
			startActivity(browserIntent);
			break;
		case R.id.btnLogin:
			txtPassword = (EditText) findViewById(R.id.txtPassword);
			txtUserName = (EditText) findViewById(R.id.txtUserName);
			String username = txtUserName.getText().toString();
			String password = txtPassword.getText().toString();
			if (username.equals("") || password.equals("")) {
				messageDialog(getResources().getString(
						R.string.dialog_fill_inFields));
			} else {
				// check if app have internet connection
				if (Util.warnIfNetworkUnavailable(this)) {
					apiRequester.login(username, password);
				}
			}

			break;
		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (locale != null) {
			newConfig.locale = locale;
			Locale.setDefault(locale);
			getResources().updateConfiguration(newConfig,
					getResources().getDisplayMetrics());
			setContentView(R.layout.activity_login);
		}
	}

	@Override
	public void onStartRequest(int mode) {
		switch (mode) {
		case Consts.API_MODE_LOGIN:
			showProgressDialog("Logging...");
			break;
		}

	}

	@Override
	public void onFinishRequest(int mode) {
		switch (mode) {
		case Consts.API_MODE_LOGIN:
            dismissDialog();
			break;

		default:
			break;
		}

	}

	@Override
	public void onFailRequest(String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj) {
		try {

			// parse returned json object
			switch (mode) {
			case Consts.API_MODE_LOGIN:
				// {"dateOfBirth":"\/Date(596044800000)\/","gender":0,"result":true,"rnd":
				// "1381639949","paypilNo":"999-999-999","otpKey":"NtNKzRwsgs2qPJFg8SPvE8kG"}
				String result = jsonObj.getString("result");
				if (result.equals("true")) {
					// store data
					prefs.setData("rnd", jsonObj.getString("rnd"));
					prefs.setData("paypilNo", jsonObj.getString("paypilNo"));
					prefs.setData("otpKey", jsonObj.getString("otpKey"));
					// once the app gets the necessary info(e.g paypilNo etc)
					// launch the main screen
					finish();
					Intent intent = new Intent(this, MainActivity.class);
					startActivity(intent);
					prefs.setData("registered", true);
				} else {
					// clear textfields
					txtPassword.setText("");
					txtUserName.setText("");
					messageDialog(getResources().getString(
							R.string.dialog_login_fail));
				}
				break;
			}

			//Log.i("Test", jsonObj.toString());

		} catch (Exception e) {

		}

	}

	private void messageDialog(String message) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.app_name))
				.setMessage(message)
				.setCancelable(false)
				.setNeutralButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();

							}
						});

		AlertDialog alert = builder.create();
		alert.show();

	}

	private void showProgressDialog(String message) {
		progressDialog = ProgressDialog.show(this, "", message, true, false);
	}

	private void dismissDialog() {
		progressDialog.dismiss();
	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj, Bundle bundle) {
		
		
	}

}
