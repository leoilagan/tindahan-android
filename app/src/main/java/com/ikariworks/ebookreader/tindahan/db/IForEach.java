package com.ikariworks.ebookreader.tindahan.db;

public interface IForEach<T, B> {
	public void apply(T cursor, B bis);
}
