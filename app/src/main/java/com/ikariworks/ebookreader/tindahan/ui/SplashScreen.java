package com.ikariworks.ebookreader.tindahan.ui;


import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.util.PreferenceKeys;
import com.ikariworks.ebookreader.tindahan.util.Util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public class SplashScreen extends Activity {

	Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.splashscreen);
		handler = new Handler();
		
		progressToNextActivity();
		
	}

	private final int SPLASH_DELAY = 3000;

	public void progressToNextActivity() {
		Runnable mainMenuRunnable = startActivityRunnableFactory(this,
				MainActivity.class, true);
		Runnable signInRunnable = startActivityRunnableFactory(this,
				IntroActivity.class, true);

		// after a splash delay, go to the logged out selector or current deal
		 if (loggedInUser(this)) {
		handler.postDelayed(mainMenuRunnable, SPLASH_DELAY);
		 } else {
		 handler.postDelayed(signInRunnable, SPLASH_DELAY);
		 }
	}

	public Runnable startActivityRunnableFactory(final Activity ctx,
			final Class goToClass, final boolean clearTop) {
		return new Runnable() {
			public void run() {
				goToActivity(goToClass, clearTop);
			}
		};
	}

	public void goToActivity(final Class goToClass, boolean clearTop) {
		Intent intent = new Intent(this, goToClass);
		if (clearTop) {
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		}
		startActivity(intent);
		if (clearTop) {
			finish();
		}
	}

	public static boolean loggedInUser(Context ctx) {
		//String username = PreferenceKeys.getInstance(ctx).getData("msisdn");
		
		boolean isVerified = PreferenceKeys.getInstance(ctx).getBooleanData("registered");
		return isVerified;
	}

}
