package com.ikariworks.ebookreader.tindahan.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceKeys {

	private static PreferenceKeys instance;
	private String pref_name ="Tindahan";	
	SharedPreferences settings ;
	public PreferenceKeys(Context context) {
		//Context context =	MyApp.getContext();
		settings = context.getSharedPreferences(pref_name, 0);
	}
	
	public static PreferenceKeys getInstance(Context context) {
		if(instance == null){
		   instance = new PreferenceKeys(context);
		}
		return instance;
	}
	
	public void destroyKeys(){
		SharedPreferences.Editor  prefEditor = settings.edit();
		prefEditor.clear();
		prefEditor.commit();
	}
	
	
	public void setData(String key, String value){
		if(settings !=null){
			SharedPreferences.Editor  prefEditor = settings.edit();
			prefEditor.putString(key, value);
			prefEditor.commit();
		}
	}
	
	public void setData(String key, boolean value){
		if(settings !=null){
			SharedPreferences.Editor  prefEditor = settings.edit();
			prefEditor.putBoolean(key, value);
			prefEditor.commit();
		}
	}
	
	public void setData(String key, int value){
		if(settings !=null){
			SharedPreferences.Editor  prefEditor = settings.edit();
			prefEditor.putInt(key, value);
			prefEditor.commit();
		}
	}
	public String getData(String key){
		String value="";
		if(settings != null){
			value = settings.getString(key, "");
		}
		return value;
	}
	
	public String getStringData(String key,String defaultValue){
		String value=defaultValue;
		if(settings != null){
			value = settings.getString(key, defaultValue);
		}
		return value;
	}
	
	public boolean getBooleanData(String key){
		boolean value=false;
		if(settings != null){
			value = settings.getBoolean(key, value);
		}
		return value;
	}
	
	public boolean getBooleanData(String key, boolean defaultValue){
		boolean value=defaultValue;
		if(settings != null){
			value = settings.getBoolean(key, defaultValue);
		}
		return value;
	}
	
	public int getIntData(String key){
		int defValue =0;
		if(settings !=null){
			defValue = settings.getInt(key, defValue);
		}
		return defValue;
	}
	
}
