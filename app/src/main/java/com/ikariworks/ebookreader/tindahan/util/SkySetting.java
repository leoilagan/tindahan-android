package com.ikariworks.ebookreader.tindahan.util;

//!!!!! IMPORTANT !!!!!

//The Advanced Demo sources are not the part of SkyEpub SDK.
//These sources are written only to show how to use SkyEpub SDK.
//The bugs found in this demo do not mean that SkyEpub SDK has the related bugs
//Developers should change and modify this Advanced Demo sources and graphics files for their purposes.
//Any request to add/change/modify the Advanced Demo sources may be refused.
//
//									Written By Heo,Jiung (skytree21@gmail.com)
//											Advanced Demo 	  Copy Left - LGPL 


public class SkySetting {
	public int bookCode;
	public String fontName;
	public int fontSize;
	public int lineSpacing;
	public int foreground;
    public int background;
    public int theme;
    public double brightness;
    public int transitionType;	
}
