package com.ikariworks.ebookreader.tindahan.adapter;

import java.util.ArrayList;

import com.ikariworks.ebookreader.tindahan.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListItemAdapter extends BaseAdapter {

	ArrayList<String> items;
	Context context;
	
	public ListItemAdapter(ArrayList<String> items,Context context) {
		this.context = context;
		this.items = items;
	}
	
	
	@Override
	public int getCount() {
		
		return items.size();
	}

	@Override
	public String getItem(int index) {
		
		return items.get(index);
	}

	@Override
	public long getItemId(int arg0) {
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		View layout = convertView;
		if (layout == null) {
			layout = View.inflate(context, R.layout.list_item, null);
		}
		String string = getItem(position);
		TextView txt = (TextView)layout.findViewById(R.id.lst_item);
		txt.setText(string);
		return layout;
	}

}
