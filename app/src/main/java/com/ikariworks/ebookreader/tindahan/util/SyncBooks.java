package com.ikariworks.ebookreader.tindahan.util;

import java.util.Observable;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;

import com.ikariworks.ebookreader.tindahan.net.command.APIRequestCallBack;
import com.ikariworks.ebookreader.tindahan.net.requestor.APIRequester;

public class SyncBooks extends Observable implements APIRequestCallBack {

	private static SyncBooks instance;
	APIRequester apiRequester;

	public static SyncBooks getInstance() {
		if (instance == null) {
			instance = new SyncBooks();
		}

		return instance;
	}

	public SyncBooks() {
		apiRequester = new APIRequester(this);
	}

	public void syncToDevice(String bookID, String paypilNo, String deviceID,
			String otpKey) {
		apiRequester.syncToDevice(bookID, paypilNo, deviceID, otpKey);
	}

	public void getSyncStatus(String bookID, String paypilNo, String deviceID,
			String otpKey) {
		apiRequester.getSyncStatus(paypilNo, deviceID, otpKey, bookID);
	}

	@Override
	public void onStartRequest(int mode) {

	}

	@Override
	public void onFinishRequest(int mode) {

	}

	@Override
	public void onFailRequest(String message) {

	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj) {

	}
	
	
	public void forceUpdate(){
		//Bundle bundle = new Bundle();
		//bundle.putString("forceUpdate", "forceUpdate");
		setChanged();
		notifyObservers();
	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj, Bundle bundle) {
		try {
			String jsonString = jsonObj.toString();

			JSONArray jsonArray = jsonObj.getJSONArray("data");
			JSONObject jsonObject = jsonArray.getJSONObject(0);
			JSONObject jsonItem = jsonObject.getJSONObject("item");
			// {"data":[{"item":{"status":true}}]}
			switch (mode) {
			/*
			 * case Consts.API_MODE_SYNCTODEVICE: if(jsonItem.has("status")){
			 * boolean status = jsonItem.getBoolean("status"); if(status){
			 * Log.i("Test","status"+status); } } if(jsonItem.has("error")){
			 * String message = jsonItem.getString("error");
			 * Log.i("Test",message); }
			 * 
			 * break;
			 */
			case Consts.API_MODE_GETSYNCSTATUS:
				// {"data":[{"item":{"bookId":2,"url":"","status":20}}]}
				jsonArray = jsonObj.getJSONArray("data");
				jsonObject = jsonArray.getJSONObject(0);
				jsonItem = jsonObject.getJSONObject("item");
				String bookID = jsonItem.getString("bookId");

				String url = jsonItem.getString("url");
				int status = jsonItem.getInt("status");
				Bundle bundleStat = new Bundle();
				bundleStat.putString("bookId", bookID);
				bundleStat.putString("url", url);
				bundleStat.putInt("status", status);

				setChanged();
				notifyObservers(bundleStat);
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
