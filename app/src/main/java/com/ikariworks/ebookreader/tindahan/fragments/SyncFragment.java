package com.ikariworks.ebookreader.tindahan.fragments;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ViewFlipper;

import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.adapter.CloudBookListAdapter;
import com.ikariworks.ebookreader.tindahan.data.Book;
import com.ikariworks.ebookreader.tindahan.db.DBAdapter;
import com.ikariworks.ebookreader.tindahan.db.SQLHelper;
import com.ikariworks.ebookreader.tindahan.managers.BookManager;
import com.ikariworks.ebookreader.tindahan.net.command.APIRequestCallBack;
import com.ikariworks.ebookreader.tindahan.net.requestor.APIRequester;
import com.ikariworks.ebookreader.tindahan.ui.BookViewActivity;
import com.ikariworks.ebookreader.tindahan.ui.MagazineActivity;
import com.ikariworks.ebookreader.tindahan.util.Consts;
import com.ikariworks.ebookreader.tindahan.util.DownloadFileAsync;
import com.ikariworks.ebookreader.tindahan.util.PreferenceKeys;
import com.ikariworks.ebookreader.tindahan.util.SyncBooks;
import com.ikariworks.ebookreader.tindahan.util.Util;
import com.skytree.epub.BookInformation;

public class SyncFragment extends BaseFragment implements
		RadioGroup.OnCheckedChangeListener, APIRequestCallBack, Observer,OnItemClickListener {
	OnFragmentSelectListener mListener;
	View localView;
	ViewFlipper flipper;

	public static String TAG = "SYNCFRAGMENT";
	APIRequester apirequester;
	ArrayList<Book> arryCloudBooks;
	ListView lstCloudBooks,lstSyncedBooks,lstSyncAll;
	String paypilNo;
	String otpKey;
	SQLHelper sqlHelper;
	String deviceID;
	SyncBooks syncBooks;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PreferenceKeys prefs = PreferenceKeys.getInstance(getActivity());
		apirequester = new APIRequester(this);
		sqlHelper = new SQLHelper(getActivity());
		Log.i("Test", "IMEI" + Util.getInstance().getIMEI(getActivity()));

		paypilNo = prefs.getData("paypilNo");
		otpKey = prefs.getData("otpKey");
		deviceID = Util.getInstance().getIMEI(getActivity());
		syncBooks = SyncBooks.getInstance();
		syncBooks.addObserver(this);
	}

	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		localView = inflater.inflate(R.layout.fragment_sync, container, false);
		RadioGroup radioGrp = (RadioGroup) localView
				.findViewById(R.id.pnlSyncedButtons);
		radioGrp.setOnCheckedChangeListener(this);

		lstCloudBooks = (ListView) localView.findViewById(R.id.synclistCloud);
		lstSyncedBooks = (ListView)localView.findViewById(R.id.synclistSynced);
		lstSyncAll =  (ListView)localView.findViewById(R.id.synclistAll);
		lstSyncedBooks.setOnItemClickListener(this);
		lstSyncAll.setOnItemClickListener(this);
		flipper = (ViewFlipper) localView.findViewById(R.id.viewFlipContainer);

		arryCloudBooks = BookManager.getInstance().getAllCloudBooks();
		ArrayList<Book> arrySyncedBooks = BookManager.getInstance().getAllSyncedBooks();
		lstSyncedBooks.setAdapter(new CloudBookListAdapter(
				arrySyncedBooks, getActivity(), paypilNo, otpKey,
				deviceID));
		ArrayList<Book> arryAllBooks = BookManager.getInstance().getArryBooks();
		lstSyncAll.setAdapter(new CloudBookListAdapter(
				arryAllBooks, getActivity(), paypilNo, otpKey,
				deviceID));
		
		
		apirequester.getSyncList(paypilNo, deviceID, otpKey);

		return localView;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.btnSyncAll:
			flipper.setDisplayedChild(0);
			break;
		case R.id.btnSyncCloud:
			flipper.setDisplayedChild(2);
			break;
		case R.id.btnSyncSynced:
			flipper.setDisplayedChild(1);
			break;
		}

	}

	@Override
	public void onStartRequest(int mode) {

	}

	@Override
	public void onFinishRequest(int mode) {

	}

	@Override
	public void onFailRequest(String message) {

	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj) {
		try {
			switch (mode) {
			case Consts.API_MODE_GETSYNCLIST:
				JSONArray jsonArryData = jsonObj.getJSONArray("data");
				int size = jsonArryData.length();
				for (int i = 0; i < size; i++) {
					JSONObject jsonParent = jsonArryData.getJSONObject(i);
					JSONObject jsonItem = jsonParent.getJSONObject("item");
					Book book = new Book();
					book.setId(jsonItem.getString("bookId"));
					book.setTitle(jsonItem.getString("title"));
					book.setAuthor(jsonItem.getString("author"));
					book.setPublisher(jsonItem.getString("publisher"));
					book.setImage(jsonItem.getString("imgUrl"));
					book.setItemType(jsonItem.getString("itemType"));
					book.setMax(jsonItem.getInt("max"));
					JSONArray jsonDevices = jsonItem.getJSONArray("devices");
					for (int j = 0; j < jsonDevices.length(); j++) {
						String deviceID = jsonDevices.getString(j);
						boolean isExisted = book.addDevice(deviceID);
						if (!isExisted) {
							sqlHelper.insertTinBookDevice(book.getId(),
									deviceID);
						}
					}
					boolean isExisted = sqlHelper.insertTinBook(book,
							book.getId());
					if (!isExisted)
						arryCloudBooks.add(book);

					lstCloudBooks.setAdapter(new CloudBookListAdapter(
							arryCloudBooks, getActivity(), paypilNo, otpKey,
							deviceID));
				}

				break;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj, Bundle bundle) {

	}

	@Override
	public void update(Observable observable, Object data) {
		if(data!=null){
		Bundle bundle = (Bundle) data;
		
		int status = bundle.getInt("status");
		String bookID = bundle.getString("bookId");
		String url = bundle.getString("url");
		int isSynced = 0;
		if (url.equals("")) {

			syncBooks.getSyncStatus(bookID, paypilNo, deviceID, otpKey);
			BookManager.getInstance().updateBookSyncStatus(bookID, status, url, false);
		} else {
			isSynced = 1;
			BookManager.getInstance().updateBookSyncStatus(bookID, status, url, true);
			BookManager.getInstance().addBookDevice(bookID, deviceID);
			sqlHelper.insertTinBookDevice(deviceID, bookID);
		
		}
		sqlHelper.updateSyncBook(bookID, status, url, isSynced);
	
		}
		updateList();
	}
	
	
	private void updateList(){
		arryCloudBooks = BookManager.getInstance().getAllCloudBooks();
		lstCloudBooks.setAdapter(new CloudBookListAdapter(
				arryCloudBooks, getActivity(), paypilNo, otpKey,
				deviceID));
		ArrayList<Book> arrySyncedBooks = BookManager.getInstance().getAllSyncedBooks();
		lstSyncedBooks.setAdapter(new CloudBookListAdapter(
				arrySyncedBooks, getActivity(), paypilNo, otpKey,
				deviceID));
		ArrayList<Book> arryAllBooks = BookManager.getInstance().getArryBooks();
		lstSyncAll.setAdapter(new CloudBookListAdapter(
				arryAllBooks, getActivity(), paypilNo, otpKey,
				deviceID));
		
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
	   int id = adapter.getId();
	   switch(id){
	   case R.id.synclistAll:
		   Book mBook = BookManager.getInstance().getArryBooks().get(position);
		   bookViewing(mBook);
		   break;
	   case R.id.synclistSynced:
		   mBook =  BookManager.getInstance().getAllSyncedBooks().get(position);
		   bookViewing(mBook);
		   break;
	   }
		
	}
	
	
	
}
