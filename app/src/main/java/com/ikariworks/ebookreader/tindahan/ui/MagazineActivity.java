package com.ikariworks.ebookreader.tindahan.ui;


import com.ikariworks.ebookreader.tindahan.R;
import com.skytree.epub.CacheListener;
import com.skytree.epub.ClickListener;
import com.skytree.epub.FixedControl;
import com.skytree.epub.MediaOverlayListener;
import com.skytree.epub.PageInformation;
import com.skytree.epub.PageMovedListener;
import com.skytree.epub.PageTransition;
import com.skytree.epub.Parallel;
import com.skytree.epub.Setting;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class MagazineActivity extends Activity {
	RelativeLayout ePubView;
	FixedControl fv;

	SkyLayout mediaBox;
 	ImageButton playAndPauseButton;
 	ImageButton stopButton;
 	ImageButton prevButton;
 	ImageButton nextButton;

	
	Parallel currentParallel;
	boolean autoStartPlayingWhenNewPagesLoaded = true;
	boolean autoMovePageWhenParallesFinished = true;
	boolean isAutoPlaying = true;

	
	private void debug(String msg) {
		Log.w("EPub",msg);
	}
	
    public void onCreate(Bundle savedInstanceState) {
        String fileName = new String();
		Bundle bundle = getIntent().getExtras();
		fileName = bundle.getString("BOOKNAME");
		super.onCreate(savedInstanceState); 		
		
		ePubView = new RelativeLayout(this);
		RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,RelativeLayout.LayoutParams.FILL_PARENT);
		ePubView.setLayoutParams(rlp);
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height        
		fv = new FixedControl(this);
		Bitmap pagesStack = 	BitmapFactory.decodeResource(getResources(), R.drawable.pagesstack);
		Bitmap pagesCenter = 	BitmapFactory.decodeResource(getResources(), R.drawable.pagescenter);	
		fv.setPagesCenterImage(pagesCenter);
		fv.setPagesStackImage(pagesStack);
		fv.setContentListener(new ContentHandler());
		fv.setContentListenerForCache(new ContentHandler());
		fv.setCacheListener(new CacheDelegate());
		fv.setBaseDirectory(getFilesDir() + "/books");
        fv.setBookName(fileName);
        fv.setLayoutParams(params);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params.width =  LayoutParams.FILL_PARENT;	// 400;
        params.height = LayoutParams.FILL_PARENT;	// 600;
        ClickDelegate cd = new ClickDelegate();
        fv.setClickListener(cd);
        PageMovedDelegate pd = new PageMovedDelegate();
        fv.setPageMovedListener(pd);
        fv.setTimeForRendering(1000);
        fv.setNavigationAreaWidthRatio(0.2f);
        fv.setMediaOverlayListener(new MediaOverlayDelegate());
        fv.setSequenceBasedForMediaOverlay(false);

        // If you want to get the license key for commercial or non commercial use, please don't hesitate to email us. (skytree21@gmail.com). 
		// Without the proper license key, watermark message(eg.'unlicensed') may be shown in background. 
		fv.setLicenseKey("0000-0000-0000-0000");	
        
		int transitionType = bundle.getInt("transitionType");
		if (transitionType==0) {
			fv.setPageTransition(PageTransition.None);
		}else if (transitionType==1) {
			fv.setPageTransition(PageTransition.Slide);
		}else if (transitionType==2) {
			fv.setPageTransition(PageTransition.Curl);
		}

        ePubView.addView(fv);

        this.makeMediaBox();
        setContentView(ePubView);
    }
    
    
	private OnClickListener listener=new OnClickListener(){
		public void onClick(View arg) {			
			if (arg.getId() == 8080) {
				playPrev();
			} else if (arg.getId() == 8081) {
				playAndPause();				
			} else if (arg.getId() == 8082) {
				stopPlaying();		
			} else if (arg.getId() == 8083) {
				playNext();
			} else if (arg.getId() == 8084) {
				finish();				
			}	    
		}
	};
	
	class MediaOverlayDelegate implements MediaOverlayListener {
		@Override
		public void onParallelStarted(Parallel parallel) {
			// TODO Auto-generated method stub
			fv.changeElementColor("#FFFF00",parallel.hash,parallel.pageIndex);
			currentParallel = parallel;			
		}

		@Override
		public void onParallelEnded(Parallel parallel) {
			// TODO Auto-generated method stub
			fv.restoreElementColor();			
		}

		@Override
		public void onParallelsEnded() {
			// TODO Auto-generated method stub
		    fv.restoreElementColor();
		    if (autoStartPlayingWhenNewPagesLoaded) isAutoPlaying = true;
		    if (autoMovePageWhenParallesFinished) {
		        fv.gotoNextPage();
		    }
		}		
	}

	
	void playAndPause() {
		if (fv.isPlayingPaused()) {
	        if (!fv.isPlayingStarted()) {
	            fv.playFirstParallel();	        	
	            if (autoStartPlayingWhenNewPagesLoaded) isAutoPlaying = true;
	        }else {
	            fv.resumePlayingParallel();	        	
	            if (autoStartPlayingWhenNewPagesLoaded) isAutoPlaying = true;
	        }        
	    
	    }else {	        
	        fv.pausePlayingParallel();
	        if (autoStartPlayingWhenNewPagesLoaded) isAutoPlaying = false;
	    }
		this.changePlayAndPauseButton();
	}
	
	void stopPlaying() {
//	    [button1 setTitle:@"Play" forState:UIControlStateNormal];
	    fv.stopPlayingParallel();
	    fv.restoreElementColor();
	    if (autoStartPlayingWhenNewPagesLoaded) isAutoPlaying = false;
	    this.changePlayAndPauseButton();
	}
	
	void playPrev() {
	    fv.restoreElementColor();
	    if (currentParallel.parallelIndex==0) {
	        if (autoMovePageWhenParallesFinished) fv.gotoPrevPage();
	    }else {
	        fv.playPrevParallel();
	    }
	}

	void playNext() {
	    fv.restoreElementColor();
		fv.playNextParallel();
	}
	
	public void onStop() {
		super.onStop();
		debug("onStop");
	}
	
	public void onPause() {
		super.onPause();
		debug("onPause");
	}
	
	public void onDestory() {
		super.onDestroy();
		debug("onDestory");		
	}
	
	public void makeMediaBox() {
		mediaBox = new SkyLayout(this);
		setFrame(mediaBox, ps(100), ps(20), ps(270),ps(50));
		
		int bs = ps(48);
		int sb = 30;
		prevButton = this.makeImageButton(9898, "icons/Prev@2x.png", bs,bs);
		setFrame(prevButton,ps(10),ps(5),bs,bs);
		prevButton.setId(8080);
		prevButton.setOnClickListener(listener);
		playAndPauseButton = this.makeImageButton(9898, "icons/Pause@2x.png", bs,bs);
		setFrame(playAndPauseButton,ps(sb)+bs+ps(10),ps(5),bs,bs);
		playAndPauseButton.setId(8081);
		playAndPauseButton.setOnClickListener(listener);
		
		stopButton = this.makeImageButton(9898, "icons/Stop@2x.png", bs,bs);
		setFrame(stopButton,(ps(sb)+bs)*2,ps(5),bs,bs);
		stopButton.setId(8082);
		stopButton.setOnClickListener(listener);
		nextButton = this.makeImageButton(9898, "icons/Next@2x.png", bs,bs);
		setFrame(nextButton,(ps(sb)+bs)*3,ps(5),bs,bs);
		nextButton.setId(8083);
		nextButton.setOnClickListener(listener);
		
		mediaBox.setVisibility(View.INVISIBLE);
		mediaBox.setVisibility(View.GONE);		
		
		mediaBox.addView(prevButton);
		mediaBox.addView(playAndPauseButton);
		mediaBox.addView(stopButton);
		mediaBox.addView(nextButton);
		this.ePubView.addView(mediaBox);		
	}
	
	public boolean isPortrait() {
		int orientation = getResources().getConfiguration().orientation;
		if (orientation==Configuration.ORIENTATION_PORTRAIT) return true;
		else return false;	
	}
	
	public void hideMediaBox() {
		if (mediaBox!=null) {
			mediaBox.setVisibility(View.INVISIBLE);
			mediaBox.setVisibility(View.GONE);
		}
	}
	
	public void showMediaBox() {
		if (this.isPortrait()) {
			setFrame(mediaBox, ps(50), ps(120), ps(270),ps(50));
		}else {
			setFrame(mediaBox, ps(110), ps(20), ps(270),ps(50));
		}
		mediaBox.setVisibility(View.VISIBLE);
	}
	
	private void changePlayAndPauseButton() {
		Drawable icon;
		String imageName = "";
		if (!fv.isPlayingStarted() || fv.isPlayingPaused()) {
			imageName = "icons/Play@2x.png";
		}else {
			imageName = "icons/Pause@2x.png";
		}
		int bs = ps(48);
		icon = this.getDrawableFromAssets(imageName);
		icon.setBounds(0,0,bs,bs);
		Bitmap iconBitmap = ((BitmapDrawable)icon).getBitmap();
		Bitmap bitmapResized = Bitmap.createScaledBitmap(iconBitmap, bs, bs, false);
		playAndPauseButton.setImageBitmap(bitmapResized);
	}

	
	class PageMovedDelegate implements PageMovedListener {
		public void onPageMoved(PageInformation pi) {
			String msg = String.format("pn:%d/tn:%d ps:%f",pi.pageIndex,pi.numberOfPagesInChapter,pi.pagePositionInBook);
			Setting.debug(msg);			
	        if (fv.isMediaOverlayAvailable()) {
//	            showMediaUI();
	        	showMediaBox();
	            if (isAutoPlaying) {
	                fv.playFirstParallel();
	            }
	        }else {
//	            hideMediaUI();
	        	hideMediaBox();
	        }
		}
		
		public void onChapterLoaded(int chapterIndex) {
			// do nothing in FixedLayout. 
		}
	}
	
	public void setFrame(View view,int dx, int dy, int width, int height) {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		param.leftMargin = dx;
		param.topMargin =  dy;
		param.width = width;
		param.height = height;
		view.setLayoutParams(param);	
	}

	
	public int getDensityDPI() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int density = metrics.densityDpi;
		return density;
	}
	
	public int getPS(float dip) {
		float densityDPI = this.getDensityDPI();
		int px = (int)(dip*(densityDPI/240));
		return px;		
//		int px = (int)(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, getResources().getDisplayMetrics()));
//		return px;
	}
	
	public int getPXFromLeft(float dip) {
		int ps = this.getPS(dip);
		return ps;		
	}
	
	public int getPXFromRight(float dip) {
		int ps = this.getPS(dip);
		int ms = this.getWidth()-ps;
		return ms;
	}
	
	public int getPYFromTop(float dip) {
		int ps = this.getPS(dip);
		return ps;
	}
	
	public int getPYFromBottom(float dip) {
		int ps = this.getPS(dip);
		int ms = this.getHeight()-ps;
		return ms;
	}
	
	public int pxl(float dp) {
		return this.getPXFromLeft(dp);		
	}
	
	public int pxr(float dp) {
		return this.getPXFromRight(dp);
	}
	
	public int pyt(float dp) {
		return this.getPYFromTop(dp);
	}
	
	public int pyb(float dp) {
		return this.getPYFromBottom(dp);
	}
	
	public int ps(float dp) {
		return this.getPS(dp);
	}
	
	public int pw(float sdp) {
		int ps = this.getPS(sdp*2);
		int ms = this.getWidth()-ps;
		return ms;
	}
	
	public int cx(float dp) {
		int ps = this.getPS(dp);
		int ms = this.getWidth()/2-ps/2;
		return ms;		
	}
	
	public int getWidth() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int width = metrics.widthPixels;
		return width;		
	}
	
	public int getHeight() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int height = metrics.heightPixels;
		return height;			
	}
	
	public ImageButton makeImageButton(int id,String imageName,int width, int height) {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		Drawable icon;
		ImageButton button = new ImageButton(this);	
		button.setId(id);
		button.setOnClickListener(listener);
		button.setBackgroundColor(Color.TRANSPARENT);
		icon = this.getDrawableFromAssets(imageName);
		icon.setBounds(0,0,width,height);
		Bitmap iconBitmap = ((BitmapDrawable)icon).getBitmap();
		Bitmap bitmapResized = Bitmap.createScaledBitmap(iconBitmap, width, height, false);
		button.setImageBitmap(bitmapResized);
		button.setVisibility(View.VISIBLE);
		param.width = 		width;		
		param.height = 		height;
		button.setLayoutParams(param);		
		button.setOnTouchListener(new ImageButtonHighlighterOnTouchListener(button));
		return button;		
	}
	
	Drawable getDrawableFromAssets(String name) {
		try {
//			InputStream ims = getResources().getAssets().open(name);
//			Drawable d = Drawable.createFromStream(ims, null);
			Drawable d = Drawable.createFromStream(getAssets().open(name), null);
			return d;
		}catch(Exception e) {
			return null;
		}	
	}
	
	class ImageButtonHighlighterOnTouchListener implements OnTouchListener {
		  final ImageButton button;

		  public ImageButtonHighlighterOnTouchListener(final ImageButton button) {
		    super();
		    this.button = button;
		  }
		  
		  @Override
		  public boolean onTouch(final View view, final MotionEvent motionEvent) {
			  if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
				  button.setColorFilter(Color.argb(155, 220, 220, 220));
			  } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
				  button.setColorFilter(Color.argb(0, 185, 185, 185));
			  }
		    return false;
		  }
	}
}


class CacheDelegate implements CacheListener {
	public void onCachingStarted(int index) {
		Setting.debug("Caching task started.");
	}
	public void onCachingFinished(int index) {
		Setting.debug("Cacing task ended");
	}
	public void onCached(int index,String path) {
		Setting.debug("PageIndex "+index+" is cached to "+path);
	}
}


class ClickDelegate implements ClickListener {
	public void onClick(int x,int y) {
		Setting.debug("Click detected at "+x+":"+y);
	}
	public void onImageClicked(int x,int y,String src) {}
	public void onLinkClicked(int x,int y,String href) {}
}