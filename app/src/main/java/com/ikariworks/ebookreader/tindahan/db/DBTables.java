package com.ikariworks.ebookreader.tindahan.db;

public class DBTables {
	public static final int BOOK = 1;
	public static final int DEVICES =2;
	public static final int BOOKDEVICES =3;


	// tin is shortcut for tindahan
	public static final String BOOK_TABLE = "tin_book";
	public static final String DEVICE_TABLE = "tin_device";
	public static final String BOOKDEVICE_TABLE = "tin_bookdevice";
	
	
	
	//SKY EPUB TABLES VARIABLES START HERE............
	public static final String SKYEPUB_BOOK_TABLE ="Book";
	public static final String SKYEPUB_BOOKMARK_TABLE="Bookmark";
	public static final String SKYEPUB_HIGHLIGHT_TABLE="Highlight";
	public static final String SKYEPUB_PAGING_TABLE="Paging";
	public static final String SKYEPUB_SETTING_TABLE="Setting";
	//SKY EPUB TABLES VARIABLES ENDS..............
	
	
	

	public static final String CREATE_BOOK_TABLE = "CREATE TABLE IF NOT EXISTS " + BOOK_TABLE
			+ "(" + "_id integer primary key autoincrement,"
			+ "bookId text not null," 
			+ "title text not null,"
			+"author text not null,"
			+"publisher text not null,"
			+"imgUrl text not null,"
			+"itemType text,"
			+"max integer default 0,"
			+"syncStatus integer default 0,"
			+"isSynced integer default 0,"
			+"isSaved integer default 0,"
			+"bookUrl integer,"
			+"fileName text"
			+ ");";
	

	public static final String CREATE_DEVICE_TABLE = "CREATE TABLE IF NOT EXISTS " + DEVICE_TABLE
			+ "(" + "_id integer primary key autoincrement,"
			+ "deviceID text not null);";
			
	public static final String CREATE_BOOKDEVICE_TABLE = "CREATE TABLE IF NOT EXISTS " + BOOKDEVICE_TABLE
			+ "(" + "_id integer primary key autoincrement,"
			+ "deviceID text not null,bookId text not null,"
			+ "FOREIGN KEY(deviceID) REFERENCES "+DEVICE_TABLE+"(deviceID),"
			+ "FOREIGN KEY(bookId) REFERENCES "+BOOK_TABLE+"(bookId) );";
	
	
	

	
	//SKY EPUB SQL STATEMENTS START HERE
	public static final String CREATE_SKYEPUB_BOOK =
			"CREATE TABLE IF NOT EXISTS " +SKYEPUB_BOOK_TABLE+"("
			+ "BookCode INTEGER UNIQUE NOT NULL PRIMARY KEY AUTOINCREMENT,"
			+ "Title TEXT,"
			+ "Author TEXT,"
			+ "Publisher TEXT,"
			+ "Subject TEXT,"
			+ "Type INTEGER,"
			+ "Date TEXT,"
			+ "Language TEXT,"
			+ "FileName TEXT,"
			+ "Position REAL DEFAULT 0,"
			+ "IsFixedLayout INTEGER DEFAULT 0,"
			+ "IsGlobalPagination INTEGER DEFAULT 0"
			+ ");";
	public static final String CREATE_SKYEPUB_BOOKMARK=
			"CREATE TABLE IF NOT EXISTS "+SKYEPUB_BOOKMARK_TABLE+ "("
			+ "BookCode INTEGER NOT NULL,"
			+ "Code INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT NOT NULL,"
			+ "ChapterIndex INTEGER,"
			+ "PagePositionInChapter REAL,"
			+ "PagePositionInBook REAL,"
			+ "Description TEXT,"
			+ "CreatedDate TEXT"
			+ ");";

	public static final String CREATE_SKYEPUB_HIGHLIGHT=
			"CREATE TABLE IF NOT EXISTS "+SKYEPUB_HIGHLIGHT_TABLE+ "("
			+ "BookCode INTEGER NOT NULL,"
			+ "Code INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT NOT NULL,"
			+ "ChapterIndex INTEGER,"
			+ "StartIndex INTEGER,"
			+ "StartOffset INTEGER,"
			+ "EndIndex INTEGER,"
			+ "EndOffset INTEGER,"
			+ "Color INTEGER,"
			+ "Text TEXT,"
			+ "Note TEXT,"
			+ "IsNote INTEGER DEFAULT 0,"
			+ "CreatedDate TEXT"
			+ ");";

	public static final String CREATE_SKYEPUB_PAGING=
			"CREATE TABLE IF NOT EXISTS "+SKYEPUB_PAGING_TABLE+ "("
			+ "BookCode INTEGER NOT NULL,"
			+ "Code INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT NOT NULL,"
			+ "ChapterIndex INTEGER,"
			+ "NumberOfPagesInChapter INTEGER,"
			+ "FontName TEXT,"
			+ "FontSize INTEGER,"
			+ "LineSpacing INTEGER,"
			+ "VerticalGapRatio REAL,"
			+ "HorizontalGapRatio REAL,"
			+ "IsPortrait INTEGER,"
			+ "IsDoublePagedForLandscape INTEGER);";
	
	public static final String CREATE_SKYEPUB_SETTING=
			"CREATE TABLE IF NOT EXISTS "+SKYEPUB_SETTING_TABLE+"("
			+ "BookCode INTEGER NOT NULL,"
			+ "FontName TEXT         \"TimesRoman\","
			+ "FontSize INTEGER      DEFAULT  3,"
			+ "LineSpacing INTEGER   DEFAULT -1,"
			+ "Foreground INTEGER    DEFAULT -1,"
			+ "Background INTEGER    DEFAULT -1,"
			+ "Theme INTEGER         DEFAULT  0,"
			+ "Brightness REAL       DEFAULT  0,"
			+ "TransitionType INTEGER DEFAULT 0);";

	
	public static final String CLEAR_TINBOOK_TABLE ="DELETE FROM "+BOOK_TABLE+";";
	public static final String CLEAR_BOOKDEVICE_TABLE ="DELETE FROM "+ BOOKDEVICE_TABLE+";";
	public static final String CLEAR_SKYEPUB_BOOK_TABLE = "DELETE FROM "+SKYEPUB_BOOK_TABLE+";";
	public static final String CLEAR_SKYEPUB_BOOKMARK_TABLE="DELETE FROM "+SKYEPUB_BOOKMARK_TABLE+";";
	public static final String CLEAR_SKYEPUB_HIGHLIGHT_TABLE="DELETE FROM "+SKYEPUB_HIGHLIGHT_TABLE+";";
	public static final String CLEAR_SKYEPUB_PAGING_TABLE ="DELETE FROM "+SKYEPUB_PAGING_TABLE+";";
	public static final String CLEAR_SKYEPUB_SETTING_TABLE ="DELETE FROM "+SKYEPUB_SETTING_TABLE+";";
//	public static final String SKYEPUB_BOOK_TABLE ="Book";
	//public static final String SKYEPUB_BOOKMARK_TABLE="Bookmark";
	//public static final String SKYEPUB_HIGHLIGHT_TABLE="Highlight";
	//public static final String SKYEPUB_PAGING_TABLE="Paging";
	//public static final String SKYEPUB_SETTING_TABLE="Setting";
	
	//SKY EPUB SQL STATEMENTS ENDS HERE
	
	

}