package com.ikariworks.ebookreader.tindahan.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;

import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.db.SQLHelper;
import com.ikariworks.ebookreader.tindahan.util.SkySetting;
import com.skytree.epub.BookmarkListener;
import com.skytree.epub.ClickListener;
import com.skytree.epub.ContentListener;
import com.skytree.epub.Highlight;
import com.skytree.epub.HighlightListener;
import com.skytree.epub.Highlights;
import com.skytree.epub.MediaOverlayListener;
import com.skytree.epub.NavPoint;
import com.skytree.epub.NavPoints;
import com.skytree.epub.PageInformation;
import com.skytree.epub.PageMovedListener;
import com.skytree.epub.PageTransition;
import com.skytree.epub.PagingInformation;
import com.skytree.epub.PagingListener;
import com.skytree.epub.Parallel;
import com.skytree.epub.SearchListener;
import com.skytree.epub.SearchResult;
import com.skytree.epub.SelectionListener;
import com.skytree.epub.Setting;
import com.skytree.epub.StateListener;
import com.skytree.epub.State;

import android.annotation.SuppressLint;

import com.skytree.epub.ReflowableControl;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;

public class BookViewActivity extends Activity {
	ReflowableControl rv;
	RelativeLayout ePubView;
	Button debugButton0;
	Button debugButton1;
	Button debugButton2;
	Button debugButton3;

	ImageButton rotationButton;
	ImageButton listButton;
	ImageButton fontButton;
	ImageButton searchButton;
	Rect bookmarkRect;
	Rect bookmarkedRect;

	boolean isRotationLocked;

	TextView titleLabel;
	TextView authorLabel;
	TextView pageIndexLabel;
	TextView secondaryIndexLabel;

	SeekBar seekBar;
	OnSeekBarChangeListener seekListener;
	SkyBox seekBox;
	TextView seekLabel;

	SkyBox menuBox;
	Button highlightMenuButton;
	Button noteMenuButton;
	Rect boxFrame;

	SkyBox highlightBox;
	ImageButton colorButtonInHighlightBox;
	ImageButton trashButtonInHighlightBox;
	ImageButton noteButtonInHighlightBox;
	ImageButton shareButtonInHighlightBox;

	SkyBox colorBox;

	SkyBox noteBox;
	EditText noteEditor;
	int noteBoxWidth;
	int noteBoxHeight;

	SkyBox searchBox;
	EditText searchEditor;
	ScrollView searchScrollView;
	LinearLayout searchResultView;

	SkyBox fontBox;
	SeekBar brightBar;
	Button increaseButton;
	Button decreaseButton;
	String fontNames[] = { "Book Fonts", "Sans Serif", "Serif", "Monospace" };
	LinearLayout fontListView;

	SkyLayout listBox;
	Button contentListButton;
	Button bookmarkListButton;
	Button highlightListButton;
	ScrollView listScrollView;
	LinearLayout listView;
	Button listTopButton;

	SkyLayout mediaBox;
	ImageButton playAndPauseButton;
	ImageButton stopButton;
	ImageButton prevButton;
	ImageButton nextButton;

	ArrayList<SearchResult> searchResults = new ArrayList<SearchResult>();

	boolean isBoxesShown;

	SkySetting setting;

	Button outsideButton;

	String fileName;
	String author;
	String title;

	ProgressBar progressBar;

	int currentColor = this.getColorByIndex(0);
	Highlight currentHighlight;

	boolean isControlsShown = true;
	double pagePositionInBook = 0;
	int bookCode;

	Parallel currentParallel;
	boolean autoStartPlayingWhenNewPagesLoaded = true;
	boolean autoMoveChapterWhenParallesFinished = true;
	boolean isAutoPlaying = true;

	boolean isDoublePagedForLandscape;

	final private String TAG = "EPub";

	public ProgressDialog progressViewer;
	Highlights highlights;
	ArrayList<PagingInformation> pagings = new ArrayList<PagingInformation>();
	int temp = 20;

	public int getDensityDPI() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int density = metrics.densityDpi;
		return density;
	}

	public void reportMetrics() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		log("densityDPI" + metrics.densityDpi);
		log("density" + metrics.density);
		log("real width  pixels" + metrics.widthPixels);
		log("real height pixels" + metrics.heightPixels);
		log("inch for width " + metrics.widthPixels / metrics.densityDpi);
		log("inch for height" + metrics.heightPixels / metrics.densityDpi);
	}

	public void reportFiles(String path) {
		Log.d("EPub", "Path: " + path);
		File f = new File(path);
		File file[] = f.listFiles();
		Log.d("EPub", "Size: " + file.length);
		for (int i = 0; i < file.length; i++) {
			Log.d("EPub", "FileName:" + file[i].getName());
		}
	}

	public boolean isHighDensityPhone() { // if HIGH density (not XHIGH) phone
											// like Galaxy S2, retuns true;
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int p0 = metrics.heightPixels;
		int p1 = metrics.widthPixels;
		int max = Math.max(p0, p1);
		if (metrics.densityDpi == 240 && max == 800) {
			return true;
		} else {
			return false;
		}
	}

	public int getPS(float dip) {
		float densityDPI = this.getDensityDPI();
		int px = (int) (dip * (densityDPI / 240));
		return px;
		// int px = (int)(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
		// 14, getResources().getDisplayMetrics()));
		// return px;
	}

	public int getPXFromLeft(float dip) {
		int ps = this.getPS(dip);
		return ps;
	}

	public int getPXFromRight(float dip) {
		int ps = this.getPS(dip);
		int ms = this.getWidth() - ps;
		return ms;
	}

	public int getPYFromTop(float dip) {
		int ps = this.getPS(dip);
		return ps;
	}

	public int getPYFromBottom(float dip) {
		int ps = this.getPS(dip);
		int ms = this.getHeight() - ps;
		return ms;
	}

	public int pxl(float dp) {
		return this.getPXFromLeft(dp);
	}

	public int pxr(float dp) {
		return this.getPXFromRight(dp);
	}

	public int pyt(float dp) {
		return this.getPYFromTop(dp);
	}

	public int pyb(float dp) {
		return this.getPYFromBottom(dp);
	}

	public int ps(float dp) {
		return this.getPS(dp);
	}

	public int pw(float sdp) {
		int ps = this.getPS(sdp * 2);
		int ms = this.getWidth() - ps;
		return ms;
	}

	public int cx(float dp) {
		int ps = this.getPS(dp);
		int ms = this.getWidth() / 2 - ps / 2;
		return ms;
	}

	// in double paged and landscape mode,get the center of view(its width is
	// dpWidth) on left page
	public int lcx(float dpWidth) {
		int ps = this.getPS(dpWidth);
		int ms = this.getWidth() / 4 - ps / 2;
		return ms;
	}

	// in double paged and landscape mode,get the center of view(its width is
	// dpWidth) on right page
	public int rcx(float dpWidth) {
		int ps = this.getPS(dpWidth);
		int ms = this.getWidth() / 2 + this.getWidth() / 4 - ps / 2;
		return ms;
	}

	public float getDIP(float px) {
		float densityDPI = this.getDensityDPI();
		float dip = px / (densityDPI / 240);
		return dip;
	}

	public int getDarkerColor(int color) {
		float[] hsv = new float[3];
		Color.colorToHSV(color, hsv);
		hsv[2] *= 0.8f; // value component
		int darker = Color.HSVToColor(hsv);
		return darker;
	}

	public void loadBooks(ReflowableControl rv) {
		rv.setBaseDirectory(getFilesDir() + "/books");
	}

	RelativeLayout.LayoutParams params;
	RelativeLayout.LayoutParams rlp;

	public void makeLayout() {
		this.setBrightness((float) setting.brightness);

		highlights = new Highlights();
		Bundle bundle = getIntent().getExtras();
		fileName = bundle.getString("BOOKNAME");
		author = bundle.getString("AUTHOR");
		title = bundle.getString("TITLE");
		pagePositionInBook = bundle.getDouble("POSITION");
		bookCode = bundle.getInt("BOOKCODE");

		ePubView = new RelativeLayout(this);
		rlp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.FILL_PARENT,
				RelativeLayout.LayoutParams.FILL_PARENT);
		ePubView.setLayoutParams(rlp);

		params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.FILL_PARENT,
				RelativeLayout.LayoutParams.FILL_PARENT); // width,height
		rv = new ReflowableControl(this); // in case that device supports
											// transparent webkit, the
											// background image under the
											// content can be shown. in some
											// devices, content may be
											// overlapped.
		// rv = new ReflowableControl(this,Color.WHITE); // in case that device
		// can not support transparent webkit, the background color will be set
		// in one color.

		rv.bookCode = this.bookCode;

		Bitmap pagesStack = BitmapFactory.decodeResource(getResources(),
				R.drawable.pagesstack);
		Bitmap pagesCenter = BitmapFactory.decodeResource(getResources(),
				R.drawable.pagescenter);
		rv.setPagesStackImage(pagesStack);
		rv.setPagesCenterImage(pagesCenter);
		/*
		Bitmap backgroundForLandscape;
		Bitmap backgroundForPortrait;
		
		if (this.isTablet()) {
			backgroundForLandscape = BitmapFactory.decodeResource(
					getResources(), R.drawable.pad_landscape);
			backgroundForPortrait = BitmapFactory.decodeResource(
					getResources(), R.drawable.pad_portrait);
			rv.setBackgroundForLandscape(backgroundForLandscape, new Rect(56,
					12, 2048 - 56, 1536 - 12)); // Android Rect -
												// left,top,right,bottom
			rv.setBackgroundForPortrait(backgroundForPortrait, new Rect(0, 12,
					1024 - 56, 1496 - 12)); // Android Rect -
											// left,top,right,bottom
		} else {
			backgroundForLandscape = BitmapFactory.decodeResource(
					getResources(), R.drawable.phone_landscape_double);
			backgroundForPortrait = BitmapFactory.decodeResource(
					getResources(), R.drawable.phone_portrait);

			rv.setBackgroundForLandscape(backgroundForLandscape, new Rect(32,
					0, 2004 - 32, 1506)); // Android Rect -
											// left,top,right,bottom
			rv.setBackgroundForPortrait(backgroundForPortrait, new Rect(0, 0,
					1002 - 32, 1506)); // Android Rect - left,top,right,bottom
		}
		*/

		rv.setBookName(fileName);
		ContentHandler contentListener = new ContentHandler();
		rv.setContentListener(contentListener);

		this.isDoublePagedForLandscape = true;
		rv.setDoublePagedForLandscape(this.isDoublePagedForLandscape);
		rv.setFont(setting.fontName, this.getRealFontSize(setting.fontSize));
		rv.setLineSpacing(setting.lineSpacing); // the value is supposed to be
												// percent(%).
		rv.setHorizontalGapRatio(0.30);
		rv.setVerticalGapRatio(0.22);
		rv.setHighlightListener(new HighlightDelegate());
		rv.setPageMovedListener(new PageMovedDelegate());
		rv.setSelectionListener(new SelectionDelegate());
		rv.setPagingListener(new PagingDelegate());
		rv.setSearchListener(new SearchDelegate());
		rv.setStateListener(new StateDelegate());
		rv.setClickListener(new ClickDelegate());
		rv.setBookmarkListener(new BookmarkDelegate());

		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		params.width = LayoutParams.FILL_PARENT; // 400;
		params.height = LayoutParams.FILL_PARENT; // 600;
		rv.setStartPositionInBook(pagePositionInBook);
		rv.useDOMForHighlight(false);
		rv.setGlobalPaging(false);
		rv.setNavigationAreaWidthRatio(0.2f); // both left and right side.
		rv.setRotationLocked(true);

		isRotationLocked = true;
		rv.setMediaOverlayListener(new MediaOverlayDelegate());
		rv.setSequenceBasedForMediaOverlay(false);

		// If you want to get the license key for commercial or non commercial
		// use, please email us (skytree21@gmail.com).
		// Without the proper license key, watermark message will be shown in
		// background.
	  //	rv.setLicenseKey("0000-0000-0000-0000");

		int transitionType = bundle.getInt("transitionType");
		if (transitionType == 0) {
			rv.setPageTransition(PageTransition.None);
		} else if (transitionType == 1) {
			rv.setPageTransition(PageTransition.Slide);
		} else if (transitionType == 2) {
			rv.setPageTransition(PageTransition.Curl);
		}

		new BookViewer(getFilesDir() + "/books").execute(rv);

		 rv.setCurlQuality(0.25f);

		ePubView.addView(rv);

		this.makeControls();
		this.makeBoxes();
		this.makeIndicator();
		this.recalcFrames();

		setContentView(ePubView);
	}

	public void makeLayout1() {
		this.setBrightness((float) setting.brightness);

		highlights = new Highlights();
		Bundle bundle = getIntent().getExtras();
		fileName = bundle.getString("BOOKNAME");
		author = bundle.getString("AUTHOR");
		title = bundle.getString("TITLE");
		pagePositionInBook = bundle.getDouble("POSITION");
		bookCode = bundle.getInt("BOOKCODE");
		// Create ReflowableControl Object
		rv = new ReflowableControl(this);
		rv.bookCode = this.bookCode;

		// Read PagesStack and save it to Bitmap.
		Bitmap pagesStack = BitmapFactory.decodeResource(getResources(),
				R.drawable.pagesstack);
		// Read PagesCenter and save it to Bitmap.
		Bitmap pagesCenter = BitmapFactory.decodeResource(getResources(),
				R.drawable.pagescenter);
		// Register pageStack to ReflowableControl
		rv.setPagesStackImage(pagesStack);
		// Register pageCenter to ReflowableControl
		rv.setPagesCenterImage(pagesCenter);

		// set ReflowableLayout background
		//Bitmap backgroundForLandscape;
		//Bitmap backgroundForPortrait;
		/*
		if (this.isTablet()) {
			backgroundForLandscape = BitmapFactory.decodeResource(
					getResources(), R.drawable.pad_landscape);
			backgroundForPortrait = BitmapFactory.decodeResource(
					getResources(), R.drawable.pad_portrait);
			rv.setBackgroundForLandscape(backgroundForLandscape, new Rect(56,
					12, 2048 - 56, 1536 - 12)); // Android Rect -
												// left,top,right,bottom
			rv.setBackgroundForPortrait(backgroundForPortrait, new Rect(0, 12,
					1024 - 56, 1496 - 12)); // Android Rect -
											// left,top,right,bottom
		} else {
			backgroundForLandscape = BitmapFactory.decodeResource(
					getResources(), R.drawable.phone_landscape_double);
			backgroundForPortrait = BitmapFactory.decodeResource(
					getResources(), R.drawable.phone_portrait);

			rv.setBackgroundForLandscape(backgroundForLandscape, new Rect(32,
					0, 2004 - 32, 1506)); // Android Rect -
											// left,top,right,bottom
			rv.setBackgroundForPortrait(backgroundForPortrait, new Rect(0, 0,
					getWidth() - 32, getHeight())); // Android Rect - left,top,right,bottom
		}
		*/
		// set base directory
		rv.setBaseDirectory(getFilesDir() + "/books");

		// set the epub file name.
		rv.setBookName(fileName);
		// set two pages mode(double paged mode)when landscape when landscape
		// view.
		this.isDoublePagedForLandscape = true;
		rv.setDoublePagedForLandscape(this.isDoublePagedForLandscape);

		// set default font
		rv.setFont(setting.fontName, this.getRealFontSize(setting.fontSize));

		// set line spacing
		rv.setLineSpacing(setting.lineSpacing); // the value is supposed to be
												// percent(%).

		
		
		// set the left and right margins. 30% of screen width.
		rv.setHorizontalGapRatio(0.30);
		// set the top and bottom margins. 22% of screen height.
		rv.setVerticalGapRatio(0.25);

		// set the Listener for Highlight processing.
		rv.setHighlightListener(new HighlightDelegate());

		// set the Listener for Page Moving.
		rv.setPageMovedListener(new PageMovedDelegate());

		// set the Listener for text processing
		rv.setSelectionListener(new SelectionDelegate());

		rv.setPagingListener(new PagingDelegate());

		// set the Listener for text searching
		rv.setSearchListener(new SearchDelegate());
		// set the Listener to detect the change of state
		rv.setStateListener(new StateDelegate());

		// set the Content processing listener.
		ContentHandler contentListener = new ContentHandler();
		rv.setContentListener(contentListener);

		rv.setClickListener(new ClickDelegate());
		rv.setBookmarkListener(new BookmarkDelegate());
		
		rv.setStartPositionInBook(pagePositionInBook);
		rv.useDOMForHighlight(false);
		rv.setGlobalPaging(false);
		rv.setNavigationAreaWidthRatio(0.2f); // both left and right side.
		

		
		rv.setRotationLocked(isRotationLocked);
		rv.setMediaOverlayListener(new MediaOverlayDelegate());
		rv.setSequenceBasedForMediaOverlay(false);

		// If you want to get the license key for commercial or non commercial
		// use, please email us (skytree21@gmail.com).
		// Without the proper license key, watermark message will be shown in
		// background.
		rv.setLicenseKey("0000-0000-0000-0000");

		int transitionType = bundle.getInt("transitionType");
		if (transitionType == 0) {
			rv.setPageTransition(PageTransition.None);
		} else if (transitionType == 1) {
			rv.setPageTransition(PageTransition.Slide);
		} else if (transitionType == 2) {
			rv.setPageTransition(PageTransition.Curl);
		}
		
		 // Create Layout Params for ReflowableControl
	    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
	                RelativeLayout.LayoutParams.FILL_PARENT,
	                RelativeLayout.LayoutParams.FILL_PARENT);
	    params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
	    params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
	    params.width = LayoutParams.FILL_PARENT; 
	    params.height =LayoutParams.FILL_PARENT;   
	    rv.setLayoutParams(params);
	    

	    // Create RelativeLayout for ContentView.   
	    ePubView = new RelativeLayout(this);
	    RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
	                RelativeLayout.LayoutParams.MATCH_PARENT,
	                RelativeLayout.LayoutParams.MATCH_PARENT);
	    ePubView.setLayoutParams(rlp);
	    // insert RelativeVie into ContentView.
	    ePubView.addView(rv);

		this.makeControls();
	    this.makeBoxes();
		this.makeIndicator();
		this.recalcFrames();
		  //  Specify ePubView including ReflowableView and mark Button as a ContentView of Activity. 
	    setContentView(ePubView);

	}

	public int getColorWithAlpha(int color, int alpha) {
		int red, green, blue;
		red = Color.red(color);
		green = Color.green(color);
		blue = Color.blue(color);
		int newColor = Color.argb(alpha, red, green, blue);
		return newColor;
	}

	public int getBrighterColor(int color) {
		float[] hsv = new float[3];
		Color.colorToHSV(color, hsv);
		hsv[2] *= 1.2f; // value component
		int darker = Color.HSVToColor(hsv);
		return darker;
	}

	public void makeIndicator() {
		progressBar = new ProgressBar(this, null,
				android.R.attr.progressBarStyle);
		ePubView.addView(progressBar);
		this.hideIndicator();
	}

	public void showIndicator() {
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) progressBar
				.getLayoutParams();
		params.addRule(RelativeLayout.CENTER_IN_PARENT, -1);
		progressBar.setLayoutParams(params);
		progressBar.setVisibility(View.VISIBLE);
	}

	public void hideIndicator() {
		if (progressBar != null) {
			progressBar.setVisibility(View.INVISIBLE);
			progressBar.setVisibility(View.GONE);
		}
	}

	public void makeBoxes() {
		this.makeOutsideButton();
		this.makeSeekBox();
		this.makeMenuBox();
		this.makeHighlightBox();
		this.makeColorBox();
		this.makeNoteBox();
		this.makeSearchBox();
		this.makeFontBox();
		this.makeListBox();
		this.makeMediaBox();
	}

	public void makeOutsideButton() {
		outsideButton = new Button(this);
		outsideButton.setId(9999);
		outsideButton.setBackgroundColor(Color.TRANSPARENT);
		outsideButton.setOnClickListener(listener);
		// rv.customView.addView(outsideButton);
		ePubView.addView(outsideButton);
		hideOutsideButton();
	}

	public void showOutsideButton() {
		this.setFrame(outsideButton, 0, 0, this.getWidth(), this.getHeight());
		outsideButton.setVisibility(View.VISIBLE);
	}

	public void hideOutsideButton() {
		outsideButton.setVisibility(View.INVISIBLE);
		outsideButton.setVisibility(View.GONE);
	}

	public void makeSeekBox() {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		seekBox = new SkyBox(this);
		seekBox.setBoxColor(Color.DKGRAY);
		seekBox.setArrowDirection(true); // to Down Arrow
		seekBox.setArrowHeight(ps(25));
		param.leftMargin = ps(0);
		param.topMargin = ps(0);
		param.width = ps(300);
		param.height = ps(65);
		seekBox.setLayoutParams(param);
		// public TextView makeLabel(int id, String text, int gravity,float
		// textSize,int textColor, int width, int height) {
		seekLabel = this.makeLabel(2000, "", Gravity.CENTER_HORIZONTAL, 13,
				Color.WHITE);
		this.setLocation(seekLabel, ps(10), ps(6));
		seekBox.addView(seekLabel);
		// rv.customView.addView(seekBox);
		ePubView.addView(seekBox);
		this.hideSeekBox();
	}

	public void showSeekBox() {
		seekBox.setVisibility(View.VISIBLE);
	}

	public void hideSeekBox() {
		seekBox.setVisibility(View.INVISIBLE);
		seekBox.setVisibility(View.GONE);
	}

	int op = 0;

	public void moveSeekBox(PageInformation pi) {
		// return;
		int position = seekBar.getProgress();
		if (Math.abs(op - position) < 10) {
			return;
		}
		seekLabel.setText(pi.chapterTitle);
		int slw = this.getLabelWidth(seekLabel);
		float cx = (float) ((this.getWidth() - ps(50) * 2) * (float) ((float) position / 999.0));
		float sx = cx - slw / 2 + ps(50);
		if (sx < ps(50))
			sx = ps(50);
		if (sx + slw > this.getWidth() - ps(50))
			sx = this.getWidth() - slw - ps(50);
		this.setFrame(seekBox, (int) sx, pyb(200), slw + ps(20), ps(65));
		seekBox.setArrowPosition((int) cx + ps(46), (int) sx, slw);
		op = position;
	}

	class ButtonHighlighterOnTouchListener implements OnTouchListener {
		final Button button;

		public ButtonHighlighterOnTouchListener(final Button button) {
			super();
			this.button = button;
		}

		@Override
		public boolean onTouch(final View view, final MotionEvent motionEvent) {
			if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
				// grey color filter, you can change the color as you like
				if (button.getId() == 5000) {
					button.setTextColor(Color.BLUE);
					button.setTextSize(16);
				} else if (button.getId() == 5001) {
					button.setTextColor(Color.BLUE);
					button.setTextSize(20);
				} else if (button.getId() == 6000 || button.getId() == 6001) {
					button.setTextSize(17);
					button.setTextColor(Color.YELLOW);
				} else if (button.getId() == 3001) {
					button.setTextColor(Color.BLACK);
				}
			} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
				if (button.getId() == 5000) {
					button.setTextColor(Color.BLACK);
					button.setTextSize(14);
				} else if (button.getId() == 5001) {
					button.setTextColor(Color.BLACK);
					button.setTextSize(18);
				} else if (button.getId() == 6000 || button.getId() == 6001) {
					button.setTextSize(15);
					button.setTextColor(Color.WHITE);
				} else if (button.getId() == 3001) {
					button.setTextColor(Color.DKGRAY);
				}
			}
			return false;
		}
	}

	class ImageButtonHighlighterOnTouchListener implements OnTouchListener {
		final ImageButton button;

		public ImageButtonHighlighterOnTouchListener(final ImageButton button) {
			super();
			this.button = button;
		}

		@Override
		public boolean onTouch(final View view, final MotionEvent motionEvent) {
			if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
				button.setColorFilter(Color.argb(155, 220, 220, 220));
			} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
				button.setColorFilter(Color.argb(0, 185, 185, 185));
			}
			return false;
		}
	}

	public void makeMenuBox() {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		menuBox = new SkyBox(this);
		menuBox.setBoxColor(Color.DKGRAY);
		menuBox.setArrowHeight(ps(25));
		menuBox.setArrowDirection(true);
		param.leftMargin = ps(100);
		param.topMargin = ps(100);
		param.width = ps(280);
		param.height = ps(80);
		menuBox.setLayoutParams(param);
		menuBox.setArrowDirection(false);
		highlightMenuButton = new Button(this);
		highlightMenuButton.setText("Highlight");
		highlightMenuButton.setId(6000);
		highlightMenuButton.setBackgroundColor(Color.TRANSPARENT);
		highlightMenuButton.setTextColor(Color.LTGRAY);
		highlightMenuButton.setTextSize(15);
		highlightMenuButton.setOnClickListener(listener);
		highlightMenuButton
				.setOnTouchListener(new ButtonHighlighterOnTouchListener(
						highlightMenuButton));
		this.setFrame(highlightMenuButton, ps(20), ps(0), ps(130), ps(65));
		menuBox.contentView.addView(highlightMenuButton);
		noteMenuButton = new Button(this);
		noteMenuButton.setText("Note");
		noteMenuButton.setId(6001);
		noteMenuButton.setBackgroundColor(Color.TRANSPARENT);
		noteMenuButton.setTextColor(Color.LTGRAY);
		noteMenuButton.setTextSize(15);
		noteMenuButton.setOnClickListener(listener);
		noteMenuButton.setOnTouchListener(new ButtonHighlighterOnTouchListener(
				noteMenuButton));
		this.setFrame(noteMenuButton, ps(150), ps(0), ps(130), ps(65));
		menuBox.contentView.addView(noteMenuButton);
		// rv.customView.addView(menuBox);
		ePubView.addView(menuBox);
		this.hideMenuBox();
	}

	public void makeHighlightBox() {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		highlightBox = new SkyBox(this);
		highlightBox.setBoxColor(currentColor);
		highlightBox.setArrowHeight(ps(25));
		highlightBox.setArrowDirection(true);
		param.leftMargin = ps(100);
		param.topMargin = ps(100);
		param.width = ps(280);
		param.height = ps(80);
		highlightBox.setLayoutParams(param);
		highlightBox.setArrowDirection(false);

		int bs = ps(38);
		colorButtonInHighlightBox = this.makeImageButton(6002,
				"icons/colorChooser@2x.png", bs, bs);
		trashButtonInHighlightBox = this.makeImageButton(6003,
				"icons/trash@2x.png", bs, bs);
		noteButtonInHighlightBox = this.makeImageButton(6004,
				"icons/memo@2x.png", bs, bs);
		shareButtonInHighlightBox = this.makeImageButton(6005,
				"icons/save@2x.png", bs, bs);

		int ds = 60;
		this.setLocation(colorButtonInHighlightBox, ps(10) + ps(ds) * 0, ps(4));
		this.setLocation(trashButtonInHighlightBox, ps(10) + ps(ds) * 1, ps(4));
		this.setLocation(noteButtonInHighlightBox, ps(10) + ps(ds) * 2, ps(8));
		this.setLocation(shareButtonInHighlightBox, ps(10) + ps(ds) * 3, ps(4));

		highlightBox.contentView.addView(colorButtonInHighlightBox);
		highlightBox.contentView.addView(trashButtonInHighlightBox);
		highlightBox.contentView.addView(noteButtonInHighlightBox);
		highlightBox.contentView.addView(shareButtonInHighlightBox);

		// rv.customView.addView(highlightBox);
		ePubView.addView(highlightBox);
		this.hideHighlightBox();
	}

	// 인수가 없을 경우 기존의 menuBox의 좌표를 따른다.
	public void showHighlightBox() {
		this.showOutsideButton();
		this.setFrame(highlightBox, boxFrame.left, boxFrame.top,
				boxFrame.width(), boxFrame.height());
		highlightBox.setArrowDirection(menuBox.isArrowDown);
		highlightBox.arrowPosition = menuBox.arrowPosition;
		highlightBox.arrowHeight = menuBox.arrowHeight;
		highlightBox.boxColor = currentColor;
		highlightBox.setVisibility(View.VISIBLE);
		isBoxesShown = true;
	}

	// 인수가 있을 경우 주로 onHighlightHit에서 전달된 좌표를 기준으로 박스를 보이게 한다.
	public void showHighlightBox(Rect startRect, Rect endRect) {
		this.showOutsideButton();
		highlightBox.setVisibility(View.VISIBLE);
		this.moveSkyBox(highlightBox, ps(280), ps(80), startRect, endRect);
		highlightBox.boxColor = currentHighlight.color;
		isBoxesShown = true;
	}

	public void hideHighlightBox() {
		highlightBox.setVisibility(View.INVISIBLE);
		highlightBox.setVisibility(View.GONE);
		isBoxesShown = false;
		hideOutsideButton();
	}

	public void makeColorBox() {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		colorBox = new SkyBox(this);
		colorBox.setBoxColor(currentColor);
		colorBox.setArrowHeight(ps(25));
		colorBox.setArrowDirection(true);
		param.leftMargin = ps(100);
		param.topMargin = ps(100);
		param.width = ps(280);
		param.height = ps(80);
		colorBox.setLayoutParams(param);
		colorBox.setArrowDirection(false);

		int bs = ps(38);
		ImageButton yellowButton = this.makeImageButton(6010,
				"icons/yellowBox@2x.png", bs, bs);
		ImageButton greenButton = this.makeImageButton(6011,
				"icons/greenBox@2x.png", bs, bs);
		ImageButton blueButton = this.makeImageButton(6012,
				"icons/blueBox@2x.png", bs, bs);
		ImageButton redButton = this.makeImageButton(6013,
				"icons/redBox@2x.png", bs, bs);

		int ds = 60;
		int oy = 3;
		this.setLocation(yellowButton, ps(10) + ps(ds) * 0, ps(oy));
		this.setLocation(greenButton, ps(10) + ps(ds) * 1, ps(oy));
		this.setLocation(blueButton, ps(10) + ps(ds) * 2, ps(oy));
		this.setLocation(redButton, ps(10) + ps(ds) * 3, ps(oy));

		colorBox.contentView.addView(yellowButton);
		colorBox.contentView.addView(greenButton);
		colorBox.contentView.addView(blueButton);
		colorBox.contentView.addView(redButton);

		// rv.customView.addView(colorBox);
		ePubView.addView(colorBox);
		this.hideColorBox();
	}

	public void showColorBox() {
		this.showOutsideButton();
		this.setFrame(colorBox, boxFrame.left, boxFrame.top, boxFrame.width(),
				boxFrame.height());
		colorBox.setArrowDirection(highlightBox.isArrowDown);
		colorBox.arrowPosition = highlightBox.arrowPosition;
		colorBox.arrowHeight = highlightBox.arrowHeight;
		colorBox.boxColor = currentColor;
		colorBox.setVisibility(View.VISIBLE);
		isBoxesShown = true;
	}

	public void hideColorBox() {
		colorBox.setVisibility(View.INVISIBLE);
		colorBox.setVisibility(View.GONE);
		isBoxesShown = false;
		hideOutsideButton();
	}

	public void showMenuBox(Rect startRect, Rect endRect) {
		menuBox.setVisibility(View.VISIBLE);
		this.moveSkyBox(menuBox, ps(280), ps(80), startRect, endRect);
		isBoxesShown = true;
	}

	public void hideMenuBox() {
		if (menuBox.getVisibility() != View.VISIBLE)
			return;
		menuBox.setVisibility(View.INVISIBLE);
		menuBox.setVisibility(View.GONE);
		isBoxesShown = false;
		hideOutsideButton();
	}

	public void dismissKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(noteEditor.getWindowToken(), 0);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		// noteEditor.clearFocus();
	}

	OnFocusChangeListener focusListener = new OnFocusChangeListener() {
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (hasFocus) {
				processForKeyboard(true);
			} else {
				processForKeyboard(false);
			}
		}
	};

	public void processForKeyboard(boolean isShown) {
		if (isShown) {
			// Toast.makeText(getApplicationContext(), "got the focus",
			// Toast.LENGTH_LONG).show();
		} else {
			// Toast.makeText(getApplicationContext(), "lost the focus",
			// Toast.LENGTH_LONG).show();
		}
	}

	// NoteBox Coodinate is always based on Highlight Area.
	public void makeNoteBox() {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		noteBox = new SkyBox(this);
		noteBox.setBoxColor(currentColor);
		noteBox.setArrowHeight(ps(25));
		noteBox.setArrowDirection(false);
		param.leftMargin = ps(50);
		param.topMargin = ps(400);
		int minWidth = Math.min(this.getWidth(), this.getHeight());
		noteBoxWidth = (int) (minWidth * 0.8);
		param.width = noteBoxWidth;
		param.height = ps(300);
		noteBox.setLayoutParams(param);
		noteBox.setArrowDirection(false);

		noteEditor = new EditText(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		params.width = LayoutParams.FILL_PARENT;
		params.height = LayoutParams.FILL_PARENT;
		noteEditor.setLayoutParams(params);
		noteEditor.setBackgroundColor(Color.TRANSPARENT);
		noteEditor.setMaxLines(1000);
		noteEditor.setGravity(Gravity.TOP | Gravity.LEFT);
		noteEditor.setOnFocusChangeListener(focusListener);
		noteBox.contentView.addView(noteEditor);

		ePubView.addView(noteBox);
		this.hideNoteBox();
	}

	public void showNoteBox() {
		if (currentHighlight == null)
			return;
		isBoxesShown = true;
		this.showOutsideButton();
		Rect startRect = rv.getStartRect(currentHighlight);
		Rect endRect = rv.getEndRect(currentHighlight);
		int minWidth = Math.min(this.getWidth(), this.getHeight());
		noteBoxWidth = (int) (minWidth * 0.7);
		noteBoxHeight = ps(300);
		noteEditor.setText(currentHighlight.note);
		noteBox.setBoxColor(currentColor);
		this.moveSkyBox(noteBox, noteBoxWidth, noteBoxHeight, startRect,
				endRect);
		noteBox.setVisibility(View.VISIBLE);
	}

	public void hideNoteBox() {
		this.noteBox.setVisibility(View.INVISIBLE);
		this.noteBox.setVisibility(View.GONE);
		this.dismissKeyboard();
		this.noteEditor.clearFocus();
		isBoxesShown = false;
		saveNoteBox();
		this.hideOutsideButton();
	}

	public void saveNoteBox() {
		if (currentHighlight == null || noteEditor == null)
			return;
		boolean isNote;
		String note = noteEditor.getText().toString();
		if (note == null || note.length() == 0)
			isNote = false;
		else
			isNote = true;
		currentHighlight.isNote = isNote;
		currentHighlight.note = note;
		rv.changeHighlightNote(currentHighlight, note);
	}

	class SkyDrawable extends ShapeDrawable {
		private final Paint fillpaint, strokepaint;

		public SkyDrawable(Shape s, int fillColor, int strokeColor,
				int strokeWidth) {
			super(s);
			fillpaint = new Paint(this.getPaint());
			fillpaint.setColor(fillColor);
			strokepaint = new Paint(fillpaint);
			strokepaint.setStyle(Paint.Style.STROKE);
			strokepaint.setStrokeWidth(strokeWidth);
			strokepaint.setColor(strokeColor);
		}

		@Override
		protected void onDraw(Shape shape, Canvas canvas, Paint paint) {
			shape.draw(canvas, fillpaint);
			shape.draw(canvas, strokepaint);
		}
	}

	public void makeSearchBox() {
		int boxColor = Color.rgb(241, 238, 229);
		int innerBoxColor = Color.rgb(246, 244, 239);
		int inlineColor = Color.rgb(133, 105, 75);

		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		searchBox = new SkyBox(this);
		searchBox.setBoxColor(boxColor);
		searchBox.setArrowHeight(ps(25));
		searchBox.setArrowDirection(false);
		param.leftMargin = ps(50);
		param.topMargin = ps(400);
		param.width = ps(400);
		param.height = ps(300);
		searchBox.setLayoutParams(param);
		searchBox.setArrowDirection(false);

		searchEditor = new EditText(this);
		this.setFrame(searchEditor, ps(20), ps(20), ps(400 - 140), ps(50));
		searchEditor.setTextSize(15f);
		searchEditor.setEllipsize(TruncateAt.END);
		searchEditor.setBackgroundColor(innerBoxColor);
		Drawable icon = this.getDrawableFromAssets("icons/search@2x.png");
		Bitmap bitmap = ((BitmapDrawable) icon).getBitmap();
		Drawable fd = new BitmapDrawable(getResources(),
				Bitmap.createScaledBitmap(bitmap, ps(28), ps(28), true));
		searchEditor.setCompoundDrawablesWithIntrinsicBounds(fd, null, null,
				null);
		RoundRectShape rrs = new RoundRectShape(new float[] { ps(15), ps(15),
				ps(15), ps(15), ps(15), ps(15), ps(15), ps(15) }, null, null);
		SkyDrawable sd = new SkyDrawable(rrs, innerBoxColor, inlineColor, 2);
		searchEditor.setBackgroundDrawable(sd);
		searchEditor.setHint("Type Words to Search");
		searchEditor.setPadding(ps(20), ps(5), ps(10), ps(5));
		searchEditor.setLines(1);
		searchEditor.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		searchEditor.setSingleLine();
		searchEditor.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_DONE
						|| actionId == EditorInfo.IME_ACTION_GO
						|| actionId == EditorInfo.IME_ACTION_SEARCH
						|| actionId == EditorInfo.IME_ACTION_NEXT) {
					String key = searchEditor.getText().toString();
					if (key != null && key.length() > 1) {
						showIndicator();
						clearSearchBox(1);
						rv.searchKey(key);
					}
				}
				return false;
			}
		});
		searchBox.contentView.addView(searchEditor);

		Button cancelButton = new Button(this);
		this.setFrame(cancelButton, ps(290), ps(20), ps(90), ps(50));
		cancelButton.setText("Cancel");
		cancelButton.setId(3001);
		RoundRectShape crs = new RoundRectShape(new float[] { ps(5), ps(5),
				ps(5), ps(5), ps(5), ps(5), ps(5), ps(5) }, null, null);
		SkyDrawable cd = new SkyDrawable(crs, innerBoxColor, inlineColor, 2);
		cancelButton.setBackgroundDrawable(cd);
		cancelButton.setTextSize(12);
		cancelButton.setOnClickListener(listener);
		cancelButton.setOnTouchListener(new ButtonHighlighterOnTouchListener(
				cancelButton));

		searchBox.contentView.addView(cancelButton);

		searchScrollView = new ScrollView(this);
		RoundRectShape rvs = new RoundRectShape(new float[] { ps(5), ps(5),
				ps(5), ps(5), ps(5), ps(5), ps(5), ps(5) }, null, null);
		SkyDrawable rd = new SkyDrawable(rvs, innerBoxColor, inlineColor, 2);
		searchScrollView.setBackgroundDrawable(rd);
		this.setFrame(searchScrollView, ps(20), ps(100), ps(360), ps(200));
		this.searchBox.contentView.addView(searchScrollView);

		searchResultView = new LinearLayout(this);
		searchResultView.setOrientation(LinearLayout.VERTICAL);
		searchScrollView.addView(searchResultView, new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

		this.ePubView.addView(searchBox);
		this.hideSearchBox();
	}

	public void hideSearchBox() {
		searchBox.setVisibility(View.INVISIBLE);
		searchBox.setVisibility(View.GONE);
		isBoxesShown = false;
		this.hideOutsideButton();
		rv.stopSearch();
	}

	public void showSearchBox() {
		isBoxesShown = true;
		this.showOutsideButton();
		this.clearSearchBox(0);
		int width = 400;
		int left, top;
		top = ps(65);
		if (!this.isTablet()) { // in case of phone
			if (this.isHighDensityPhone()) {
				left = pxr(width + 40);
				if (!isPortrait())
					top = ps(40);
			} else {
				left = pxr(width + 60);
			}
		} else { // in case of tablet
			left = pxr(width + 140);
			top = ps(120);
		}

		searchBox.setVisibility(View.VISIBLE);
		int sh;
		if (this.isPortrait()) {
			if (this.isTablet()) {
				sh = this.getHeight() - ps(400);
			} else {
				sh = this.getHeight() - ps(240);
			}
		} else {
			if (this.isTablet()) {
				sh = this.getHeight() - ps(350);
			} else {
				sh = this.getHeight() - ps(140);
			}
		}
		int rh = sh - ps(150);
		this.setFrame(searchBox, left, top, ps(width), sh);
		this.setFrame(searchScrollView, ps(20), ps(100), ps(360), rh);
		searchBox.setArrowHeight(ps(25));
		searchBox.setArrowPosition(pxr(100), left, ps(width));
	}

	public void clearSearchBox(int mode) {
		if (mode == 0) {
			this.dismissKeyboard();
			searchEditor.setText("");
			searchResultView.removeAllViews();
			searchResults.clear();
		} else {
			searchResultView.removeAllViews();
			searchResults.clear();
		}
	}

	public void cancelPressed() {
		this.clearSearchBox(0);
		this.hideSearchBox();
	}

	public void addSearchResult(SearchResult sr, int mode) {
		View view = this.makeResultItem(sr, mode);
		this.searchResultView.addView(view);
		if (mode == 0) {
			this.searchResults.add(sr);
		} else {
			this.moveSearchScrollViewToEnd();
		}
	}

	public void removeLastResult() {
		this.searchResultView
				.removeViewAt(searchResultView.getChildCount() - 1);
	}

	// 결국 여기서 정교한 표시가 있어야 한다.
	public View makeResultItem(SearchResult sr, int mode) {
		int inlineColor = Color.rgb(133, 105, 75);
		int headColor = Color.rgb(94, 61, 34);
		int textColor = Color.rgb(50, 40, 40);

		SkyLayout view = new SkyLayout(this);
		int itemWidth = ps(370);
		int itemHeight = ps(190);

		this.setFrame(view, 0, 0, itemWidth, itemHeight);

		TextView chapterLabel = null;
		TextView positionLabel = null;
		TextView textLabel = null;
		Button itemButton = null;
		itemButton = new Button(this);
		itemButton.setBackgroundColor(Color.TRANSPARENT);
		itemButton.setOnClickListener(listener);
		this.setFrame(itemButton, 0, 0, itemWidth, itemHeight);

		if (mode == 0) { // Normal case
			chapterLabel = this.makeLabel(3090, sr.chapterTitle, Gravity.LEFT,
					15, headColor);
			positionLabel = this.makeLabel(3091, String.format("%d/%d",
					sr.pageIndex + 1, sr.numberOfPagesInChapter), Gravity.LEFT,
					15, headColor);
			textLabel = this.makeLabel(3092, sr.text, Gravity.LEFT, 15,
					textColor);
			itemButton.setId(100000 + searchResults.size());
		} else if (mode == 1) { // Paused
			chapterLabel = this.makeLabel(3090, "Search More....",
					Gravity.CENTER, 18, headColor);
			// positionLabel = this.makeLabel(3091,
			// String.format("%d/%d",sr.pageIndex+1,sr.numberOfPagesInChapter),
			// Gravity.LEFT, 15, headColor);
			textLabel = this.makeLabel(3092, sr.numberOfSearched + " found.",
					Gravity.CENTER, 16, textColor);
			itemButton.setId(3093);
		} else if (mode == 2) { // finished
			chapterLabel = this.makeLabel(3090, "Search Finised.",
					Gravity.CENTER, 18, headColor);
			// positionLabel = this.makeLabel(3091,
			// String.format("%d/%d",sr.pageIndex+1,sr.numberOfPagesInChapter),
			// Gravity.LEFT, 15, headColor);
			textLabel = this.makeLabel(3092, sr.numberOfSearched + " found.",
					Gravity.CENTER, 16, textColor);
			itemButton.setId(3094);
		}

		textLabel.setMaxLines(3);

		if (mode == 0) {
			this.setFrame(chapterLabel, ps(20), ps(20), ps(270), ps(30));
			this.setFrame(positionLabel, itemWidth - ps(80), ps(20), ps(70),
					ps(30));
			this.setFrame(textLabel, ps(20), ps(80), itemWidth - ps(40),
					itemHeight - ps(80 + 20));
		} else {
			this.setFrame(chapterLabel, ps(20), ps(20), ps(350), ps(40));
			// this.setFrame(positionLabel, itemWidth-ps(80),
			// ps(20),ps(70),ps(30));
			this.setFrame(textLabel, ps(20), ps(80), itemWidth - ps(40),
					itemHeight - ps(80 + 20));
		}

		view.addView(chapterLabel);
		if (mode == 0)
			view.addView(positionLabel);
		view.addView(textLabel);
		view.addView(itemButton);

		RectShape crs = new RectShape();
		SkyDrawable cd = new SkyDrawable(crs, Color.TRANSPARENT, inlineColor, 1);
		view.setBackgroundDrawable(cd);

		return view;
	}

	public void moveSearchScrollViewToEnd() {
		searchScrollView.post(new Runnable() {
			@Override
			public void run() {
				searchScrollView.fullScroll(View.FOCUS_DOWN);
			}

		});
	}

	public void makeFontBox() {
		int boxColor = Color.rgb(241, 238, 229);
		int innerBoxColor = Color.rgb(246, 244, 239);
		int inlineColor = Color.rgb(133, 105, 75);

		int width = 450;
		int height = 400;
		fontBox = new SkyBox(this);
		fontBox.setBoxColor(boxColor);
		fontBox.setArrowHeight(ps(25));
		fontBox.setArrowDirection(false);
		setFrame(fontBox, ps(50), ps(200), ps(width), ps(height));

		// first make brightness controller
		View brView = new View(this);
		RoundRectShape rrs = new RoundRectShape(new float[] { ps(5), ps(5),
				ps(5), ps(5), ps(5), ps(5), ps(5), ps(5) }, null, null);
		SkyDrawable srd = new SkyDrawable(rrs, innerBoxColor, inlineColor, 1);
		brView.setBackgroundDrawable(srd);
		setFrame(brView, ps(20), ps(20), ps(width - 40), ps(53));
		this.fontBox.contentView.addView(brView);
		ImageButton sbb = this.makeImageButton(9005, "icons/brightness@2x.png",
				ps(30), ps(30));
		setFrame(sbb, ps(50), ps(32), ps(32), ps(32));
		sbb.setAlpha(200);
		ImageButton bbb = this.makeImageButton(9006, "icons/brightness@2x.png",
				ps(40), ps(40));
		setFrame(bbb, ps(width - 20 - 70), ps(28), ps(40), ps(40));
		bbb.setAlpha(200);
		this.fontBox.contentView.addView(sbb);
		this.fontBox.contentView.addView(bbb);
		brightBar = new SeekBar(this);
		brightBar.setMax(999);
		brightBar.setId(997);
		brightBar.setBackgroundColor(Color.TRANSPARENT);
		brightBar.setOnSeekBarChangeListener(new SeekBarDelegate());
		brightBar.setProgressDrawable(new LineDrawable(
				Color.rgb(160, 160, 160), ps(10)));
		brightBar.setThumbOffset(-1);
		setFrame(brightBar, ps(100), ps(24), ps(width - 210), ps(50));
		fontBox.contentView.addView(brightBar);

		// second make decrese/increse font buttons
		decreaseButton = new Button(this);
		setFrame(decreaseButton, ps(20), ps(90), ps(width - 40 - 20) / 2,
				ps(60));
		decreaseButton.setText("A");
		decreaseButton.setGravity(Gravity.CENTER);
		decreaseButton.setTextSize(14);
		decreaseButton.setId(5000);
		RoundRectShape drs = new RoundRectShape(new float[] { ps(5), ps(5),
				ps(5), ps(5), ps(5), ps(5), ps(5), ps(5) }, null, null);
		SkyDrawable drd = new SkyDrawable(drs, innerBoxColor, inlineColor, 1);
		decreaseButton.setBackgroundDrawable(drd);
		decreaseButton.setOnClickListener(listener);
		fontBox.contentView.addView(decreaseButton);
		decreaseButton.setOnTouchListener(new ButtonHighlighterOnTouchListener(
				decreaseButton));

		increaseButton = new Button(this);
		setFrame(increaseButton, ps(10 + width / 2), ps(90),
				ps(width - 40 - 20) / 2, ps(60));
		increaseButton.setText("A");
		increaseButton.setTextSize(18);
		increaseButton.setGravity(Gravity.CENTER);
		increaseButton.setId(5001);
		RoundRectShape irs = new RoundRectShape(new float[] { ps(5), ps(5),
				ps(5), ps(5), ps(5), ps(5), ps(5), ps(5) }, null, null);
		SkyDrawable ird = new SkyDrawable(irs, innerBoxColor, inlineColor, 1);
		increaseButton.setBackgroundDrawable(ird);
		fontBox.contentView.addView(increaseButton);
		increaseButton.setOnClickListener(listener);
		increaseButton.setOnTouchListener(new ButtonHighlighterOnTouchListener(
				increaseButton));

		// font list box
		int fontListHeight = height - 170 - 40;
		ScrollView fontScrollView = new ScrollView(this);
		RoundRectShape rvs = new RoundRectShape(new float[] { ps(5), ps(5),
				ps(5), ps(5), ps(5), ps(5), ps(5), ps(5) }, null, null);
		SkyDrawable rd = new SkyDrawable(rvs, innerBoxColor, inlineColor, 1);
		fontScrollView.setBackgroundDrawable(rd);
		this.setFrame(fontScrollView, ps(20), ps(170), ps(width - 40),
				ps(fontListHeight));
		this.fontBox.contentView.addView(fontScrollView);

		fontListView = new LinearLayout(this);
		fontListView.setOrientation(LinearLayout.VERTICAL);
		fontScrollView.addView(fontListView, new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

		int inlineColor2 = Color.argb(140, 133, 105, 75);
		int fontButtonHeight = 80;
		for (int i = 0; i < fontNames.length; i++) {
			Button fontButton = new Button(this);
			fontButton.setText(fontNames[i]);
			fontButton.setTextSize(20);
			Typeface tf = this.getTypeface(fontNames[i], Typeface.BOLD);
			fontButton.setTypeface(tf);
			fontButton.setId(5100 + i);
			RoundRectShape rs = new RoundRectShape(new float[] { ps(5), ps(5),
					ps(5), ps(5), ps(5), ps(5), ps(5), ps(5) }, null, null);
			SkyDrawable brd = new SkyDrawable(rs, innerBoxColor, inlineColor2,
					1);
			fontButton.setBackgroundDrawable(brd);
			this.setFrame(fontButton, ps(0), ps(0), ps(width - 40),
					ps(fontButtonHeight));
			fontListView.addView(fontButton);
			fontButton.setOnClickListener(listener);
			fontButton.setOnTouchListener(new ButtonHighlighterOnTouchListener(
					fontButton));
		}
		this.ePubView.addView(fontBox);
		this.hideFontBox();
	}

	public Typeface getTypeface(String fontName, int fontStyle) {
		Typeface tf = null;
		if (fontName.toLowerCase().contains("book")) {
			tf = Typeface.create(Typeface.DEFAULT, fontStyle);
		} else if (fontName.toLowerCase().contains("default")) {
			tf = Typeface.create(Typeface.DEFAULT, fontStyle);
		} else if (fontName.toLowerCase().contains("mono")) {
			tf = Typeface.create(Typeface.MONOSPACE, fontStyle);
		} else if ((fontName.toLowerCase().contains("sans"))) {
			tf = Typeface.create(Typeface.SANS_SERIF, fontStyle);
		} else if ((fontName.toLowerCase().contains("serif"))) {
			tf = Typeface.create(Typeface.SERIF, fontStyle);
		}
		return tf;
	}

	public String getFontName(int fontIndex) {
		if (fontIndex < 0)
			fontIndex = 0;
		if (fontIndex > (fontNames.length - 1))
			fontIndex = fontNames.length - 1;
		String name = fontNames[fontIndex];
		return name;
	}

	public int getFontIndex(String fontName) {
		for (int i = 0; i < this.fontNames.length; i++) {
			String name = fontNames[i];
			if (name.equalsIgnoreCase(fontName))
				return i;
		}
		return 0;
	}

	public void hideFontBox() {
		fontBox.setVisibility(View.INVISIBLE);
		fontBox.setVisibility(View.GONE);
		isBoxesShown = false;
		this.hideOutsideButton();
	}

	public void showFontBox() {
		isBoxesShown = true;
		this.showOutsideButton();
		int width = 450;
		int height = 400;
		int left, top;
		if (!this.isTablet()) {
			if (this.isHighDensityPhone()) {
				left = pxr(width + 20);
				top = ps(50);
				if (!isPortrait())
					height = 380;
			} else {
				left = pxr(width + 60);
				top = ps(65);
			}
		} else {
			if (this.isPortrait()) {
				left = pxr(width + 230);
				top = ps(120);
			} else {
				left = pxr(width + 200);
				top = ps(120);
			}
		}

		fontBox.setVisibility(View.VISIBLE);
		int sh = this.getHeight() - ps(240);
		int rh = sh - ps(150);
		this.setFrame(fontBox, left, top, ps(width), ps(height));
		fontBox.setArrowHeight(ps(25));
		fontBox.setArrowPosition(pxr(150), left, ps(width));
		brightBar.setProgress((int) (setting.brightness * 999));
		this.checkFonts();
	}

	public void makeListBox() {
		this.listBox = new SkyLayout(this);
		listTopButton = new Button(this);
		listTopButton.setId(9009);
		listTopButton.setOnClickListener(listener);
		listTopButton.setBackgroundColor(Color.TRANSPARENT);

		GradientDrawable gradForChecked = new GradientDrawable(
				Orientation.TOP_BOTTOM, new int[] { 0xff407ee6, 0xff6ca2f9 });
		GradientDrawable grad = new GradientDrawable(Orientation.TOP_BOTTOM,
				new int[] { 0xfff4f4f4, 0xffcdcdcd });
		this.contentListButton = new Button(this);
		this.contentListButton.setId(2700);
		this.contentListButton.setOnClickListener(listener);
		this.contentListButton.setText("Contents");
		this.contentListButton.setTextSize(13);

		this.bookmarkListButton = new Button(this);
		this.bookmarkListButton.setId(2701);
		this.bookmarkListButton.setOnClickListener(listener);
		this.bookmarkListButton.setText("Bookmark");
		this.bookmarkListButton.setTextSize(13);

		this.highlightListButton = new Button(this);
		this.highlightListButton.setId(2702);
		this.highlightListButton.setOnClickListener(listener);
		this.highlightListButton.setText("Highlight");
		this.highlightListButton.setTextSize(13);

		this.listScrollView = new ScrollView(this);
		this.listView = new LinearLayout(this);
		listView.setOrientation(LinearLayout.VERTICAL);

		this.listBox.addView(listTopButton);
		this.listBox.addView(contentListButton);
		this.listBox.addView(bookmarkListButton);
		this.listBox.addView(highlightListButton);

		this.listBox.addView(listScrollView);
		this.listScrollView.addView(listView, new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

		this.ePubView.addView(this.listBox);
		this.hideListBox();
	}

	public void hideListBox() {
		listBox.setVisibility(View.INVISIBLE);
		listBox.setVisibility(View.GONE);
		isBoxesShown = false;
		this.hideOutsideButton();
	}

	public void showListBox() {
		isBoxesShown = true;
		this.showOutsideButton();
		int lx, ly, lw, lh;
		if (this.isDoublePagedForLandscape && !this.isPortrait()) {
			lw = this.getWidth() / 2;
			lh = this.getHeight();
			lx = this.getWidth() / 2;
			ly = 0;
		} else {
			lx = 0;
			ly = 0;
			lw = this.getWidth();
			lh = this.getHeight();
		}
		this.setFrame(listBox, lx, ly, lw, lh);

		float tbh = .1f; // topButton height ratio;
		float bgy = .12f; // buttons guide line y ratio
		float bhr = .15f;
		float lgx = .1f; // left guide line x ratio
		float sgty = .22f; // scrollBox top ratio
		float sgby = .1f; // scrollBox bottom ratio

		int bh = ps(50); // button height;

		this.setFrame(listTopButton, 0, 0, lw, (int) (lh * tbh)); // topButton
																	// to hide
																	// listBox
		this.setFrame(contentListButton, (int) (lw * lgx), (int) (lh * bgy),
				(int) ((lw - (lw * lgx * 2)) / 3), bh);
		this.setFrame(bookmarkListButton, (int) (lw * lgx)
				+ (int) ((lw - (lw * lgx * 2)) / 3), (int) (lh * bgy),
				(int) ((lw - (lw * lgx * 2)) / 3), bh);
		this.setFrame(highlightListButton, (int) (lw * lgx)
				+ (int) ((lw - (lw * lgx * 2)) / 3) * 2, (int) (lh * bgy),
				(int) ((lw - (lw * lgx * 2)) / 3), bh);

		this.setFrame(this.listScrollView, (int) (lw * lgx), (int) (lh * sgty),
				(int) (lw - (lw * lgx * 2)),
				(int) (lh - (lh * sgty + lh * sgby)));

		this.checkListButton(listSelectedIndex);
		this.listBox.setVisibility(View.VISIBLE);
	}

	int listSelectedIndex = 0;

	public void checkListButton(int index) {
		GradientDrawable gradChecked = new GradientDrawable(
				Orientation.TOP_BOTTOM, new int[] { 0xff407ee6, 0xff6ca2f9 });
		gradChecked.setStroke(ps(1), Color.BLUE);
		GradientDrawable grad = new GradientDrawable(Orientation.TOP_BOTTOM,
				new int[] { 0xfff4f4f4, 0xffcdcdcd });
		grad.setStroke(ps(1), Color.LTGRAY);
		listSelectedIndex = index;
		Button buttons[] = { contentListButton, bookmarkListButton,
				highlightListButton };
		for (int i = 0; i < buttons.length; i++) {
			Button button = buttons[i];
			button.setBackgroundDrawable(grad);
		}
		Button target = buttons[index];
		target.setBackgroundDrawable(gradChecked);
		// show contents..
		if (listSelectedIndex == 0)
			fillContentsList();
		else if (listSelectedIndex == 1)
			fillBookmarkList();
		else if (listSelectedIndex == 2)
			fillHighlightList();
	}

	private void displayNavPoints() {
		NavPoints nps = rv.getNavPoints();
		for (int i = 0; i < nps.getSize(); i++) {
			NavPoint np = nps.getNavPoint(i);
			debug("" + i + ":" + np.text);
		}

		// modify one NavPoint object at will
		NavPoint onp = nps.getNavPoint(1);
		onp.text = "preface - it is modified";

		for (int i = 0; i < nps.getSize(); i++) {
			NavPoint np = nps.getNavPoint(i);
			debug("" + i + ":" + np.text + "   :" + np.sourcePath);
		}
	}

	public void fillContentsList() {
		this.listView.removeAllViews();
		NavPoints nps = rv.getNavPoints();
		for (int i = 0; i < nps.getSize(); i++) {
			NavPoint np = nps.getNavPoint(i);
			Button contentButton = new Button(this);
			contentButton.setBackgroundColor(Color.TRANSPARENT);
			contentButton.setText(np.text);
			contentButton.setTextSize(14);
			contentButton.setGravity(Gravity.LEFT);
			contentButton.setId(i);
			contentButton.setOnClickListener(contentDelegate);
			listView.addView(contentButton);
			debug("" + i + ":" + np.text);
		}
	}

	NavPoint targetNavPoint = null;
	private OnClickListener contentDelegate = new OnClickListener() {
		public void onClick(View arg) {
			int index = arg.getId();
			RectShape rs = new RectShape();
			GradientDrawable sd = new GradientDrawable(Orientation.TOP_BOTTOM,
					new int[] { 0xff407ee6, 0xff6ca2f9 });
			SkyDrawable ed = new SkyDrawable(rs, Color.TRANSPARENT,
					Color.TRANSPARENT, ps(1));
			blinkBackground(arg, sd, ed);
			NavPoints nps = rv.getNavPoints();
			targetNavPoint = nps.getNavPoint(index);
			new Handler().postDelayed(new Runnable() {
				public void run() {
					isPagesHidden = false;
					showPages();
					rv.gotoPageByNavPoint(targetNavPoint);
				}
			}, 200);
		}
	};

	public void fillBookmarkList() {
		this.listView.removeAllViews();
		ArrayList<PageInformation> pis = sd.fetchBookmarks(this.bookCode);
		for (int i = 0; i < pis.size(); i++) {
			PageInformation pi = pis.get(i);
			SkyLayout item = new SkyLayout(this);
			setFrame(item, 0, 0, listBox.getWidth(), ps(80));
			ImageButton mark = this.makeImageButton(9898,
					"icons/bookmarked@2x.png", ps(40), ps(70));
			setFrame(mark, ps(10), ps(5), ps(40), ps(70));
			item.addView(mark);
			TextView chapterLabel = this.makeLabel(9899,
					rv.getChapterTitle(pi.chapterIndex), Gravity.LEFT, 16,
					Color.BLACK);
			setFrame(chapterLabel, ps(80), ps(5), this.listBox.getWidth()
					- ps(80), ps(40));
			item.addView(chapterLabel);
			TextView dateLabel = this.makeLabel(9899, pi.datetime,
					Gravity.LEFT, 12, Color.BLACK);
			setFrame(dateLabel, this.listBox.getWidth() - ps(50 + 250), ps(48),
					this.listBox.getWidth() - ps(40), ps(40));
			View lineView = new View(this);
			lineView.setBackgroundColor(Color.LTGRAY);
			setFrame(lineView, 0, ps(79), this.listBox.getWidth(), ps(1));
			item.addView(dateLabel);
			item.addView(lineView);
			item.setSkyLayoutListener(bookmarkListDelegate);
			item.setId(pi.code);
			item.data = pi;

			Button deleteButton = new Button(this);
			GradientDrawable grad = new GradientDrawable(
					Orientation.TOP_BOTTOM,
					new int[] { 0xffcf666e, 0xff671521 });
			grad.setStroke(ps(2), 0xff282828);
			deleteButton.setBackgroundDrawable(grad);
			deleteButton.setText("Delete");
			deleteButton.setTypeface(null, Typeface.BOLD);
			deleteButton.setTextColor(Color.WHITE);
			deleteButton.setId(pi.code);
			deleteButton.setVisibility(View.INVISIBLE);
			deleteButton.setVisibility(View.GONE);
			deleteButton.setOnClickListener(deleteBookmarkDelegate);
			int dw = ps(120);
			int dh = ps(50);
			setFrame(deleteButton, this.listView.getWidth() - dw,
					(ps(80) - dh) / 2, dw, dh);
			item.editControl = deleteButton;
			item.addView(deleteButton);

			this.listView.addView(item);
		}
	}

	View tempView;
	Drawable tempDrawable;

	private void blinkBackground(View view, Drawable startDrawable,
			Drawable endDrawable) {
		tempView = view;
		tempDrawable = endDrawable;
		view.setBackgroundDrawable(startDrawable);
		new Handler().postDelayed(new Runnable() {
			public void run() {
				tempView.setBackgroundDrawable(tempDrawable);
				tempView = null;
			}
		}, 100);
	}

	private OnClickListener deleteBookmarkDelegate = new OnClickListener() {
		public void onClick(View arg) {
			int targetCode = arg.getId();
			for (int i = 0; i < listView.getChildCount(); i++) {
				SkyLayout view = (SkyLayout) listView.getChildAt(i);
				if (view.getId() == targetCode) {
					listView.removeViewAt(i);
					sd.deleteBookmarkByCode(targetCode);
				}
			}
		}
	};

	private SkyLayoutListener bookmarkListDelegate = new SkyLayoutListener() {
		@Override
		public void onShortPress(SkyLayout view, MotionEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onLongPress(SkyLayout view, MotionEvent e) {
			// TODO Auto-generated method stub
			beep(100);
			Button deleteButton = (Button) view.editControl;
			int vt = deleteButton.getVisibility();
			if (vt != View.VISIBLE) {
				deleteButton.setVisibility(View.VISIBLE);
			} else {
				deleteButton.setVisibility(View.INVISIBLE);
				deleteButton.setVisibility(View.GONE);
			}
		}

		@Override
		public void onSwipeToLeft(SkyLayout view) {
			// TODO Auto-generated method stub
			beep(100);
			Button deleteButton = (Button) view.editControl;
			deleteButton.setVisibility(View.INVISIBLE);
			deleteButton.setVisibility(View.GONE);
		}

		@Override
		public void onSwipeToRight(SkyLayout view) {
			// TODO Auto-generated method stub
			beep(100);
			Button deleteButton = (Button) view.editControl;
			deleteButton.setVisibility(View.VISIBLE);
		}

		PageInformation targetPI = null;

		@Override
		public void onSingleTapUp(SkyLayout view, MotionEvent e) {
			// TODO Auto-generated method stub
			Button deleteButton = (Button) view.editControl;
			int vt = deleteButton.getVisibility();
			if (vt == View.VISIBLE)
				return;
			PageInformation pi = (PageInformation) view.data;
			RectShape rs = new RectShape();
			GradientDrawable sd = new GradientDrawable(Orientation.TOP_BOTTOM,
					new int[] { 0xff407ee6, 0xff6ca2f9 });
			SkyDrawable ed = new SkyDrawable(rs, Color.TRANSPARENT,
					Color.TRANSPARENT, ps(1));
			blinkBackground(view, sd, ed);
			targetPI = pi;
			new Handler().postDelayed(new Runnable() {
				public void run() {
					isPagesHidden = false;
					showPages();
					rv.gotoPageByPagePositionInBook(targetPI.pagePositionInBook);
				}
			}, 200);
		}
	};

	public void fillHighlightList() {
		this.listView.removeAllViews();
		Highlights highlights = sd.fetchAllHighlights(this.bookCode);
		for (int i = 0; i < highlights.getSize(); i++) {
			Highlight highlight = highlights.getHighlight(i);
			SkyLayout item = new SkyLayout(this);
			TextView chapterLabel = this.makeLabel(9899,
					rv.getChapterTitle(highlight.chapterIndex), Gravity.LEFT,
					16, Color.BLACK);
			setFrame(chapterLabel, ps(20), ps(5), this.listView.getWidth()
					- ps(20), ps(40));
			item.addView(chapterLabel);

			GradientDrawable textGrad = new GradientDrawable(
					Orientation.TOP_BOTTOM, new int[] {
							getBrighterColor(highlight.color),
							getDarkerColor(highlight.color) });
			TextView textLabel = this.makeLabel(9899, highlight.text,
					Gravity.LEFT, 16, Color.BLACK);
			setFrame(textLabel, ps(20), ps(5 + 40 + 5),
					this.listView.getWidth() - ps(20), ps(70));
			textLabel.setBackgroundDrawable(textGrad);
			textLabel.getBackground().setAlpha(180);

			item.addView(textLabel);

			int noteHeight = 0;

			if (highlight.isNote && highlight.note != null
					&& highlight.note.length() != 0
					&& !highlight.note.equalsIgnoreCase("null")) {
				TextView noteLabel = this.makeLabel(9899, highlight.note,
						Gravity.LEFT, 16, Color.BLACK);
				noteLabel.setTextColor(getDarkerColor(highlight.color));
				// noteHeight =
				// noteLabel.getLineCount()*noteLabel.getLineHeight();
				noteHeight = 70;
				setFrame(noteLabel, ps(20), ps(5 + 40 + 5 + 70 + 5),
						this.listView.getWidth() - ps(20), ps(noteHeight));
				item.addView(noteLabel);
			}

			TextView dateLabel = this.makeLabel(9899, highlight.datetime,
					Gravity.RIGHT, 12, Color.BLACK);
			int lw = this.listView.getWidth();
			setFrame(dateLabel, 0, ps(5 + 40 + 5 + 70 + 5 + noteHeight + 5),
					lw, ps(40));
			item.addView(dateLabel);

			// int itemHeight = ps(5+40+5+90+5+noteHeight+5+40+5);
			int itemHeight = ps(5 + 40 + 5 + 90 + 5 + noteHeight + 5 + 15 + 5);

			View lineView = new View(this);
			lineView.setBackgroundColor(Color.LTGRAY);
			setFrame(lineView, 0, itemHeight - ps(1), this.listView.getWidth(),
					ps(1));
			item.addView(lineView);

			setFrame(item, 0, 0, listView.getWidth(), itemHeight);
			item.setSkyLayoutListener(highlightListDelegate);
			item.setId(highlight.code);
			item.data = highlight;

			Button deleteButton = new Button(this);
			GradientDrawable grad = new GradientDrawable(
					Orientation.TOP_BOTTOM,
					new int[] { 0xffcf666e, 0xff671521 });
			grad.setStroke(ps(2), 0xff282828);
			deleteButton.setBackgroundDrawable(grad);
			deleteButton.setText("Delete");
			deleteButton.setTypeface(null, Typeface.BOLD);
			deleteButton.setTextColor(Color.WHITE);
			deleteButton.setId(highlight.code);
			deleteButton.setVisibility(View.INVISIBLE);
			deleteButton.setVisibility(View.GONE);
			deleteButton.setOnClickListener(deleteHighlightDelegate);
			int dw = ps(120);
			int dh = ps(50);
			setFrame(deleteButton, this.listView.getWidth() - dw,
					(itemHeight - dh) / 2, dw, dh);
			item.editControl = deleteButton;
			item.addView(deleteButton);

			this.listView.addView(item);
		}
	}

	private void beep(int ms) {
		Vibrator vibe = (Vibrator) this
				.getSystemService(Context.VIBRATOR_SERVICE);
		vibe.vibrate(ms);
	}

	private OnClickListener deleteHighlightDelegate = new OnClickListener() {
		public void onClick(View arg) {
			int targetCode = arg.getId();
			for (int i = 0; i < listView.getChildCount(); i++) {
				SkyLayout view = (SkyLayout) listView.getChildAt(i);
				if (view.getId() == targetCode) {
					listView.removeViewAt(i);
					sd.deleteHighlightByCode(targetCode);
				}
			}
		}
	};

	private SkyLayoutListener highlightListDelegate = new SkyLayoutListener() {
		@Override
		public void onShortPress(SkyLayout view, MotionEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onLongPress(SkyLayout view, MotionEvent e) {
			// TODO Auto-generated method stub
			beep(100);
			Button deleteButton = (Button) view.editControl;
			int vt = deleteButton.getVisibility();
			if (vt != View.VISIBLE) {
				deleteButton.setVisibility(View.VISIBLE);
			} else {
				deleteButton.setVisibility(View.INVISIBLE);
				deleteButton.setVisibility(View.GONE);
			}
		}

		@Override
		public void onSwipeToLeft(SkyLayout view) {
			// TODO Auto-generated method stub
			beep(100);
			Button deleteButton = (Button) view.editControl;
			deleteButton.setVisibility(View.INVISIBLE);
			deleteButton.setVisibility(View.GONE);
		}

		@Override
		public void onSwipeToRight(SkyLayout view) {
			// TODO Auto-generated method stub
			beep(100);
			Button deleteButton = (Button) view.editControl;
			deleteButton.setVisibility(View.VISIBLE);
		}

		Highlight targetHighlight = null;

		@Override
		public void onSingleTapUp(SkyLayout view, MotionEvent e) {
			// TODO Auto-generated method stub
			Button deleteButton = (Button) view.editControl;
			int vt = deleteButton.getVisibility();
			if (vt == View.VISIBLE)
				return;
			Highlight highlight = (Highlight) view.data;
			RectShape rs = new RectShape();
			GradientDrawable sd = new GradientDrawable(Orientation.TOP_BOTTOM,
					new int[] { 0xff407ee6, 0xff6ca2f9 });
			SkyDrawable ed = new SkyDrawable(rs, Color.TRANSPARENT,
					Color.TRANSPARENT, ps(1));
			blinkBackground(view, sd, ed);
			targetHighlight = highlight;
			new Handler().postDelayed(new Runnable() {
				public void run() {
					isPagesHidden = false;
					showPages();
					rv.gotoPageByHighlight(targetHighlight);
				}
			}, 200);
		}
	};

	public void makeMediaBox() {
		mediaBox = new SkyLayout(this);
		setFrame(mediaBox, 100, 200, ps(270), ps(50));

		int bs = ps(38);
		int sb = 15;
		prevButton = this.makeImageButton(9898, "icons/Prev@2x.png", bs, bs);
		setFrame(prevButton, ps(10), ps(5), bs, bs);
		prevButton.setId(8080);
		prevButton.setOnClickListener(listener);
		playAndPauseButton = this.makeImageButton(9898, "icons/Pause@2x.png",
				bs, bs);
		setFrame(playAndPauseButton, ps(sb) + bs + ps(10), ps(5), bs, bs);
		playAndPauseButton.setId(8081);
		playAndPauseButton.setOnClickListener(listener);

		stopButton = this.makeImageButton(9898, "icons/Stop@2x.png", bs, bs);
		setFrame(stopButton, (ps(sb) + bs) * 2, ps(5), bs, bs);
		stopButton.setId(8082);
		stopButton.setOnClickListener(listener);
		nextButton = this.makeImageButton(9898, "icons/Next@2x.png", bs, bs);
		setFrame(nextButton, (ps(sb) + bs) * 3, ps(5), bs, bs);
		nextButton.setId(8083);
		nextButton.setOnClickListener(listener);

		mediaBox.setVisibility(View.INVISIBLE);
		mediaBox.setVisibility(View.GONE);

		mediaBox.addView(prevButton);
		mediaBox.addView(playAndPauseButton);
		mediaBox.addView(stopButton);
		mediaBox.addView(nextButton);
		this.ePubView.addView(mediaBox);
	}

	public void hideMediaBox() {
		if (mediaBox != null) {
			titleLabel.setVisibility(View.VISIBLE);
			mediaBox.setVisibility(View.INVISIBLE);
			mediaBox.setVisibility(View.GONE);
		}
	}

	public void showMediaBox() {
		titleLabel.setVisibility(View.INVISIBLE);
		titleLabel.setVisibility(View.GONE);
		mediaBox.setVisibility(View.VISIBLE);
	}

	public void moveSkyBox(SkyBox box, int boxWidth, int boxHeight,
			Rect startRect, Rect endRect) {
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) box
				.getLayoutParams();
		int topMargin = ps(80);
		int bottomMargin = ps(80);
		int boxTop = 0;
		int boxLeft = 0;
		int arrowX;
		boolean isArrowDown;
		// 상부에 공간이 있는지 살핀다.
		// 가장 정상정인 경우
		if (startRect.top - topMargin > boxHeight) {
			boxTop = startRect.top - boxHeight - ps(10);
			boxLeft = (startRect.left + startRect.width() / 2 - boxWidth / 2);
			arrowX = (startRect.left + startRect.width() / 2);
			isArrowDown = true;
		} else if ((this.getHeight() - endRect.bottom) - bottomMargin > boxHeight) { // 하부에
																						// 공간이
																						// 있는지
																						// 살핀다.
			boxTop = endRect.bottom + ps(10);
			boxLeft = (endRect.left + endRect.width() / 2 - boxWidth / 2);
			arrowX = (endRect.left + endRect.width() / 2);
			isArrowDown = false;
		} else {
			boxTop = ps(100);
			boxLeft = (startRect.left + startRect.width() / 2 - boxWidth / 2);
			arrowX = (startRect.left + startRect.width() / 2);
			isArrowDown = true;
		}

		if (boxLeft + boxWidth > this.getWidth() * .9) {
			boxLeft = (int) (this.getWidth() * .9) - boxWidth;
		} else if (boxLeft < this.getWidth() * .1) {
			boxLeft = (int) (this.getWidth() * .1);
		}

		box.setArrowPosition(arrowX, boxLeft, boxWidth);
		box.setArrowDirection(isArrowDown);
		params.leftMargin = boxLeft;
		params.topMargin = boxTop;
		params.width = boxWidth;
		params.height = boxHeight;
		box.setLayoutParams(params);
		box.invalidate();

		boxFrame = new Rect();
		boxFrame.left = boxLeft;
		boxFrame.top = boxTop;
		boxFrame.right = boxLeft + boxWidth;
		boxFrame.bottom = boxTop + boxHeight;
	}

	Drawable getDrawableFromAssets(String name) {
		try {
			// InputStream ims = getResources().getAssets().open(name);
			// Drawable d = Drawable.createFromStream(ims, null);
			Drawable d = Drawable
					.createFromStream(getAssets().open(name), null);
			return d;
		} catch (Exception e) {
			return null;
		}
	}

	public ImageButton makeImageButton(int id, String imageName, int width,
			int height) {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		Drawable icon;
		ImageButton button = new ImageButton(this);
		button.setId(id);
		button.setOnClickListener(listener);
		button.setBackgroundColor(Color.TRANSPARENT);
		icon = this.getDrawableFromAssets(imageName);
		icon.setBounds(0, 0, width, height);
		Bitmap iconBitmap = ((BitmapDrawable) icon).getBitmap();
		Bitmap bitmapResized = Bitmap.createScaledBitmap(iconBitmap, width,
				height, false);
		button.setImageBitmap(bitmapResized);
		button.setVisibility(View.VISIBLE);
		param.width = width;
		param.height = height;
		button.setLayoutParams(param);
		button.setOnTouchListener(new ImageButtonHighlighterOnTouchListener(
				button));
		return button;
	}

	public TextView makeLabel(int id, String text, int gravity, float textSize,
			int textColor) {
		TextView label = new TextView(this);
		label.setId(id);
		label.setGravity(gravity);
		label.setBackgroundColor(Color.TRANSPARENT);
		label.setText(text);
		label.setTextColor(textColor);
		label.setTextSize(textSize);
		return label;
	}

	public void setFrame(View view, int dx, int dy, int width, int height) {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		param.leftMargin = dx;
		param.topMargin = dy;
		param.width = width;
		param.height = height;
		view.setLayoutParams(param);
	}

	public void setLocation(View view, int px, int py) {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		param.leftMargin = px;
		param.topMargin = py;
		view.setLayoutParams(param);
	}

	int getLabelWidth(TextView tv) {
		tv.measure(0, 0); // must call measure!
		return tv.getMeasuredWidth(); // get height
	}

	int getLabelHeight(TextView tv) {
		tv.measure(0, 0); // must call measure!
		return tv.getMeasuredHeight(); // get width
	}

	long timeRepainted = 0;

	public void toggleControls() {
		long timeNow = System.currentTimeMillis();
		long diff = timeNow - timeRepainted;

		if (diff < 1000)
			return; // prevent continuous tapping.

		isControlsShown = !isControlsShown;
		if (isControlsShown) {
			showControls();
		} else {
			hideControls();
		}
		// rv.repaint();
		timeRepainted = System.currentTimeMillis();
	}

	public void showControls() {
		this.rotationButton.setVisibility(View.VISIBLE);
		this.listButton.setVisibility(View.VISIBLE);
		this.fontButton.setVisibility(View.VISIBLE);
		this.searchButton.setVisibility(View.VISIBLE);
		this.seekBar.setVisibility(View.VISIBLE);
	}

	public void hideControls() {
		this.rotationButton.setVisibility(View.INVISIBLE);
		this.rotationButton.setVisibility(View.GONE);
		this.listButton.setVisibility(View.INVISIBLE);
		this.listButton.setVisibility(View.GONE);
		this.fontButton.setVisibility(View.INVISIBLE);
		this.fontButton.setVisibility(View.GONE);
		this.searchButton.setVisibility(View.INVISIBLE);
		this.searchButton.setVisibility(View.GONE);
		this.seekBar.setVisibility(View.INVISIBLE);
		this.seekBar.setVisibility(View.GONE);
	}

	public boolean isAboveIcecream() {
		if (android.os.Build.VERSION.SDK_INT >= 14) { // api >= icecream
			return true;
		} else {
			return false;
		}
	}

	public boolean isHoneycomb() {
		int API = android.os.Build.VERSION.SDK_INT;
		if (API == 11 || API == 12 || API == 13) { // Honeycomb
			return true;
		} else {
			return false;
		}
	}

	public void makeControls() {
		int bs = 38;
		rotationButton = this.makeImageButton(9000,
				"icons/rotationLocked@2x.png", ps(42), ps(42));
		listButton = this.makeImageButton(9001, "icons/list@2x.png", getPS(bs),
				getPS(bs));
		fontButton = this.makeImageButton(9002, "icons/font@2x.png", getPS(bs),
				getPS(bs));
		searchButton = this.makeImageButton(9003, "icons/search@2x.png",
				getPS(bs), getPS(bs));

		titleLabel = this.makeLabel(3000, title, Gravity.CENTER_HORIZONTAL, 17,
				Color.argb(240, 94, 61, 35)); // setTextSize in android uses sp
												// (Scaled Pixel) as default,
												// they say that sp guarantees
												// the device dependent size,
												// but as usual in android it
												// can't be 100% sure.
		authorLabel = this.makeLabel(3000, author, Gravity.CENTER_HORIZONTAL,
				17, Color.argb(240, 94, 61, 35));
		pageIndexLabel = this.makeLabel(3000, "......",
				Gravity.CENTER_HORIZONTAL, 13, Color.argb(240, 94, 61, 35));
		secondaryIndexLabel = this.makeLabel(3000, "......",
				Gravity.CENTER_HORIZONTAL, 13, Color.argb(240, 94, 61, 35));

		rv.customView.addView(rotationButton);
		rv.customView.addView(listButton);
		rv.customView.addView(fontButton);
		rv.customView.addView(searchButton);
		rv.customView.addView(titleLabel);
		rv.customView.addView(authorLabel);
		// rv.customView.addView(pageIndexLabel);
		// rv.customView.addView(secondaryIndexLabel);
		ePubView.addView(pageIndexLabel);
		ePubView.addView(secondaryIndexLabel);

		seekBar = new SeekBar(this);
		seekBar.setMax(999);
		seekBar.setId(999);
		RectShape rectShape = new RectShape();
		ShapeDrawable thumb = new ShapeDrawable(rectShape);
		thumb.getPaint().setColor(Color.argb(240, 94, 61, 35));
		thumb.setIntrinsicHeight(getPS(28));
		thumb.setIntrinsicWidth(getPS(28));
		seekBar.setThumb(thumb);
		seekBar.setBackgroundColor(Color.TRANSPARENT);
		seekBar.setOnSeekBarChangeListener(new SeekBarDelegate());
		seekBar.setProgressDrawable(new DottedDrawable());
		seekBar.setThumbOffset(-3);
		seekBar.setMinimumHeight(24);
		// if (this.isAboveIcecream()) {
		// rv.customView.addView(seekBar);
		// }else {
		// ePubView.addView(seekBar);
		// }
		ePubView.addView(seekBar);
	}

	public void recalcFrames() {
		this.authorLabel.setVisibility(View.VISIBLE);
		this.secondaryIndexLabel.setVisibility(View.VISIBLE);

		if (!this.isTablet()) { // for phones - tested with Galaxy S2, Galaxy
								// S3, Galaxy S4
			if (this.isPortrait()) {
				this.setLocation(rotationButton, pxl(20), pyt(15 - 2));
				this.setLocation(listButton, pxl(20 + (48 + 5) * 1), pyt(15));
				this.setLocation(fontButton, pxr(40 + (48 + 5) * 3), pyt(15));
				this.setLocation(searchButton, pxr(40 + (48 + 5) * 2), pyt(15));
				this.setFrame(seekBar, pxl(45), pyb(140), pw(50 + 6), ps(36));
				int brx = 36 + (44) * 1;
				int bry = 23;
				bookmarkRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 40),
						pyt(bry + 40));
				bookmarkedRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 38),
						pyt(bry + 70));
			} else {

				int sd = ps(40);
				this.setLocation(rotationButton, pxl(10), pyt(5 - 2));
				this.setLocation(listButton, pxl(10 + (48 + 5) * 1), pyt(5));
				this.setLocation(fontButton, pxr(60 + (48 + 5) * 3), pyt(5));
				this.setLocation(searchButton, pxr(60 + (48 + 5) * 2), pyt(5));
				this.setFrame(seekBar, pxl(70), pyb(105), pw(80), ps(36));
				int brx = 40 + (48 + 12) * 1;
				int bry = 14;
				bookmarkRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 40),
						pyt(bry + 40));
				bookmarkedRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 38),
						pyt(bry + 70));
			}
		} else { // for tables - tested with Galaxy Tap 10.1, Galaxy Note 10.1
			if (this.isPortrait()) {
				int ox = 50;
				int rx = 100;
				int oy = 30;
				int bw = ps(65);
				this.setFrame(rotationButton, pxl(ox), pyt(oy - 2), bw, bw);
				this.setFrame(listButton, pxl(ox + (65) * 1), pyt(oy), bw, bw);
				this.setFrame(fontButton, pxr(rx + (65) * 3), pyt(oy), bw, bw);
				this.setFrame(searchButton, pxr(rx + (65) * 2), pyt(oy), bw, bw);
				this.setFrame(seekBar, pxl(50), pyb(140), pw(50 + 6), ps(45));
				if (this.isHoneycomb()) {
					this.setFrame(seekBar, pxl(80), pyb(220), pw(100), ps(45));
				} else {
					this.setFrame(seekBar, pxl(80), pyb(140), pw(100), ps(45));
				}
				int brx = rx - 10 + (44) * 1;
				int bry = oy + 10;
				bookmarkRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 50),
						pyt(bry + 50));
				bookmarkedRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 50),
						pyt(bry + 90));
			} else {
				int sd = ps(40);
				int ox = 40;
				int rx = 130;
				int oy = 20;
				int bw = ps(65);
				this.setFrame(rotationButton, pxl(ox), pyt(oy - 2), bw, bw);
				this.setFrame(listButton, pxl(ox + (65) * 1), pyt(oy), bw, bw);

				this.setFrame(fontButton, pxr(rx + (65) * 3), pyt(oy), bw, bw);
				this.setFrame(searchButton, pxr(rx + (65) * 2), pyt(oy), bw, bw);

				if (this.isHoneycomb()) {
					this.setFrame(seekBar, pxl(200), pyb(180), pw(200), ps(45));
				} else {
					this.setFrame(seekBar, pxl(150), pyb(105), pw(200), ps(45));
				}

				int brx = rx - 20 + (48 + 12) * 1;
				int bry = oy + 10;
				bookmarkRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 40),
						pyt(bry + 40));
				bookmarkedRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 38),
						pyt(bry + 70));
			}
		}
		this.recalcLabelsLayout();
	}

	public void recalcLabelsLayout() {
		int sd = this.getWidth() / 40;
		if (!this.isTablet()) { // phone
			if (this.isPortrait()) {
				this.setLocation(
						titleLabel,
						(this.getWidth() / 2 - this.getLabelWidth(titleLabel) / 2)
								- sd, pyt(28));
				this.setLocation(mediaBox, this.getWidth() / 2 - ps(270) / 2,
						pyt(22));
				this.authorLabel.setVisibility(View.INVISIBLE);
				this.authorLabel.setVisibility(View.GONE);
				this.secondaryIndexLabel.setVisibility(View.INVISIBLE);
				this.secondaryIndexLabel.setVisibility(View.GONE);
				this.setLocation(
						pageIndexLabel,
						(this.getWidth() / 2 - this
								.getLabelWidth(pageIndexLabel) / 2) - sd,
						pyb(105));
			} else {
				if (this.isDoublePagedForLandscape) {
					if (this.isHighDensityPhone()) {
						this.authorLabel.setVisibility(View.INVISIBLE);
						this.authorLabel.setVisibility(View.GONE);
					} else {
						this.authorLabel.setVisibility(View.VISIBLE);
					}
					this.secondaryIndexLabel.setVisibility(View.VISIBLE);
					this.setLocation(
							titleLabel,
							this.getWidth() / 4
									- this.getLabelWidth(titleLabel) / 2,
							pyt(17));
					this.setLocation(mediaBox, this.getWidth() / 4 - ps(270)
							/ 2 + sd, pyt(14));
					this.setLocation(
							authorLabel,
							this.getWidth() / 2 + this.getWidth() / 4
									- this.getLabelWidth(authorLabel) / 2 - sd
									* 4, pyt(17));
					this.setLocation(
							pageIndexLabel,
							this.getWidth() / 4
									- this.getLabelWidth(titleLabel) / 2 + sd,
							pyb(83));
					this.setLocation(
							secondaryIndexLabel,
							this.getWidth() / 2 + this.getWidth() / 4
									- this.getLabelWidth(authorLabel) / 2 + sd,
							pyb(83));
				} else {
					this.setLocation(
							titleLabel,
							(this.getWidth() / 2 - this
									.getLabelWidth(titleLabel) / 2) - sd,
							pyt(17));
					this.setLocation(mediaBox, this.getWidth() / 2 - ps(270)
							/ 2 - sd * 2, pyt(14));
					this.authorLabel.setVisibility(View.INVISIBLE);
					this.authorLabel.setVisibility(View.GONE);
					this.secondaryIndexLabel.setVisibility(View.INVISIBLE);
					this.secondaryIndexLabel.setVisibility(View.GONE);
					this.setLocation(
							pageIndexLabel,
							(this.getWidth() / 2 - this
									.getLabelWidth(pageIndexLabel) / 2) - sd,
							pyb(83));
				}
			}
		} else {
			if (this.isPortrait()) { // tablet
				this.setLocation(
						titleLabel,
						(this.getWidth() / 2 - this.getLabelWidth(titleLabel) / 2)
								- sd, pyt(28 + 20));
				this.setLocation(mediaBox, this.getWidth() / 2 - ps(270) / 2
						- sd * 2, pyt(28 + 20));
				this.authorLabel.setVisibility(View.INVISIBLE);
				this.authorLabel.setVisibility(View.GONE);
				this.secondaryIndexLabel.setVisibility(View.INVISIBLE);
				this.secondaryIndexLabel.setVisibility(View.GONE);
				if (this.isHoneycomb()) {
					this.setLocation(
							pageIndexLabel,
							(this.getWidth() / 2 - this
									.getLabelWidth(pageIndexLabel) / 2) - sd,
							pyb(95 + 80));
				} else {
					this.setLocation(
							pageIndexLabel,
							(this.getWidth() / 2 - this
									.getLabelWidth(pageIndexLabel) / 2) - sd,
							pyb(95));
				}
			} else {
				if (this.isDoublePagedForLandscape) {
					this.setLocation(
							titleLabel,
							this.getWidth() / 4
									- this.getLabelWidth(titleLabel) / 2,
							pyt(30));
					this.setLocation(mediaBox, this.getWidth() / 4 - ps(270)
							/ 2, pyt(33));
					this.setLocation(
							authorLabel,
							this.getWidth() / 2 + this.getWidth() / 4
									- this.getLabelWidth(authorLabel) / 2 - sd
									* 4, pyt(30));
					if (this.isHoneycomb()) {
						this.setLocation(pageIndexLabel, this.getWidth() / 4
								- this.getLabelWidth(titleLabel) / 2 + sd,
								pyb(72 + 70));
						this.setLocation(secondaryIndexLabel,
								this.getWidth() / 2 + this.getWidth() / 4
										- this.getLabelWidth(authorLabel) / 2
										+ sd, pyb(72 + 70));
					} else {
						this.setLocation(pageIndexLabel, this.getWidth() / 4
								- this.getLabelWidth(titleLabel) / 2 + sd,
								pyb(62));
						this.setLocation(secondaryIndexLabel,
								this.getWidth() / 2 + this.getWidth() / 4
										- this.getLabelWidth(authorLabel) / 2
										+ sd, pyb(62));
					}
				} else {
					this.setLocation(
							titleLabel,
							(this.getWidth() / 2 - this
									.getLabelWidth(titleLabel) / 2) - sd,
							pyt(27));
					this.setLocation(mediaBox, this.getWidth() / 2 - ps(270)
							/ 2 - sd * 2, pyt(33));
					this.authorLabel.setVisibility(View.INVISIBLE);
					this.authorLabel.setVisibility(View.GONE);
					this.secondaryIndexLabel.setVisibility(View.INVISIBLE);
					this.secondaryIndexLabel.setVisibility(View.GONE);
					if (isHoneycomb()) {
						this.setLocation(
								pageIndexLabel,
								(this.getWidth() / 2 - this
										.getLabelWidth(pageIndexLabel) / 2)
										- sd, pyb(72 + 70));
					} else {
						this.setLocation(
								pageIndexLabel,
								(this.getWidth() / 2 - this
										.getLabelWidth(pageIndexLabel) / 2)
										- sd, pyb(62));
					}
				}
			}
		}
	}

	public void setLabelsText(String title, String author) {
		titleLabel.setText(title);
		authorLabel.setText(author);
	}

	public void setIndexLabelsText(int pageIndex, int pageCount) {
		int pi = 0;
		int si = 0;
		int pc;
		if (rv.isDoublePaged()) {
			pc = pageCount * 2;
			pi = pageIndex * 2 + 1;
			si = pageIndex * 2 + 2;
		} else {
			pc = pageCount;
			pi = pageIndex + 1;
			si = pageIndex + 2;
		}
		String pt = String.format("%3d/%3d", pi, pc);
		String st = String.format("%3d/%3d", si, pc);
		pageIndexLabel.setText(pt);
		secondaryIndexLabel.setText(st);
	}

	class SeekBarDelegate implements OnSeekBarChangeListener {
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			int position = seekBar.getProgress();
			if (seekBar.getId() == 999) {
				stopPlaying();
				double ppb = (double) position / (double) 999;
				rv.gotoPageByPagePositionInBook(ppb);
				hideSeekBox();
			}
		}

		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			if (seekBar.getId() == 999) {
				showSeekBox();
			}
		}

		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// TODO Auto-generated method stub
			if (seekBar.getId() == 999) {
				double ppb = (double) progress / (double) 999.0f;
				PageInformation pi = rv.getPageInformation(ppb);
				moveSeekBox(pi);
			}
			if (seekBar.getId() == 997) {
				setting.brightness = (float) progress / (float) 999.f;
				setBrightness((float) setting.brightness);
			}
			// Log.w("EPub","title "+pi.chapterTitle);
		}
	}

	public void setBrightness(float brightness) {
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.screenBrightness = brightness;
		getWindow().setAttributes(lp);
	}

	public boolean isPortrait() {
		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_PORTRAIT)
			return true;
		else
			return false;
	}

	// this is not 100% accurate function.
	public boolean isTablet() {
		return (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	public int getWidth() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int width = metrics.widthPixels;
		return width;
	}

	public int getHeight() {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int height = metrics.heightPixels;
		return height;
	}

	public void log(String msg) {
		Log.w("EPub", msg);
	}

	// this event is called after device is rotated.
	@Override
	public void onConfigurationChanged(Configuration config) {
		super.onConfigurationChanged(config);
		this.stopPlaying();
		if (this.isPortrait()) {
			log("portrait");
		} else {
			log("landscape");
		}
		this.hideBoxes();
		this.recalcFrames();
	}

	SQLHelper sd;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		sd = new SQLHelper(this);
		setting = sd.fetchSetting();
		progressViewer = new ProgressDialog(this);
		this.makeLayout1();
	}

	private void rotationPressed() {
		isRotationLocked = !isRotationLocked;
		if (isRotationLocked) {
			rv.setRotationLocked(true);

		} else {
			rv.setRotationLocked(false);
		}
		changeRotationButton();
	}

	private void changeRotationButton() {
		Drawable icon;
		String imageName = "icons/rotationLocked@2x.png";
		if (isRotationLocked) {
			imageName = "icons/rotationLocked@2x.png";
		} else {
			imageName = "icons/rotation@2x.png";
		}
		int bs = ps(42);
		icon = this.getDrawableFromAssets(imageName);
		icon.setBounds(0, 0, bs, bs);
		Bitmap iconBitmap = ((BitmapDrawable) icon).getBitmap();
		Bitmap bitmapResized = Bitmap.createScaledBitmap(iconBitmap, bs, bs,
				false);
		rotationButton.setImageBitmap(bitmapResized);
		rv.repaint();
	}

	private void changePlayAndPauseButton() {
		Drawable icon;
		String imageName = "";
		if (!rv.isPlayingStarted() || rv.isPlayingPaused()) {
			imageName = "icons/Play@2x.png";
		} else {
			imageName = "icons/Pause@2x.png";
		}
		int bs = ps(38);
		icon = this.getDrawableFromAssets(imageName);
		icon.setBounds(0, 0, bs, bs);
		Bitmap iconBitmap = ((BitmapDrawable) icon).getBitmap();
		Bitmap bitmapResized = Bitmap.createScaledBitmap(iconBitmap, bs, bs,
				false);
		playAndPauseButton.setImageBitmap(bitmapResized);
	}

	boolean isPagesHidden = false;

	private void hidePages() {
		this.rotationButton.setVisibility(View.INVISIBLE);
		this.fontButton.setVisibility(View.INVISIBLE);
		this.searchButton.setVisibility(View.INVISIBLE);
		this.seekBar.setVisibility(View.INVISIBLE);
		this.pageIndexLabel.setVisibility(View.INVISIBLE);
		this.mediaBox.setVisibility(View.INVISIBLE);

		this.rotationButton.setVisibility(View.GONE);
		this.fontButton.setVisibility(View.GONE);
		this.searchButton.setVisibility(View.GONE);
		this.seekBar.setVisibility(View.GONE);
		this.pageIndexLabel.setVisibility(View.GONE);
		this.mediaBox.setVisibility(View.GONE);

		if (!this.isPortrait() && this.isDoublePagedForLandscape) {
			this.secondaryIndexLabel.setVisibility(View.INVISIBLE);
			this.secondaryIndexLabel.setVisibility(View.GONE);
		}
		rv.hidePages();
		this.showListBox();
	}

	private void showPages() {
		this.rotationButton.setVisibility(View.VISIBLE);
		this.fontButton.setVisibility(View.VISIBLE);
		this.searchButton.setVisibility(View.VISIBLE);
		this.seekBar.setVisibility(View.VISIBLE);
		this.pageIndexLabel.setVisibility(View.VISIBLE);
		if (!this.isPortrait() && this.isDoublePagedForLandscape) {
			this.secondaryIndexLabel.setVisibility(View.VISIBLE);
		}

		if (rv.isMediaOverlayAvailable()) {
			this.mediaBox.setVisibility(View.VISIBLE);
		}
		this.hideListBox();
		rv.showPages();
	}

	private void listPressed() {
		this.stopPlaying();
		if (!isPagesHidden) {
			this.isRotationLocked = false;
			this.rotationPressed();
			this.hidePages();
		} else {
			this.showPages();
			new Handler().postDelayed(new Runnable() {
				public void run() {
					rv.repaint();
				}
			}, 200);
		}
		isPagesHidden = !isPagesHidden;
	}

	private void fontPressed() {
		this.stopPlaying();
		this.showFontBox();
	}

	private void searchPressed() {
		this.stopPlaying();
		this.showSearchBox();
	}

	public void gotoPageBySearchResult(SearchResult sr) {
		// showToast(sr.text);
		rv.gotoPageBySearchResult(sr);
	}

	private OnClickListener listener = new OnClickListener() {
		public void onClick(View arg) {
			if (arg.getId() == 8080) {
				playPrev();
			} else if (arg.getId() == 8081) {
				playAndPause();
			} else if (arg.getId() == 8082) {
				stopPlaying();
			} else if (arg.getId() == 8083) {
				playNext();
			} else if (arg.getId() == 8084) {
				finish();
			}

			if (arg.getId() == 3001) {
				cancelPressed();
			} else if (arg.getId() == 3093) {
				// search More
				removeLastResult();
				// showToast("Search More...");
				rv.searchMore();
			} else if (arg.getId() == 3094) {
				removeLastResult();
				hideSearchBox();
				// stopSearch
			}

			if (arg.getId() == 9000) { // homePressed
				rotationPressed();
			} else if (arg.getId() == 9001 || arg.getId() == 9009) { // listPressed
				listPressed();
			} else if (arg.getId() == 9002) { // fontPressed
				fontPressed();
			} else if (arg.getId() == 9003) { // searchPressed
				searchPressed();
			}

			if (arg.getId() == 6000) {
				// highlightMenuButton
				mark();
				hideMenuBox();
				showHighlightBox();
			} else if (arg.getId() == 6001) {
				mark();
				hideMenuBox();
				showNoteBox();
			}

			if (arg.getId() == 6002) {
				// Color Chooser
				hideHighlightBox();
				showColorBox();
			} else if (arg.getId() == 6003) {
				hideHighlightBox();
				rv.deleteHighlight(currentHighlight);
			} else if (arg.getId() == 6004) {
				hideHighlightBox();
				showNoteBox();
			}

			int color;
			if (arg.getId() == 6010) {
				color = getColorByIndex(0);
				changeHighlightColor(currentHighlight, color);
			} else if (arg.getId() == 6011) {
				color = getColorByIndex(1);
				changeHighlightColor(currentHighlight, color);
			} else if (arg.getId() == 6012) {
				color = getColorByIndex(2);
				changeHighlightColor(currentHighlight, color);
			} else if (arg.getId() == 6013) {
				color = getColorByIndex(3);
				changeHighlightColor(currentHighlight, color);
			}

			if (arg.getId() == 9999) {
				hideOutsideButton();
				hideBoxes();
			}

			// click on the search result
			if (arg.getId() >= 100000 && arg.getId() < 200000) {
				int index = arg.getId() - 100000;
				hideSearchBox();
				SearchResult sr = searchResults.get(index);
				gotoPageBySearchResult(sr);
			}

			// fonts related
			if (arg.getId() == 5000) {
				// decrease button
				decreaseFont();
			} else if (arg.getId() == 5001) {
				// increase button
				increaseFont();
			} else if (arg.getId() >= 5100 && arg.getId() < 5500) {
				// one of fontButtons is clicked.
				fontSelected(arg.getId() - 5100);
			}

			// list processing
			if (arg.getId() == 2700) {
				checkListButton(0);
			} else if (arg.getId() == 2701) {
				checkListButton(1);
			} else if (arg.getId() == 2702) {
				checkListButton(2);
			}

			// list processing
			if (arg.getId() >= 200000 && arg.getId() < 300000) { // click on the
																	// one of
																	// contents

			} else if (arg.getId() >= 300000 && arg.getId() < 400000) { // click
																		// on
																		// the
																		// bookmark
																		// of
																		// bookmark
																		// list

			} else if (arg.getId() >= 400000 && arg.getId() < 500000) { // click
																		// on
																		// the
																		// highlight
																		// of
																		// highlight
																		// list

			}
		}
	};

	int getRealFontSize(int fontSizeIndex) {
		int rs = 0;
		switch (fontSizeIndex) {
		case 0:
			rs = 24;
			break;
		case 1:
			rs = 27;
			break;
		case 2:
			rs = 30;
			break;
		case 3:
			rs = 34;
			break;
		case 4:
			rs = 37;
			break;
		default:
			rs = 27;
		}
		return rs;
	}

	public void checkFonts() {
		if (this.setting.fontSize == 0) {
			decreaseButton.setTextColor(Color.LTGRAY);
		} else {
			decreaseButton.setTextColor(Color.BLACK);
		}

		if (this.setting.fontSize == 4) {
			increaseButton.setTextColor(Color.LTGRAY);
		} else {
			increaseButton.setTextColor(Color.BLACK);
		}

		int fontIndex = this.getFontIndex(setting.fontName);
		for (int i = 0; i < fontListView.getChildCount(); i++) {
			Button button = (Button) fontListView.getChildAt(i);
			button.setTextColor(Color.BLACK);

		}
		for (int i = 0; i < fontListView.getChildCount(); i++) {
			Button button = (Button) fontListView.getChildAt(i);
			if (button.getId() == (fontIndex + 5100)) {
				button.setTextColor(Color.BLUE);
			}
		}
	}

	public void decreaseFont() {
		if (this.setting.fontSize != 0) {
			this.setting.fontSize--;
			rv.changeFont(setting.fontName,
					this.getRealFontSize(setting.fontSize));
		}
		this.checkFonts();
	}

	public void increaseFont() {
		if (this.setting.fontSize != 4) {
			this.setting.fontSize++;
			rv.changeFont(setting.fontName,
					this.getRealFontSize(setting.fontSize));
		}
		this.checkFonts();
	}

	public void fontSelected(int index) {
		String name = this.getFontName(index);
		if (!setting.fontName.equalsIgnoreCase(name)) {
			setting.fontName = name;
			checkFonts();
			rv.changeFont(setting.fontName,
					this.getRealFontSize(setting.fontSize));
		}
	}

	public void changeHighlightColor(Highlight highlight, int color) {
		currentHighlight.color = color;
		rv.changeHighlightColor(currentHighlight, color);
		this.hideColorBox();
	}

	private void mark() {
		rv.markSelection(currentColor, "");
	}

	private void showToast(String msg) {
		Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
		toast.show();
	}

	void hideBoxes() {
		this.hideColorBox();
		this.hideHighlightBox();
		this.hideMenuBox();
		this.hideNoteBox();
		this.hideSearchBox();
		this.hideFontBox();
		this.hideListBox();
		if (isPagesHidden)
			this.showPages();
	}

	public void reportMemory() {
		Runtime rt = Runtime.getRuntime();
		long maxMemory = rt.maxMemory();

		// Log.v("EPub", "maxMemory:" + Long.toString(maxMemory));

		ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		int memoryClass = am.getMemoryClass();
		// Log.v("EPub", "memoryClass:" + Integer.toString(memoryClass));

		ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		MemoryInfo memoryInfo = new MemoryInfo();
		activityManager.getMemoryInfo(memoryInfo);
		// Log.i("EPub", "AvailMem" + memoryInfo.availMem);

		long memoryAvail = memoryInfo.availMem;
		long memoryAlloc = maxMemory - memoryAvail;

		String message = String.format("Max :%d Avail:%d", maxMemory,
				memoryAvail);
		showToast(message);
	}

	public void test02() {
		String str = "Capitulo%205%20EL%20PAPEL%20DE%20LOS%20CORTICOIDES.xhtml";
		String res = null;
		try {
			res = URLDecoder.decode(str, "UTF-8");
		} catch (Exception e) {
		}
		// try {
		// res = java.net.URLEncoder.encode(str, "UTF-8");
		// }catch(Exception e) {}
		showToast(res);
	}

	class ClickDelegate implements ClickListener {
		public void onClick(int x, int y) {
			if (isBoxesShown) {
				// hideBoxes();
			} else {
				toggleControls();
				// reportMemory();
				// test02();
				// reportFiles("/data/data/com.skytree.epubtest/files/books");
			}

		}

		public void onImageClicked(int x, int y, String src) {
			showToast("Image Clicked at " + x + ":" + y + " src:" + src);
			Log.w("EPub", "Click on Image Detected at " + x + ":" + y + " src:"
					+ src);
		}

		public void onLinkClicked(int x, int y, String href) {
			showToast("Link Clicked at " + x + ":" + y + " href:" + href);
			Log.w("EPub", "Link Clicked at " + x + ":" + y + " href:" + href);
		}
	}

	class StateDelegate implements StateListener {
		public void onStateChanged(State state) {
			if (state == State.LOADING) {
				showIndicator();
				// showToast("Loading...");
				// Context context = getApplicationContext();
				// if (context != null) {
				// dialog = ProgressDialog.show(context, "","Loading", true);
				// }
				// ProgressDialog dialog =
				// ProgressDialog.show(getApplicationContext(),
				// "","Please wait for few seconds...", true);
			} else if (state == State.ROTATING) {
				// showToast("Rotating...");
			} else if (state == State.BUSY) {
				showIndicator();
				// showToast("Busy...");
			} else if (state == State.NORMAL) {
				// showToast("Normal...");
				hideIndicator();
				// if (dialog!=null) dialog.dismiss();
				// dialog = null;
			}
		}
	}

	int getColorByIndex(int colorIndex) {
		int color;
		if (colorIndex == 0) {
			color = Color.argb(255, 238, 230, 142);
		} else if (colorIndex == 1) {
			color = Color.argb(255, 218, 244, 160);
		} else if (colorIndex == 2) {
			color = Color.argb(255, 172, 201, 246);
		} else if (colorIndex == 3) {
			color = Color.argb(255, 249, 182, 214);
		} else {
			color = Color.argb(255, 249, 182, 214);
		}
		return color;
	}

	int getIndexByColor(int color) {
		int index;
		if (color == Color.argb(255, 238, 230, 142)) {
			index = 0;
		} else if (color == Color.argb(255, 218, 244, 160)) {
			index = 1;
		} else if (color == Color.argb(255, 172, 201, 246)) {
			index = 2;
		} else if (color == Color.argb(255, 249, 182, 214)) {
			index = 3;
		} else {
			index = 0;
		}
		return index;
	}

	class HighlightDelegate implements HighlightListener {
		public void onHighlightDeleted(Highlight highlight) {
			sd.deleteHighlight(highlight);
		}

		public void onHighlightInserted(Highlight highlight) {
			sd.insertHighlight(highlight);
		}

		public void onHighlightHit(Highlight highlight, int x, int y,
				Rect startRect, Rect endRect) {
			debug("onHighlightHit at " + highlight.text);
			currentHighlight = highlight;
			currentColor = currentHighlight.color;
			showHighlightBox(startRect, endRect);
		}

		public Highlights getHighlightsForChapter(int chapterIndex) {
			return sd.fetchHighlights(bookCode, chapterIndex);
		}

		@Override
		public void onHighlightUpdated(Highlight highlight) {
			// TODO Auto-generated method stub
			sd.updateHighlight(highlight);
		}

		@Override
		public Bitmap getNoteIconBitmapForColor(int color) {
			// TODO Auto-generated method stub
			Drawable icon;
			Bitmap iconBitmap;
			int index = getIndexByColor(color);
			if (index == 0) {
				icon = getDrawableFromAssets("icons/yellowMemo@2x.png");
			} else if (index == 1) {
				icon = getDrawableFromAssets("icons/greenMemo@2x.png");
			} else if (index == 2) {
				icon = getDrawableFromAssets("icons/blueMemo@2x.png");
			} else if (index == 3) {
				icon = getDrawableFromAssets("icons/redMemo@2x.png");
			} else {
				icon = getDrawableFromAssets("icons/yellowMemo@2x.png");
			}
			iconBitmap = ((BitmapDrawable) icon).getBitmap();
			return iconBitmap;
		}

		@Override
		public void onNoteIconHit(Highlight highlight) {
			// TODO Auto-generated method stub
			if (isBoxesShown) {
				hideBoxes();
				return;
			}
			currentHighlight = highlight;
			currentColor = highlight.color;
			showNoteBox();
		}

		@Override
		public Rect getNoteIconRect() {
			// TODO Auto-generated method stub
			// only width, height will be used for the engine.
			Rect rect = new Rect(0, 0, ps(32), ps(32));
			return rect;
		}
	}

	class BookmarkDelegate implements BookmarkListener {
		@Override
		public void onBookmarkHit(PageInformation pi, boolean isBookmarked) {
			// TODO Auto-generated method stub
			sd.toggleBookmark(pi);
			rv.repaint();
		}

		@Override
		public Rect getBookmarkRect(boolean isBookmarked) {
			// TODO Auto-generated method stub
			if (isBookmarked) {
				return bookmarkedRect;
			} else {
				return bookmarkRect;
			}
		}

		@Override
		public Bitmap getBookmarkBitmap(boolean isBookmarked) {
			// TODO Auto-generated method stub
			Drawable markIcon;
			Bitmap iconBitmap;
			if (isBookmarked) {
				markIcon = getDrawableFromAssets("icons/bookmarked@2x.png");
			} else {
				markIcon = getDrawableFromAssets("icons/bookmark@2x.png");
			}
			iconBitmap = ((BitmapDrawable) markIcon).getBitmap();
			return iconBitmap;
		}

		@Override
		public boolean isBookmarked(PageInformation pi) {
			// TODO Auto-generated method stub
			return sd.isBookmarked(pi);
		}
	}

	class PagingDelegate implements PagingListener {
		public void onPagingStarted(int bookCode) {
			showToast("Global Pagination Started.");
		}

		public void onPaged(PagingInformation pagingInformation) {
			pagings.add(pagingInformation);
			showToast("Paging for " + pagingInformation.chapterIndex
					+ " is Finished. The numberOfPages :"
					+ pagingInformation.numberOfPagesInChapter);
		}

		public void onPagingFinished(int bookCode) {
			showToast("Global Pagination Finished.");
		}

		public int getNumberOfPagesForPagingInformation(
				PagingInformation pagingInformation) {
			for (int i = 0; i < pagings.size(); i++) {
				PagingInformation pgi = pagings.get(i);
				if (pgi.equals(pagingInformation)) {
					// Log.w("EPub","PagingInformation found !!");
					return pgi.numberOfPagesInChapter;
				}
			}
			return 0;
		}
	}

	class PageMovedDelegate implements PageMovedListener {
		public void onPageMoved(PageInformation pi) {
			String msg = String
					.format("title %s chapterIndex:%d pageIndex:%d numberOfPages :%d positionInBook:%f    ",
							pi.chapterTitle, pi.chapterIndex, pi.pageIndex,
							pi.numberOfPagesInChapter, pi.pagePositionInBook);
			// Log.w("EPub",msg);
			double ppb = pi.pagePositionInBook;
			int progress = (int) ((double) 999.0f * (ppb));
			seekBar.setProgress(progress);
			// setLabelsText(, author);
			setIndexLabelsText(pi.pageIndex, pi.numberOfPagesInChapter);
			pagePositionInBook = (float) pi.pagePositionInBook;
		}

		public void onChapterLoaded(int chapterIndex) {
			if (rv.isMediaOverlayAvailable()) {
				showMediaBox();
				if (autoStartPlayingWhenNewPagesLoaded) {
					if (isAutoPlaying)
						rv.playFirstParallelInPage();
				}
			} else {
				hideMediaBox();
			}
		}
	}

	int numberOfSearched = 0;
	int ms = 10;

	class SearchDelegate implements SearchListener {
		public void onKeySearched(SearchResult searchResult) {
			addSearchResult(searchResult, 0);
			// debug("chapterIndex"+searchResult.chapterIndex+" pageIndex:" +
			// searchResult.pageIndex + " startOffset:"
			// + searchResult.startOffset + " tag:" + searchResult.nodeName
			// + " text:" + searchResult.text);

		}

		public void onSearchFinishedForChapter(SearchResult searchResult) {
			if (searchResult.numberOfSearchedInChapter != 0) {
				addSearchResult(searchResult, 1);
				debug("Searching for Chapter:" + searchResult.chapterIndex
						+ " is finished. ");
				rv.pauseSearch();
				numberOfSearched = searchResult.numberOfSearched;
			} else {
				rv.searchMore();
				numberOfSearched = searchResult.numberOfSearched;
			}
		}

		public void onSearchFinished(SearchResult searchResult) {
			debug("Searching is finished. ");
			addSearchResult(searchResult, 2);
		}
	}

	class SelectionDelegate implements SelectionListener {
		// startRect is the first rectangle for selection area
		// endRect is the last rectable for selection area.
		// highlight holds information for selected area.
		public void selectionStarted(Highlight highlight, Rect startRect,
				Rect endRect) {
			hideMenuBox();
		}; // in case user touches down selection bar, normally hide custom

		public void selectionChanged(Highlight highlight, Rect startRect,
				Rect endRect) {
			hideMenuBox();
		}; // this may happen when user dragging selection.

		public void selectionEnded(Highlight highlight, Rect startRect,
				Rect endRect) {
			currentHighlight = highlight;
			showMenuBox(startRect, endRect);
		}; // in case user touches up selection bar,custom menu view has to be
			// shown near endX,endY.

		public void selectionCancelled() {
			hideMenuBox();
		} // selection cancelled by user.
	}

	public void debug(String msg) {
		if (Setting.isDebug()) {
			Log.d(Setting.getTag(), msg);
		}
	}

	class MediaOverlayDelegate implements MediaOverlayListener {
		@Override
		public void onParallelStarted(Parallel parallel) {
			// TODO Auto-generated method stub
			// Log.w("EPub","onParallelStarted");
			currentParallel = parallel;
			if (rv.pageIndexInChapter() != parallel.pageIndex) {
				if (autoMoveChapterWhenParallesFinished)
					rv.gotoPageInChapter(parallel.pageIndex);
			}
			rv.changeElementColor("#FFFF00", parallel.hash);
		}

		@Override
		public void onParallelEnded(Parallel parallel) {
			// TODO Auto-generated method stub
			// Log.w("EPub","onParallelEnded");
			rv.restoreElementColor();
		}

		@Override
		public void onParallelsEnded() {
			// TODO Auto-generated method stub
			rv.restoreElementColor();
			if (autoStartPlayingWhenNewPagesLoaded)
				isAutoPlaying = true;
			if (autoMoveChapterWhenParallesFinished) {
				// Log.w("EPub","onParallesEnded");
				rv.gotoNextChapter();
			}
		}
	}

	void playAndPause() {
		if (rv.isPlayingPaused()) {
			if (!rv.isPlayingStarted()) {
				rv.playFirstParallelInPage();
				if (autoStartPlayingWhenNewPagesLoaded)
					isAutoPlaying = true;
			} else {
				rv.resumePlayingParallel();
				if (autoStartPlayingWhenNewPagesLoaded)
					isAutoPlaying = true;
			}

		} else {
			rv.pausePlayingParallel();
			if (autoStartPlayingWhenNewPagesLoaded)
				isAutoPlaying = false;
		}
		this.changePlayAndPauseButton();
	}

	void stopPlaying() {
		// [button1 setTitle:@"Play" forState:UIControlStateNormal];
		rv.stopPlayingParallel();
		rv.restoreElementColor();
		if (autoStartPlayingWhenNewPagesLoaded)
			isAutoPlaying = false;
		this.changePlayAndPauseButton();
	}

	void playPrev() {
		rv.playPrevParallel();
	}

	void playNext() {
		rv.playNextParallel();
	}

	@Override
	protected void onPause() {
		super.onPause();
		// log("onPause() in BookViewActivity");
		sd.updatePosition(bookCode, pagePositionInBook);
		sd.updateSetting(setting);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// log("onStop() in BookViewActivity");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		// log("onRestart() in BookViewActivity");
	}

	@Override
	protected void onResume() {
		super.onResume();
		// log("onResume() in BookViewActivity");
	}

	@Override
	protected void onStart() {
		super.onStart();
		// log("onStart() in BookViewActivity");
	}

	class BookViewer extends AsyncTask<ReflowableControl, String, String> {

		Context context;
		String str;

		public BookViewer(String strg) {
			str = strg;
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			progressViewer.setMessage("Loading...");
			progressViewer.show();

		}

		@Override
		protected String doInBackground(ReflowableControl... objs) {
			ReflowableControl rv = objs[0];
			rv.setBaseDirectory(str);
			rv.setLayoutParams(params);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
		}

	}

}

interface SkyLayoutListener {
	void onShortPress(SkyLayout view, MotionEvent e);

	void onLongPress(SkyLayout view, MotionEvent e);

	void onSingleTapUp(SkyLayout view, MotionEvent e);

	void onSwipeToLeft(SkyLayout view);

	void onSwipeToRight(SkyLayout view);
}

class SkyLayout extends RelativeLayout implements
		android.view.GestureDetector.OnGestureListener {
	public Object data;
	public View editControl;
	private GestureDetector gestureScanner;
	private static final int SWIPE_MIN_DISTANCE = 50;
	private static final int SWIPE_MAX_OFF_PATH = 1024;
	private static final int SWIPE_THRESHOLD_VELOCITY = 50;

	private SkyLayoutListener skyLayoutListener = null;

	public SkyLayout(Context context) {
		super(context);
		gestureScanner = new GestureDetector(this);
	}

	public void setSkyLayoutListener(SkyLayoutListener sl) {
		this.skyLayoutListener = sl;
	}

	@Override
	public boolean onTouchEvent(MotionEvent me) {
		return gestureScanner.onTouchEvent(me);
	}

	public boolean onDown(MotionEvent e) {
		return true;
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		try {
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;

			// right to left swipe
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				// Toast.makeText(getContext(), "Left Swipe",
				// Toast.LENGTH_SHORT).show();
				if (this.skyLayoutListener != null) {
					skyLayoutListener.onSwipeToLeft(this);
				}
			}
			// left to right swipe
			else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				// Toast.makeText(getContext(), "Right Swipe",
				// Toast.LENGTH_SHORT).show();
				if (this.skyLayoutListener != null) {
					skyLayoutListener.onSwipeToRight(this);
				}
			}
			// down to up swipe
			else if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
				// Toast.makeText(getContext(), "Swipe up",
				// Toast.LENGTH_SHORT).show();
			}
			// up to down swipe
			else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
				// Toast.makeText(getContext(), "Swipe down",
				// Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {

		}
		return true;
	}

	public void onLongPress(MotionEvent e) {
		if (this.skyLayoutListener != null) {
			this.skyLayoutListener.onLongPress(this, e);
		}
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return true;
	}

	public void onShowPress(MotionEvent e) {
		if (this.skyLayoutListener != null) {
			this.skyLayoutListener.onShortPress(this, e);
		}
	}

	public boolean onSingleTapUp(MotionEvent e) {
		if (this.skyLayoutListener != null) {
			this.skyLayoutListener.onSingleTapUp(this, e);
		}
		return true;
	}
}

class SkyBox extends RelativeLayout {
	public boolean isArrowDown;
	int boxColor;
	int strokeColor;
	public float arrowPosition;
	float boxX, boxWidth;
	public float arrowHeight;
	RelativeLayout contentView;
	boolean layoutChanged;

	public SkyBox(Context context) {
		super(context);
		this.setWillNotDraw(false);
		arrowHeight = 50;
		boxColor = Color.YELLOW;
		strokeColor = Color.DKGRAY;
		contentView = new RelativeLayout(context);
		this.addView(contentView);
	}

	public void setArrowDirection(boolean isArrowDown) {
		this.isArrowDown = isArrowDown;
		layoutChanged = true;
	}

	public void setArrowHeight(float arrowHeight) {
		this.arrowHeight = arrowHeight;
		layoutChanged = true;
	}

	public int getDarkerColor(int color) {
		float[] hsv = new float[3];
		Color.colorToHSV(color, hsv);
		hsv[2] *= 0.8f; // value component
		int darker = Color.HSVToColor(hsv);
		return darker;
	}

	public int getBrighterColor(int color) {
		float[] hsv = new float[3];
		Color.colorToHSV(color, hsv);
		hsv[2] *= 1.2f; // value component
		int darker = Color.HSVToColor(hsv);
		return darker;
	}

	public void setBoxColor(int boxColor) {
		this.boxColor = boxColor;
		this.strokeColor = this.getDarkerColor(boxColor);
	}

	public void setArrowPosition(int arrowX, int boxLeft, int boxWidth) {
		this.boxX = boxLeft;
		this.boxWidth = boxWidth;
		this.arrowPosition = arrowX - boxX;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		return true;
	}

	private void recalcLayout() {
		RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT); // width,height
		param.leftMargin = 0;
		if (this.isArrowDown) {
			param.topMargin = 0;
		} else {
			param.topMargin = (int) this.arrowHeight;
		}
		param.width = this.getWidth();
		param.height = this.getHeight() - (int) this.arrowHeight + 10;
		contentView.setLayoutParams(param);
	}

	@SuppressLint({ "DrawAllocation", "DrawAllocation" })
	@Override
	protected void onDraw(Canvas canvas) {
		Paint paint = new Paint();

		float sl, sr, st, sb;
		sl = 0;
		sr = this.getWidth();
		float ah = this.arrowHeight; // arrow Height;
		if (this.isArrowDown) {
			st = 0;
			sb = this.getHeight() - ah;
		} else {
			st = ah;
			sb = this.getHeight();
		}

		Path boxPath = new Path();
		boxPath.addRoundRect(new RectF(sl, st, sr, sb), 20, 20,
				Path.Direction.CW);

		if (arrowPosition <= arrowHeight * 1.5f) {
			arrowPosition = arrowHeight * 1.5f;
		} else if (arrowPosition >= this.getWidth() - arrowHeight * 1.5f) {
			arrowPosition = this.getWidth() - arrowHeight * 1.5f;
		}

		Path arrowPath = new Path();
		if (isArrowDown) {
			arrowPath.moveTo(arrowPosition, sb + ah);
			arrowPath.lineTo((float) (arrowPosition - ah * 0.75), sb - 10);
			arrowPath.lineTo((float) (arrowPosition + ah * 0.75), sb - 10);
			arrowPath.close();
		} else {
			arrowPath.moveTo(arrowPosition, 0);
			arrowPath.lineTo((float) (arrowPosition - ah * 0.75), ah + 10);
			arrowPath.lineTo((float) (arrowPosition + ah * 0.75), ah + 10);
			arrowPath.close();
		}

		paint.setColor(this.strokeColor);
		paint.setStyle(Paint.Style.FILL);
		boxPath.addPath(arrowPath);
		canvas.drawPath(boxPath, paint);

		paint.setColor(this.boxColor);
		paint.setStyle(Paint.Style.FILL);
		boxPath.addPath(arrowPath);
		canvas.save();
		float sf = 0.995f;
		float ox = (this.getWidth() - (this.getWidth() * sf)) / 2.0f;
		float oy = ((this.getHeight() - arrowHeight) - ((this.getHeight() - arrowHeight) * sf)) / 2.0f;

		canvas.translate(ox, oy);
		canvas.scale(sf, sf);
		canvas.drawPath(boxPath, paint);
		canvas.restore();

		if (layoutChanged) {
			this.recalcLayout();
			layoutChanged = false;
		}
	}
}

class DottedDrawable extends Drawable {
	private Paint mPaint;

	public DottedDrawable() {
		mPaint = new Paint();
		mPaint.setStrokeWidth(3);
	}

	@Override
	protected boolean onLevelChange(int level) {
		invalidateSelf();
		return true;
	}

	@Override
	public void setAlpha(int alpha) {
	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSLUCENT;
	}

	@Override
	public void draw(Canvas canvas) {
		// TODO Auto-generated method stub
		int lvl = getLevel();
		Rect b = getBounds();
		float x = b.width() * lvl / 10000.0f;
		float y = (b.height() - mPaint.getStrokeWidth()) / 2;
		mPaint.setColor(Color.argb(120, 160, 124, 95));
		mPaint.setStyle(Paint.Style.FILL);
		for (int cx = 10; cx < b.width(); cx += 30) {
			canvas.drawCircle(cx, y, 4, mPaint);
		}
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
		// TODO Auto-generated method stub

	}
}

class LineDrawable extends Drawable {
	private Paint mPaint;
	private int mColor;
	private int mStrokeWidth;

	public LineDrawable(int color, int strokeWidth) {
		mPaint = new Paint();
		mPaint.setStrokeWidth(3);
		mColor = color;
		mStrokeWidth = strokeWidth;
	}

	@Override
	protected boolean onLevelChange(int level) {
		invalidateSelf();
		return true;
	}

	@Override
	public void setAlpha(int alpha) {
	}

	public void setColor(int color) {
		this.mColor = color;
	}

	public void setStokeWidth(int strokeWidth) {
		mStrokeWidth = strokeWidth;
	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSLUCENT;
	}

	@Override
	public void draw(Canvas canvas) {
		// TODO Auto-generated method stub
		Rect b = getBounds();
		mPaint.setColor(mColor);
		mPaint.setStrokeWidth(mStrokeWidth);
		mPaint.setStyle(Paint.Style.FILL);
		canvas.drawLine(0, b.height() / 2 + b.height() * 0.1f, b.width(),
				b.height() / 2 + b.height() * .1f, mPaint);
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
		// TODO Auto-generated method stub

	}
}

class ContentHandler implements ContentListener {
	public long getLength(String baseDirectory, String contentPath) {
		String path = baseDirectory + "/" + contentPath;
		File file = new File(path);
		if (file.exists())
			return file.length();
		else
			return 0;
	}

	public boolean isExists(String baseDirectory, String contentPath) {
		String path = baseDirectory + "/" + contentPath;
		File file = new File(path);
		boolean res = false;
		if (file.exists())
			res = true;
		else
			res = false;
		return res;
	}

	public long getLastModified(String baseDirectory, String contentPath) {
		String path = baseDirectory + "/" + contentPath;
		File file = new File(path);
		if (file.exists())
			return file.lastModified();
		else
			return 0;
	}

	public InputStream getInputStream(String baseDirectory, String contentPath) {
		String path = baseDirectory + "/" + contentPath;
		File file = new File(path);
		try {
			FileInputStream fis = new FileInputStream(file);
			return fis;
		} catch (Exception e) {
			return null;
		}
	}

}
