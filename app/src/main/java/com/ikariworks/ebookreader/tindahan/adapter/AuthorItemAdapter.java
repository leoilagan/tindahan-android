package com.ikariworks.ebookreader.tindahan.adapter;

import java.util.ArrayList;


import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.data.Author;

import com.ikariworks.ebookreader.tindahan.util.ImageLoader;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import android.widget.TextView;

public class AuthorItemAdapter extends BaseAdapter   {

	ArrayList<Author> items;
	Context context;
	ImageLoader imageLoader;

	public AuthorItemAdapter(ArrayList<Author> items, Context context) {
		this.context = context;
		this.items = items;
		imageLoader = new ImageLoader(context);
	}

	@Override
	public int getCount() {

		return items.size();
	}

	@Override
	public Author getItem(int index) {

		return items.get(index);
	}

	@Override
	public long getItemId(int arg0) {

		return 0;
	}

	
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		View layout = convertView;
		if (layout == null) {
			layout = View.inflate(context, R.layout.fragment_home_authors_item,
					null);
		}
		TextView txt = (TextView) layout.findViewById(R.id.txtAuthor);
		txt.setText(getItem(position).getName());
		txt = (TextView) layout.findViewById(R.id.txtNumbers);
		int numBooks = getItem(position).numBooks();
		if (numBooks > 1)
			txt.setText(getItem(position).numBooks() + " Books");
		else {
			txt.setText(getItem(position).numBooks() + " Book");
		}

		return layout;
	}

	

	

}
