package com.ikariworks.ebookreader.tindahan.fragments;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.adapter.ExpandableListAdapter;
import com.ikariworks.ebookreader.tindahan.adapter.ListItemAdapter;
import com.ikariworks.ebookreader.tindahan.ui.MainActivity;

public class FAQFragment extends BaseFragment implements OnItemClickListener {

	View localView;
	ViewFlipper viewFlipper;
	public static String TAG = "FAQFRAGMENT";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		localView = inflater.inflate(R.layout.fragment_faq, container, false);
		String[] arry = getActivity().getResources().getStringArray(
				R.array.faq_array);
		ListView lstView = (ListView) localView.findViewById(R.id.lstView);
		ArrayList<String> lstArray = new ArrayList<String>(Arrays.asList(arry));
		lstView.setAdapter(new ListItemAdapter(lstArray, getActivity()));
		lstView.setOnItemClickListener(this);
		viewFlipper = (ViewFlipper)localView.findViewById(R.id.flip_faq_main);
		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(),
                  android.R.anim.slide_in_left));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(),
                  android.R.anim.slide_out_right));
		initContent();
		return localView;
	}
	
	
	private void initContent(){
		ExpandableListView expandlistView = (ExpandableListView)localView.findViewById(R.id.lstExBanks);
		String[] arryList = getActivity().getResources().getStringArray(R.array.paypurchase_method);
		ExpandableListAdapter adapter = new ExpandableListAdapter((MainActivity) getActivity(), arryList);
		expandlistView.setAdapter(adapter);
	}
	
	
	@Override
	public void onAttach(Activity activity) {
		
		super.onAttach(activity);
		try {
			mListener = (OnFragmentSelectListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentSelectListener");
		}
	}

	
	public void backToHome(){
		viewFlipper.setDisplayedChild(0);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		if (position == 0) {
			viewFlipper.setDisplayedChild(1);
			mListener.selectOnItemClicked(TAG,R.id.lstView, position);
		} else if (position == 1) {
			viewFlipper.setDisplayedChild(2);
			mListener.selectOnItemClicked(TAG,R.id.lstView, position);
		}

	}

}
