package com.ikariworks.ebookreader.tindahan.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class DBContentProvider extends ContentProvider {
	public static final String AUTHORITY = "com.ikariworks.ebookreader.tindahan.db.DBContentProvider";

	public static final Uri BOOK_URI = Uri.parse("content://" + AUTHORITY
			+ "/"+DBTables.BOOK_TABLE);
	public static final Uri DEVICE_URI = Uri.parse("content://" + AUTHORITY
			+ "/"+DBTables.DEVICE_TABLE);
	
	private static final UriMatcher uriMatcher;

	DatabaseHelper dbHelper;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY, DBTables.BOOK_TABLE, DBTables.BOOK);
		uriMatcher.addURI(AUTHORITY, DBTables.DEVICE_TABLE, DBTables.DEVICES);
	}

	@Override
	public boolean onCreate() {
		dbHelper = new DatabaseHelper(getContext());
		return (dbHelper == null) ? false : true;

	}



	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues initialValues) {
		ContentValues values;
		if (initialValues != null) {
			values = new ContentValues(initialValues);
		} else {
			values = new ContentValues();
		}

		String table_name = DBTables.BOOK_TABLE;
		Uri selectedUri = BOOK_URI;
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		switch (uriMatcher.match(uri)) {
		case DBTables.BOOK:
			table_name = DBTables.BOOK_TABLE;
			selectedUri = BOOK_URI;
			break;
		case DBTables.DEVICES:
			table_name = DBTables.DEVICE_TABLE;
			selectedUri = DEVICE_URI;
			break;	
		
		}
		long rowId = db.insert(table_name, null, values);
		if (rowId > 0) {
			Uri noteUri = ContentUris.withAppendedId(selectedUri, rowId);
			getContext().getContentResolver().notifyChange(noteUri, null);
			return noteUri;
		}
		throw new SQLException("Failed to insert row into " + uri);

	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
      
		switch (uriMatcher.match(uri)) {
		case DBTables.BOOK:
			qb.setTables(DBTables.BOOK_TABLE);
			break;
		case DBTables.DEVICES:
			qb.setTables(DBTables.DEVICE_TABLE);
			break;	
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = qb.query(db, projection, selection, selectionArgs, null,
				null, sortOrder);
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues initialValues, String selection,
			String[] selectionArgs) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values;
		int count = 0;
		if (initialValues != null) {
			values = new ContentValues(initialValues);
		} else {
			values = new ContentValues();
		}
		switch (uriMatcher.match(uri)) {
		case DBTables.BOOK:
			count = db.update(DBTables.BOOK_TABLE, values, selection,
					selectionArgs);
		case DBTables.DEVICES:
			count = db.update(DBTables.DEVICE_TABLE, values, selection,
					selectionArgs);	
			
			break;
			
		}

		return count;

	}

}
