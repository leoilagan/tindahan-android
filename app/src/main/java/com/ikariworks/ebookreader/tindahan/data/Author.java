package com.ikariworks.ebookreader.tindahan.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Author {
	String name;
    Map<String, Book> hashBooks;
    
    public Author() {
	    hashBooks = new HashMap<String, Book>();
	}
    
    public void setName(String name) {
		this.name = name;
	}
    public String getName() {
		return name;
	}
    
    public void addBook(Book book){
    	if(!hashBooks.containsKey(book.getId())){
    		hashBooks.put(book.getId(), book);
    	}
    }
    
    public int numBooks(){
    	return hashBooks.size();
    }
    
    public ArrayList<Book> getAllBooks(){
    	ArrayList<Book> bookList = 
    			Collections.list(Collections.enumeration(hashBooks.values()));
    	return bookList;
    }

	
	
    
}
