package com.ikariworks.ebookreader.tindahan.net.requestor;

import java.net.URLEncoder;

import android.os.Bundle;
import android.util.Log;

import com.ikariworks.ebookreader.tindahan.net.command.APIRequestCallBack;
import com.ikariworks.ebookreader.tindahan.net.extended.ExtendedJsonHttpResponseHandler;
import com.ikariworks.ebookreader.tindahan.util.Consts;

public class APIRequester extends GenericRequestor {

	APIRequestCallBack command;

	public APIRequester(APIRequestCallBack command) {
		this.command = command;
	}

	public void login(String userName, String password) {
		try {
			// http://127.0.0.1/tindahan/route_dummy.php?uid=test%40email.com&pwd=pass1234&mode=signin&Submit=Submit
			/*
			 * RequestParams params = new RequestParams(); params.put("uid",
			 * userName); params.put("pwd", password); params.put("mode",
			 * "signin");
			 */

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(Consts.APIURL);
			strBuilder.append("?");
			strBuilder.append("uid=");
			strBuilder.append(URLEncoder.encode(userName, "UTF-8"));
			strBuilder.append("&pwd=");
			strBuilder.append(password);
			strBuilder.append("&mode=signin");
			// strBuilder.append("&Submit=Submit");

			get("signin", strBuilder.toString(),
					new ExtendedJsonHttpResponseHandler(Consts.API_MODE_LOGIN,
							command));
		} catch (Exception e) {

		}

	}

	public void syncToDevice(String bookID, String paypilNo, String deviceID,
			String otpKey) {
		String action = "syncToDevice";
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(Consts.APIURL2);
		strBuilder.append("?");
		strBuilder.append("paypil_no=");
		strBuilder.append(paypilNo);
		strBuilder.append("&device_id=");
		strBuilder.append(deviceID);
		strBuilder.append("&action=" + action);
		strBuilder.append("&book_id=" + bookID);
		strBuilder.append("&key=");
		strBuilder.append(otpKey);

		Bundle bundle = new Bundle();
		bundle.putString("deviceID", deviceID);
		bundle.putString("bookID", bookID);
		
		get(action, strBuilder.toString(), new ExtendedJsonHttpResponseHandler(
				Consts.API_MODE_SYNCTODEVICE, command, bundle));
	}

	public void getSyncList(String paypilNo, String deviceID, String otpKey) {
		String action = "getSyncList";
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(Consts.APIURL2);
		strBuilder.append("?");
		strBuilder.append("paypil_no=");
		strBuilder.append(paypilNo);
		strBuilder.append("&device_id=");
		strBuilder.append(deviceID);
		strBuilder.append("&action=" + action);
		strBuilder.append("&key=");
		strBuilder.append(otpKey);

		get(action, strBuilder.toString(), new ExtendedJsonHttpResponseHandler(
				Consts.API_MODE_GETSYNCLIST, command));
	}
	
	public void getSyncStatus(String paypilNo,String deviceID,String otpKey,String bookID){
		String action = "getSyncStatus";
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(Consts.APIURL2);
		strBuilder.append("?");
		strBuilder.append("paypil_no=");
		strBuilder.append(paypilNo);
		strBuilder.append("&device_id=");
		strBuilder.append(deviceID);
		strBuilder.append("&action=" + action);
		strBuilder.append("&book_id=" + bookID);
		strBuilder.append("&key=");
		strBuilder.append(otpKey);

		Bundle bundle = new Bundle();
		bundle.putString("deviceID", deviceID);
		bundle.putString("bookID", bookID);
		
		get(action, strBuilder.toString(), new ExtendedJsonHttpResponseHandler(
				Consts.API_MODE_GETSYNCSTATUS, command, bundle));
	}

}
