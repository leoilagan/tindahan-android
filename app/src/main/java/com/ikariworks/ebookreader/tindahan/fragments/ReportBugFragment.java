package com.ikariworks.ebookreader.tindahan.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ikariworks.ebookreader.tindahan.R;


public class ReportBugFragment extends BaseFragment  {
	 OnFragmentSelectListener mListener;
	View localView;
	public static String TAG="REPORTBUGFRAGMENT";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		localView = inflater.inflate(R.layout.fragment_bug, container, false);
		return localView;
	}
	
	
}
