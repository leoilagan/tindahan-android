package com.ikariworks.ebookreader.tindahan.data;

import java.util.ArrayList;

public class Book {

	String bookID;
	String title;
	String author;
	String publisher;
	String image;
	String itemType;
	ArrayList<String> devices;
	int max;
	int syncStatus;
	boolean isSynced;
	boolean isSaved=false;
	String bookUrl;
    String fileName;
	public Book() {
		devices = new ArrayList<String>();
		isSynced = false;
		syncStatus = 0;
		bookUrl = "";
		isSaved = false;
	}

	public void setId(String bookID) {
		this.bookID = bookID;
	}

	public String getId() {
		return bookID;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor() {
		return author;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImage() {
		return image;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemType() {
		return itemType;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getMax() {
		return max;
	}

	public boolean addDevice(String deviceID) {
		if (!devices.contains(deviceID) ){
			devices.add(deviceID);
			return false;
		}
			return true;
	}

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}

	public int getSyncStatus() {
		return syncStatus;
	}

	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}

	public boolean isSaved() {
		return isSaved;
	}

	public boolean isSynced() {
		return isSynced;
	}
	
	public int getNumSyncedDevices(){
		return devices.size();
	}

	public void setSaved(boolean isSaved) {
		this.isSaved = isSaved;
	}
	
	public void setBookUrl(String bookUrl) {
		this.bookUrl = bookUrl;
	}
	public String getBookUrl() {
		return bookUrl;
	}
	/**
	 * Checks if the devices is listed on books listed as synced devices
	 * @param deviceID
	 * @return returns true if device's IMEI is listed on book synced devices otherwise return false
	 */
	public boolean checkDeviceIsSynced(String deviceID){
		if(devices.contains(deviceID))
			return true;
		return false;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileName() {
		return fileName;
	}
}
