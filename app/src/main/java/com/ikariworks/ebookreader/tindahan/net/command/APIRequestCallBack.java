package com.ikariworks.ebookreader.tindahan.net.command;

import org.json.JSONObject;

import android.os.Bundle;


public interface APIRequestCallBack {
	public void onStartRequest(int mode);
	public void onFinishRequest(int mode);
	public void onFailRequest(String message);
	public void onSuccessRequest(int mode,JSONObject jsonObj);
	public void onSuccessRequest(int mode,JSONObject jsonObj,Bundle bundle);
}
