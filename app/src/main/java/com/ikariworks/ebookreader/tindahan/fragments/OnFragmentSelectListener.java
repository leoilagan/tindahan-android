package com.ikariworks.ebookreader.tindahan.fragments;


public interface OnFragmentSelectListener {

	public void selectOnClicked(int id);
	
	public void selectOnItemClicked(String parent,int id,int position);
	
	public void selectOnItemClicked(String parent, String value,int id);
}