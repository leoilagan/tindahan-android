package com.ikariworks.ebookreader.tindahan.ui;


import android.os.Bundle;
import android.util.Log;

import com.actionbarsherlock.app.SherlockActivity;
import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.util.FileUtil;

public class EPubActivity extends SherlockActivity {
	FileUtil fileUtil;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_epubreader);
		fileUtil = new FileUtil(this);
		installSamples();
	}
	
	
	private void debug(String message){
		Log.i("Test",message);
	}
	
	private void installSamples() {
//		if (this.isSampleInstalled()) return; 
        if (!fileUtil.makeDirectory("scripts")) {
        	debug("faild to make scripts directory");
        }
        
        if (!fileUtil.makeDirectory("images")) {
        	debug("faild to make images directory");
        }
        
     //   copyFileToFolder("PagesCenter.png","images");
       // copyFileToFolder("PagesStack.png","images");
       // copyFileToFolder("Pad-Landscape.png","images");
       // copyFileToFolder("Pad-Portrait.png","images");
       // copyFileToFolder("Phone-Landscape.png","images");
       // copyFileToFolder("Phone-Landscape-Double.png","images");
       // copyFileToFolder("Phone-Portrait.png","images");

        
        if (!fileUtil.makeDirectory("downloads")) {
        	debug("faild to make downloads directory");
        }
//        
        if (!fileUtil.makeDirectory("books")) {
        	debug("faild to make books directory");
        }
        
        fileUtil.installBook("Mobydick.epub");    
        fileUtil.installBook("Doctor.epub");
        fileUtil.installBook("LittleBoPeep.epub");
        
     
	}
}
