package com.ikariworks.ebookreader.tindahan.adapter;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.data.Book;
import com.ikariworks.ebookreader.tindahan.net.command.APIRequestCallBack;
import com.ikariworks.ebookreader.tindahan.net.requestor.APIRequester;
import com.ikariworks.ebookreader.tindahan.util.Consts;
import com.ikariworks.ebookreader.tindahan.util.ImageLoader;
import com.ikariworks.ebookreader.tindahan.util.SyncBooks;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CloudBookListAdapter extends BaseAdapter implements
		APIRequestCallBack {

	ArrayList<Book> items;
	Context context;
	ImageLoader imageLoader;

	String paypilNo;
	String otpKey;
	String deviceID;

	APIRequester apirequester;

	public CloudBookListAdapter(ArrayList<Book> items, Context context,
			String paypilNo, String otpKey, String deviceID) {
		this.context = context;
		this.items = items;
		apirequester = new APIRequester(this);
		imageLoader = new ImageLoader(context);
		this.paypilNo = paypilNo;
		this.otpKey = otpKey;
		this.deviceID = deviceID;

	}

	@Override
	public int getCount() {

		return items.size();
	}

	@Override
	public Book getItem(int index) {

		return items.get(index);
	}

	@Override
	public long getItemId(int arg0) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		View layout = convertView;
		if (layout == null) {
			layout = View.inflate(context, R.layout.list_item_sync, null);

		}
		Book bookItem = getItem(position);
		ImageView imageView = (ImageView) layout
				.findViewById(R.id.imgBookCover);
		String imgUrl = bookItem.getImage();
		imageLoader.DisplayImage(imgUrl, imageView, true);
		TextView txt = (TextView) layout.findViewById(R.id.txtAuthor);
		txt.setText(bookItem.getAuthor());
		txt = (TextView) layout.findViewById(R.id.txtPublisher);
		txt.setText(bookItem.getPublisher());
		txt = (TextView) layout.findViewById(R.id.txtTitle);
		txt.setText(bookItem.getTitle());
		txt = (TextView) layout.findViewById(R.id.txtSyncStatus);
		int syncStatus = bookItem.getSyncStatus();
		boolean isDeviceSynced = bookItem.checkDeviceIsSynced(deviceID);
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("Synced to ");
		int numDevices = bookItem.getNumSyncedDevices();
		if (isDeviceSynced && bookItem.isSynced()) {
			strBuilder.append("this device ");
			numDevices = numDevices - 1;
			if (numDevices > 0) {
				strBuilder.append("and ");
				strBuilder.append(numDevices);
				strBuilder.append(" more");
			}
		}else {
			strBuilder.append(numDevices);
			strBuilder.append(" devices");	
		}
		txt.setText(strBuilder.toString());
		if (syncStatus > 0 && syncStatus < 100) {
			txt.setText("Syncing...(" + syncStatus + "%)");
			layout.findViewById(R.id.progressStatus)
					.setVisibility(View.VISIBLE);
			ProgressBar progressBar = (ProgressBar) layout
					.findViewById(R.id.progressStatus);
			progressBar.setProgress(syncStatus);
			layout.findViewById(R.id.btnCancel).setVisibility(View.VISIBLE);
			
		} else {
			layout.findViewById(R.id.progressStatus).setVisibility(View.GONE);
			layout.findViewById(R.id.btnCancel).setVisibility(View.GONE);
			
		}
		

		if (bookItem.isSynced()) {
			layout.findViewById(R.id.imgSynced).setVisibility(View.VISIBLE);
			layout.findViewById(R.id.btnSync).setVisibility(View.GONE);
		} else {
			layout.findViewById(R.id.imgSynced).setVisibility(View.GONE);
			layout.findViewById(R.id.btnSync).setVisibility(View.VISIBLE);
			ImageButton btnSync = (ImageButton) layout
					.findViewById(R.id.btnSync);
			btnSync.setOnClickListener(new OnClickItemButton(position));

		}

		return layout;
	}

	private class OnClickItemButton implements OnClickListener {

		int position;

		public OnClickItemButton(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			String bookID = getItem(position).getId();
			
			if (!getItem(position).checkDeviceIsSynced(deviceID)
					&& getItem(position).getNumSyncedDevices() == 5) {
				messageDialog("Reached the maximum allowable syncing for this book");
			} else
				apirequester.syncToDevice(bookID, paypilNo, deviceID, otpKey);
		}

	}

	private void messageDialog(String message) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Tindahan")
				.setMessage(message)
				.setCancelable(false)
				.setNeutralButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();

							}
						});

		AlertDialog alert = builder.create();
		alert.show();

	}

	@Override
	public void onStartRequest(int mode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinishRequest(int mode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFailRequest(String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj, Bundle bundle) {
		try {
			String jsonString = jsonObj.toString();

			// {"data":[{"item":{"status":true}}]}
			switch (mode) {
			case Consts.API_MODE_SYNCTODEVICE:

				JSONArray jsonArray = jsonObj.getJSONArray("data");
				JSONObject jsonObject = jsonArray.getJSONObject(0);
				JSONObject jsonItem = jsonObject.getJSONObject("item");
				if (jsonItem.has("status")) {
					boolean status = jsonItem.getBoolean("status");
					if (status) {

						String bookID = bundle.getString("bookID");
						SyncBooks.getInstance().getSyncStatus(bookID, paypilNo,
								deviceID, otpKey);
					}
				}
				if (jsonItem.has("error")) {
					String message = jsonItem.getString("error");
					messageDialog(message);
					String bookID = bundle.getString("bookID");
					SyncBooks.getInstance().getSyncStatus(bookID, paypilNo,
							deviceID, otpKey);
				}

				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
