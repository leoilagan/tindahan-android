package com.ikariworks.ebookreader.tindahan.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.ikariworks.ebookreader.tindahan.data.Author;
import com.ikariworks.ebookreader.tindahan.data.Book;
import com.skytree.epub.BookInformation;

public class BookManager {

	public static BookManager instance;

	Map<String, Book> hashBooks;
	Map<String, Author> hashAuthors;
	String currentAuthor;
	ArrayList<BookInformation> arrBooks;
	public static BookManager getInstance() {
		if (instance == null)
			instance = new BookManager();

		return instance;
	}

	public BookManager() {

		hashBooks = new LinkedHashMap<String, Book>();
		hashAuthors = new HashMap<String, Author>();
	}

	public ArrayList<Book> getArryBooks() {
		ArrayList<Book> arryBooks = Collections.list(Collections
				.enumeration(hashBooks.values()));
		return arryBooks;

	}

	public void add(Book book) {
		/*
		 * Iterator<Book> itBook = lnkBooks.values().iterator();
		 * while(itBook.hasNext()){ Book mBook = itBook.next(); }
		 */
		if (!hashBooks.containsKey(book.getId())) {
			hashBooks.put(book.getId(), book);
		}

	}
	
	public void setArrBooks(ArrayList<BookInformation> arrBooks) {
		this.arrBooks = arrBooks;
	}
	
	public ArrayList<Book> getAllCloudBooks(){
		Iterator<Book> itBook = hashBooks.values().iterator();
		ArrayList<Book> arryBooks = new ArrayList<Book>();
		while(itBook.hasNext()){
			Book mBook = itBook.next();
			if(!mBook.isSynced()){
				arryBooks.add(mBook);
			}
		}
		return arryBooks;
	}

	public ArrayList<Author> getAllAuthors() {
		Iterator<Book> itBook = hashBooks.values().iterator();
		while (itBook.hasNext()) {
			Book mBook = itBook.next();
			if(mBook.isSynced() || mBook.getSyncStatus()>0){
				String authorName = mBook.getAuthor();
				Author author = hashAuthors.get(authorName);
				if(author == null){
					author = new Author();
				}
				author.setName(authorName);
				author.addBook(mBook);
				hashAuthors.put(authorName, author);	
			}
			
			
		}
		
		ArrayList<Author> arryAuthors = Collections.list(Collections
				.enumeration(hashAuthors.values()));
		return arryAuthors;
	}

	
	public ArrayList<Book> getAllSyncedBooks(){
		ArrayList<Book> arryBook = new ArrayList<Book>();
		Iterator<Book> itBook = hashBooks.values().iterator();
		while(itBook.hasNext()){
			Book mBook = itBook.next();
			if(mBook.isSynced())
				arryBook.add(mBook);
		}
	
		return arryBook;
	}
	
	public ArrayList<Book> getAllSyncedBooks1(){
		ArrayList<Book> arryBook = new ArrayList<Book>();
		Iterator<Book> itBook = hashBooks.values().iterator();
		while(itBook.hasNext()){
			Book mBook = itBook.next();
			if(mBook.isSynced() || mBook.getSyncStatus()>0)
				arryBook.add(mBook);
		}
	
		return arryBook;
	}
	
	public ArrayList<Book> getBookByAuthor(String name){
		ArrayList<Book> arryBook = new ArrayList<Book>();
		Iterator<Book> itBook = hashBooks.values().iterator();
		while(itBook.hasNext()){
			Book mBook = itBook.next();
			String mAuthorName = mBook.getAuthor();
			if(mAuthorName.equals(name)){
				arryBook.add(mBook);
			}
		}
	
		return arryBook;
	}
	public void updateBookSyncStatus(String bookID,int status, String url, boolean isSynced){
		Book book = hashBooks.get(bookID);
		if(book!=null){
			book.setSyncStatus(status);
			book.setBookUrl(url);
			book.setSynced(isSynced);
		}
	}
	
	public void updateBookSaveStatus(String fileName,String bookID, boolean isSaved){
		Book book = hashBooks.get(bookID);
		if(book!=null){
			book.setSaved(isSaved);
			book.setFileName(fileName);
			hashBooks.put(bookID, book);
		}
	}
	
	
	public void addBookDevice(String bookID,String deviceID){
		Book book  = hashBooks.get(bookID);
		if(book!=null){
			book.addDevice(deviceID);
		}
	}
	
	
	public BookInformation getBookInformation(String title){
		if(arrBooks!=null && !arrBooks.isEmpty()){
			int size = arrBooks.size();
			for(int i=0; i<size;i++){
				BookInformation bi = arrBooks.get(i);
			//	Log.i("Test","bi="+bi.fileName+" fileName="+title);
				if(bi.fileName.equals(title)){
				//	Log.i("Test","index"+i);
					return bi;
				}
			}
		}
		return null;
	}
	
	public void setCurrentAuthor(String currentAuthor) {
		this.currentAuthor = currentAuthor;
	}
	public String getCurrentAuthor() {
		return currentAuthor;
	}
}
