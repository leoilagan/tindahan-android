package com.ikariworks.ebookreader.tindahan.adapter;

import java.util.ArrayList;

import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.data.Book;
import com.ikariworks.ebookreader.tindahan.util.ImageLoader;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BookItemAdapter extends BaseAdapter {

	ArrayList<Book> items;
	Context context;
	ImageLoader imageLoader;

	public BookItemAdapter(ArrayList<Book> items, Context context) {
		this.context = context;
		this.items = items;
		imageLoader = new ImageLoader(context);
		
	}

	@Override
	public int getCount() {

		return items.size();
	}

	@Override
	public Book getItem(int index) {

		return items.get(index);
	}

	@Override
	public long getItemId(int arg0) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		View layout = convertView;
		if (layout == null) {
			layout = View.inflate(context,
					R.layout.fragment_home_recent_book_item, null);
		}

		ImageView imageView = (ImageView) layout
				.findViewById(R.id.imgBookCover);
		String imgUrl = getItem(position).getImage();
		imageLoader.DisplayImage(imgUrl, imageView, true);

		return layout;
	}

}
