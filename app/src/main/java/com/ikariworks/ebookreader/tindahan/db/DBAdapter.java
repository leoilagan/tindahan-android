package com.ikariworks.ebookreader.tindahan.db;


import com.ikariworks.ebookreader.tindahan.data.Book;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;


public class DBAdapter {

	Context context;
	ContentResolver content;
	
	public DBAdapter(Context context) {
	     this.context = context;
	     this.content = context.getContentResolver();
	}
	
	
	public void insertBooks(Book book){
		ContentValues values = new ContentValues();
		values.put("bookId",book.getId());
		values.put("title", book.getTitle());
		values.put("author", book.getAuthor());
		values.put("imgUrl", book.getImage());
		values.put("publisher", book.getPublisher());
		values.put("itemType", book.getItemType());
		values.put("max", book.getMax());
		
		content.insert(DBContentProvider.BOOK_URI, values);
	}
	
	
	
	public void insertBook(Book book, String bookID){
		
		ContentValues values = new ContentValues();
		values.put("bookId", bookID);
		String where = "bookId=?";
		String[] selectionArgs = { book.getId() };
		
		
		String selection = "bookId='" + bookID + "'";
		boolean isExisted = true;

		Cursor cursor = content.query(DBContentProvider.BOOK_URI, selectionArgs,
				selection, null, null);
		if (!cursor.moveToFirst()) {
			insertBooks(book);
			isExisted = false;
		}

		cursor.close();

	
	}
	

}
