package com.ikariworks.ebookreader.tindahan.fragments;


import android.content.Intent;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragment;
import com.ikariworks.ebookreader.tindahan.data.Book;
import com.ikariworks.ebookreader.tindahan.managers.BookManager;
import com.ikariworks.ebookreader.tindahan.ui.BookViewActivity;
import com.ikariworks.ebookreader.tindahan.ui.MagazineActivity;
import com.ikariworks.ebookreader.tindahan.util.DownloadFileAsync;
import com.skytree.epub.BookInformation;

public class BaseFragment extends SherlockFragment {
	 OnFragmentSelectListener mListener;

	


	 public void startBookView(BookInformation bi, String itemType) {
			Intent intent;
			//Log.i("Test","itemType="+itemType);
			//Log.i("Test",bi.title);
			if (itemType.equalsIgnoreCase("magazine")) {
				intent = new Intent(getActivity(), MagazineActivity.class);
			} else {
				intent = new Intent(getActivity(), BookViewActivity.class);
			}
			intent.putExtra("BOOKCODE", bi.bookCode);
			intent.putExtra("TITLE", bi.title);
			intent.putExtra("AUTHOR", bi.creator);
			intent.putExtra("BOOKNAME", bi.fileName);
			intent.putExtra("POSITION", bi.position);
			intent.putExtra("transitionType", 2);
			getActivity().startActivity(intent);
		}
	 
	 public void bookViewing(Book mBooks){
			String url = mBooks.getBookUrl();
			if (!url.equals("")) {

				if (!mBooks.isSaved())
					new DownloadFileAsync(getActivity(), mBooks).execute(url);
				else {
					BookInformation bi = BookManager.getInstance()
							.getBookInformation(mBooks.getFileName());
					if (bi != null) {
						startBookView(bi, mBooks.getItemType());
					}
					// messageDialog("ebook reader should start here");
				}

			}
		}
}
