package com.ikariworks.ebookreader.tindahan.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.ikariworks.ebookreader.tindahan.data.Book;
import com.ikariworks.ebookreader.tindahan.managers.BookManager;
import com.ikariworks.ebookreader.tindahan.util.SkySetting;
import com.skytree.epub.BookInformation;
import com.skytree.epub.Highlight;
import com.skytree.epub.Highlights;
import com.skytree.epub.PageInformation;
import com.skytree.epub.ReflowableControl;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class SQLHelper {

	Context context;
	DatabaseHelper dbHelper;
	private SQLiteDatabase db;

	/**
	 * SIDE NOTE execSQL vs rawQuery if you want to execute something in
	 * database without concerning its output (e.g create/alter tables), then
	 * use execSQL, but if you are expecting some results in return against your
	 * query (e.g. select records) then use rawQuery
	 * 
	 */

	public SQLHelper(Context context) {
		this.context = context;
		dbHelper = new DatabaseHelper(context);
		open();
	}

	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}

	public void close() {
		db.close();
		dbHelper.close();
	}

	private Cursor executeSQL(String stmt, String[] columns) {
		// open();

		Cursor cursor = db.rawQuery(stmt, columns);
		if (cursor.moveToFirst()) {

		}

		//db.close();
		// close();

		return cursor;
	}

	public Cursor sampleSQL(String parameter) {

		String sqlStmt = "";
		return executeSQL(sqlStmt, new String[] { parameter });
	}

	public void updateSetting(SkySetting setting) {
		String sql = String
				.format("UPDATE Setting SET FontName='%s', FontSize=%d , LineSpacing=%d , Foreground=%d , Background=%d , Theme=%d , Brightness=%f, TransitionType=%d where BookCode=0",
						setting.fontName, setting.fontSize,
						setting.lineSpacing, setting.foreground,
						setting.background, setting.theme, setting.brightness,
						setting.transitionType);
		db.execSQL(sql);
	}

	public SkySetting fetchSetting() {
		String sql = "SELECT * FROM Setting where BookCode=0";
		Cursor result = db.rawQuery(sql, null);
		if (result.moveToFirst()) {
			SkySetting setting = new SkySetting();
			setting.bookCode = result.getInt(0);
			setting.fontName = result.getString(1);
			setting.fontSize = result.getInt(2);
			setting.lineSpacing = result.getInt(3);
			setting.foreground = result.getInt(4);
			setting.background = result.getInt(5);
			setting.theme = result.getInt(6);
			setting.brightness = result.getDouble(7);
			setting.transitionType = result.getInt(8);
			result.close();
			return setting;
		}
		result.close();
		return null;
	}

	public String getDateString() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	public void insertBookmark(PageInformation pi) {
		double ppb = pi.pagePositionInBook;
		double ppc = pi.pagePositionInChapter;
		int ci = pi.chapterIndex;
		int bc = pi.bookCode;
		String dateInString = this.getDateString();
		String sql = String
				.format("INSERT INTO Bookmark (BookCode,ChapterIndex,PagePositionInChapter,PagePositionInBook,CreatedDate) VALUES(%d,%d,%f,%f,'%s')",
						bc, ci, ppc, ppb, dateInString);
		db.execSQL(sql);
	}

	public void deleteBookmarkByCode(int code) {
		String sql = String
				.format("DELETE FROM Bookmark where Code = %d", code);
		db.execSQL(sql);
	}

	public void deleteBookmark(PageInformation pi) {
		int code = pi.code;
		this.deleteBookmarkByCode(code);
	}

	public int getBookmarkCode(PageInformation pi) {
		double pageDelta = 1.0f / pi.numberOfPagesInChapter;
		double target = pi.pagePositionInChapter;
		int bookCode = pi.bookCode;
		String selectSql = String
				.format("SELECT Code,PagePositionInChapter from Bookmark where BookCode=%d and ChapterIndex=%d",
						bookCode, pi.chapterIndex);
		Cursor cursor = db.rawQuery(selectSql, null);
		while (cursor.moveToNext()) {
			double ppc = cursor.getDouble(1);
			int code = cursor.getInt(0);
			if (target >= (ppc - pageDelta / 2)
					&& target <= (ppc + pageDelta / 2.0f)) {
				cursor.close();
				return code;
			}
		}
		cursor.close();
		return -1;
	}

	/**
	 * CREATE TABLE IF NOT EXISTS Bookmark ( BookCode INTEGER NOT NULL, Code
	 * INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT NOT NULL, ChapterIndex INTEGER,
	 * PagePositionInChapter REAL, PagePositionInBook REAL, Description TEXT,
	 * CreatedDate TEXT );
	 */

	public ArrayList<PageInformation> fetchBookmarks(int bookCode) {
		ArrayList<PageInformation> pis = new ArrayList<PageInformation>();
		String selectSql = String
				.format("SELECT* from Bookmark where bookCode=%d ORDER BY ChapterIndex",
						bookCode);
		Cursor cursor = db.rawQuery(selectSql, null);
		while (cursor.moveToNext()) {
			PageInformation pi = new PageInformation();
			pi.bookCode = cursor.getInt(0);
			pi.code = cursor.getInt(1);
			pi.chapterIndex = cursor.getInt(2);
			pi.pagePositionInChapter = cursor.getDouble(3);
			pi.pagePositionInBook = cursor.getDouble(4);
			pi.datetime = cursor.getString(6);
			pis.add(pi);
		}
		cursor.close();
		return pis;

	}

	public void toggleBookmark(PageInformation pi) {
		int code = this.getBookmarkCode(pi);
		if (code == -1) { // if not exist
			this.insertBookmark(pi);
		} else {
			this.deleteBookmarkByCode(code); // if exist, delete it
		}
	}

	public boolean isBookmarked(PageInformation pi) {
		int code = this.getBookmarkCode(pi);
		if (code == -1) {
			return false;
		} else {
			return true;
		}
	}

	public void updatePosition(int bookCode, double position) {
		String sql = String.format(
				"UPDATE Book SET Position=%f where BookCode=%d", position,
				bookCode);
		db.execSQL(sql);
	}

	public void insertBook(BookInformation bi) {

		String sql = "INSERT INTO Book (Title,Author,Publisher,Subject,Type,Date,Language,Filename,IsFixedLayout) VALUES('"
				+ bi.title
				+ "','"
				+ bi.creator
				+ "','"
				+ bi.publisher
				+ "','"
				+ bi.subject
				+ "','"
				+ bi.type
				+ "','"
				+ bi.date
				+ "','"
				+ bi.language
				+ "','"
				+ bi.fileName
				+ "',"
				+ (bi.isFixedLayout ? 1 : 0) + ")";
		db.execSQL(sql);
		
		Log.w("EPub", sql);
	}

	public ArrayList<BookInformation> fetchBookInformations() {
		return fetch("SELECT * from Book",
				new IForEach<Cursor, ArrayList<BookInformation>>() {
					public void apply(Cursor cursor,
							ArrayList<BookInformation> bis) {
						BookInformation bi = new BookInformation();
						bi.bookCode = cursor.getInt(0);
						bi.title = cursor.getString(1);
						bi.creator = cursor.getString(2);
						bi.publisher = cursor.getString(3);
						bi.subject = cursor.getString(4);
						bi.type = cursor.getString(5);
						bi.date = cursor.getString(6);
						bi.language = cursor.getString(7);
						bi.fileName = cursor.getString(8);
						bi.position = cursor.getDouble(9);
						bi.isFixedLayout = cursor.getInt(10) != 0;
						bis.add(bi);
					}
				});
	}

	
	
	
	
	
	public ArrayList<BookInformation> fetch(String sql,
			IForEach<Cursor, ArrayList<BookInformation>> f) {
		ArrayList<BookInformation> bis = new ArrayList<BookInformation>();
		Cursor cursor = db.rawQuery(sql, null);
		while (cursor.moveToNext()) {
			f.apply(cursor, bis);
		}
		cursor.close();
		return bis;
	}
	
	
	public BookInformation fetchBookInformation(String bookTitle) {
		BookInformation bi = null;
		
		String where = "Title=?";
		String[] args = { bookTitle };

		

		Cursor cursor = db.query("Book", null, where, args,
				null, null, null);

		if (!cursor.moveToFirst()) {
			bi =new BookInformation();
			bi.bookCode = cursor.getInt(0);
			bi.title = cursor.getString(1);
			bi.creator = cursor.getString(2);
			bi.publisher = cursor.getString(3);
			bi.subject = cursor.getString(4);
			bi.type = cursor.getString(5);
			bi.date = cursor.getString(6);
			bi.language = cursor.getString(7);
			bi.fileName = cursor.getString(8);
			bi.position = cursor.getDouble(9);
			bi.isFixedLayout = cursor.getInt(10) != 0;
			
		}
		cursor.close();
		return bi;
	}
	

	public Highlights fetchHighlights(int bookCode, int chapterIndex) {
		Highlights results = new Highlights();
		String selectSql = String
				.format("SELECT * FROM Highlight where BookCode=%d and ChapterIndex=%d ORDER BY ChapterIndex",
						bookCode, chapterIndex);
		Cursor cursor = db.rawQuery(selectSql, null);
		while (cursor.moveToNext()) {
			Highlight highlight = new Highlight();
			highlight.bookCode = bookCode;
			highlight.code = cursor.getInt(1);
			highlight.chapterIndex = chapterIndex;
			highlight.startIndex = cursor.getInt(3);
			highlight.startOffset = cursor.getInt(4);
			highlight.endIndex = cursor.getInt(5);
			highlight.endOffset = cursor.getInt(6);
			highlight.color = cursor.getInt(7);
			highlight.text = cursor.getString(8);
			highlight.note = cursor.getString(9);
			highlight.isNote = cursor.getInt(10) != 0;
			highlight.datetime = cursor.getString(11);
			results.addHighlight(highlight);

		}
		cursor.close();
		return results;
	}

	public Highlights fetchAllHighlights(int bookCode) {
		Highlights results = new Highlights();
		String selectSql = String
				.format("SELECT * FROM Highlight where BookCode=%d ORDER BY ChapterIndex",
						bookCode);
		Cursor cursor = db.rawQuery(selectSql, null);
		while (cursor.moveToNext()) {
			Highlight highlight = new Highlight();
			highlight.bookCode = bookCode;
			highlight.code = cursor.getInt(1);
			highlight.chapterIndex = cursor.getInt(2);
			highlight.startIndex = cursor.getInt(3);
			highlight.startOffset = cursor.getInt(4);
			highlight.endIndex = cursor.getInt(5);
			highlight.endOffset = cursor.getInt(6);
			highlight.color = cursor.getInt(7);
			highlight.text = cursor.getString(8);
			highlight.note = cursor.getString(9);
			highlight.isNote = cursor.getInt(10) != 0;
			highlight.datetime = cursor.getString(11);
			results.addHighlight(highlight);
		}
		cursor.close();
		return results;
	}

	public void insertHighlight(Highlight highlight) {
		String dateString = this.getDateString();
		String sql = String
				.format("INSERT INTO Highlight (BookCode,ChapterIndex,StartIndex,StartOffset,EndIndex,EndOffset,Color,Text,Note,IsNote,CreatedDate) VALUES(%d,%d,%d,%d,%d,%d,%d,'%s','%s',%d,'%s')",
						highlight.bookCode, highlight.chapterIndex,
						highlight.startIndex, highlight.startOffset,
						highlight.endIndex, highlight.endOffset,
						highlight.color, highlight.text, highlight.note,
						highlight.isNote ? 1 : 0, dateString);
		db.execSQL(sql);

		Log.w("EPub", sql);
	}

	public void deleteHighlight(Highlight highlight) {
		String sql = String
				.format("DELETE FROM Highlight where BookCode=%d and ChapterIndex=%d and StartIndex=%d and StartOffset=%d and EndIndex=%d and EndOffset=%d",
						highlight.bookCode, highlight.chapterIndex,
						highlight.startIndex, highlight.startOffset,
						highlight.endIndex, highlight.endOffset);
		db.execSQL(sql);
		Log.w("EPub", sql);
	}

	public void deleteHighlightByCode(int code) {
		String sql = String.format("DELETE FROM Highlight where Code=%d", code);
		db.execSQL(sql);
		Log.w("EPub", sql);
	}

	// Update is 1 Based
	public void updateHighlight(Highlight highlight) {
		String sql = String
				.format("UPDATE Highlight SET StartIndex=%d,StartOffset=%d,EndIndex=%d,EndOffset=%d,Color=%d,Text='%s',Note='%s',IsNote=%d where BookCode=%d and ChapterIndex=%d and StartIndex=%d and StartOffset=%d and EndIndex=%d and EndOffset=%d",
						highlight.startIndex, highlight.startOffset,
						highlight.endIndex, highlight.endOffset,
						highlight.color, highlight.text, highlight.note,
						highlight.isNote ? 1 : 0, highlight.bookCode,
						highlight.chapterIndex, highlight.startIndex,
						highlight.startOffset, highlight.endIndex,
						highlight.endOffset);
		db.execSQL(sql);

		Log.w("EPub", sql);
	}



	public void fetchAllTinBooks() {

		String selectSql = "SELECT * from " + DBTables.BOOK_TABLE +" order by _id ASC";
		Cursor cursor = db.rawQuery(selectSql, null);
		while (cursor.moveToNext()) {
			Book book = new Book();
			book.setId(cursor.getString(1));
			book.setTitle(cursor.getString(2));
			
			book.setAuthor(cursor.getString(3));
			book.setPublisher(cursor.getString(4));
			book.setImage(cursor.getString(5));
			book.setItemType(cursor.getString(6));
			book.setMax(cursor.getInt(7));
			book.setSyncStatus(cursor.getInt(8));
			int val = cursor.getInt(9);
			boolean valbool = false;
			if (val == 1) {
				valbool = true;
			}
			
			book.setSynced(valbool);
			
			val = cursor.getInt(10);
			valbool = false;
			if (val == 1) {
				valbool = true;
			}
			book.setSaved(valbool);
			
			book.setBookUrl(cursor.getString(11));
			book.setFileName(cursor.getString(12));
			BookManager.getInstance().add(book);
		}
		cursor.close();

	}

	public void fetchAllUniqueAuthor() {
		String selectSql = "select author,count(author) as book_count from tin_book group by author";
		Cursor cursor = db.rawQuery(selectSql, null);

	}

	public boolean insertTinBook(Book book, String bookID) {
		String[] columns = { "bookId" };
		String where = "bookId=?";
		String[] args = { bookID };

		boolean isExisted = true;

		Cursor cursor = db.query(DBTables.BOOK_TABLE, columns, where, args,
				null, null, null);

		if (!cursor.moveToFirst()) {
			insertTinBook(book);
			isExisted = false;
			
		}

		cursor.close();
		return isExisted;
	}

	
	
	// insert tindahan sync cloud book to db
	public void insertTinBook(Book book) {
		ContentValues values = new ContentValues();
		values.put("bookId", book.getId());
		values.put("title", book.getTitle());
		values.put("author", book.getAuthor());
		values.put("imgUrl", book.getImage());
		values.put("publisher", book.getPublisher());
		values.put("itemType", book.getItemType());
		values.put("max", book.getMax());

		db.insert(DBTables.BOOK_TABLE, null, values);
	}

	
	/**
	 * 
	 * @param bookID
	 * @param status
	 * @param url
	 * @param isSaved no boolean on SQLite so we are using 1 as true 0 as false
	 */
	public void updateSyncBook(String bookID,int status,String url,int isSync){
		String whereClause ="bookId=?";
		String[] whereArgs = {bookID};
		ContentValues values = new ContentValues();
		values.put("syncStatus", status);
		values.put("isSynced", isSync);
		values.put("bookUrl",url);
		db.update(DBTables.BOOK_TABLE, values, whereClause, whereArgs);
	}
	
	
	public void updateSyncBook(int isSaved,String bookID,String fileName){
		String whereClause ="bookId=?";
		String[] whereArgs = {bookID};
		ContentValues values = new ContentValues();
		
		values.put("isSaved",isSaved);
		values.put("fileName", fileName);
		db.update(DBTables.BOOK_TABLE, values, whereClause, whereArgs);
	}
	
	
	public void insertTinBookDevice(String deviceID, String bookID) {
		String[] columns ={"bookId","deviceID"};
		String where = "bookId=? AND deviceID=?";
		String[] args={bookID,deviceID};
		
		Cursor cursor = db.query(DBTables.BOOKDEVICE_TABLE, columns, where, args,
				null, null, null);
		if (!cursor.moveToFirst()) {
			ContentValues values = new ContentValues();
			values.put("deviceID", deviceID);
			values.put("bookId", bookID);
			db.insert(DBTables.BOOKDEVICE_TABLE, null, values);	
		}
		cursor.close();
	}
}
