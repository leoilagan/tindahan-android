package com.ikariworks.ebookreader.tindahan.net.action;

public interface ISampleListener extends IBaseListener {

	
	public void onSampleStart();
	public void onSampleFinish();
	
}
