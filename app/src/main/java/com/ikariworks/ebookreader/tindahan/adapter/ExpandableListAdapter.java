package com.ikariworks.ebookreader.tindahan.adapter;

import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.ui.MainActivity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	String[] groupNames;
	MainActivity context;

	public ExpandableListAdapter(MainActivity context, String[] groupNames) {
		this.groupNames = groupNames;
		this.context = context;
	}

	@Override
	public Object getChild(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean arg2, View convertView, ViewGroup arg4) {
		View layout = convertView;
			if (groupPosition == 0) {
				layout = View.inflate(context, R.layout.frag_pay_method1, null);
			} else if (groupPosition == 1) {
				layout = View.inflate(context, R.layout.frag_pay_method2, null);
			} else if (groupPosition == 2) {
				layout = View.inflate(context, R.layout.frag_pay_method3, null);
				Button btn = (Button)layout.findViewById(R.id.btnSelectCountry);
				btn.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
					 context.showSelectCountryDlg();
						
					}
				});
				
			}
	
		
		
		return layout;
	}

	@Override
	public int getChildrenCount(int index) {

		return 1;
	}

	@Override
	public String getGroup(int index) {
		// TODO Auto-generated method stub
		return this.groupNames[index];
	}

	@Override
	public int getGroupCount() {

		return this.groupNames.length;
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int position, boolean arg1, View convertView,
			ViewGroup arg3) {
		View layout = convertView;
		if (layout == null) {
			layout = View.inflate(context, R.layout.list_item, null);
		}
		String string = getGroup(position);
		TextView txt = (TextView) layout.findViewById(R.id.lst_item);
		txt.setText(string);
		return layout;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {

		return true;
	}

}
