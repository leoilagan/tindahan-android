package com.ikariworks.ebookreader.tindahan.net.extended;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;

import com.ikariworks.ebookreader.tindahan.net.command.APIRequestCallBack;
import com.ikariworks.ebookreader.tindahan.util.Consts;
import com.loopj.android.http.JsonHttpResponseHandler;

public class ExtendedJsonHttpResponseHandler extends JsonHttpResponseHandler {
	private APIRequestCallBack command;
	int mode;
	Bundle bundle;

	public ExtendedJsonHttpResponseHandler(int mode, APIRequestCallBack command) {
		this.command = command;
		this.mode = mode;
	}

	public ExtendedJsonHttpResponseHandler(int mode,
			APIRequestCallBack command, Bundle bundle) {
		this.command = command;
		this.mode = mode;
		this.bundle = bundle;
	}

	public void onFailure(Throwable e, JSONArray response) {

		command.onFailRequest("client: " + e.getMessage());
	}

	public void onFailure(Throwable e, JSONObject response) {

		command.onFailRequest("client: " + e.getMessage());
	}

	public void onFinish() {

		command.onFinishRequest(this.mode);
	}

	public void onStart() {

		command.onStartRequest(this.mode);
	}

	public void onSuccess(JSONArray response) {

		command.onFailRequest("server: invalid response");
	}

	@Override
	public void onSuccess(JSONObject arg0) {
		if (mode == Consts.API_MODE_SYNCTODEVICE
				|| mode == Consts.API_MODE_GETSYNCSTATUS)
			command.onSuccessRequest(mode, arg0, bundle);
		else
			command.onSuccessRequest(mode, arg0);
	}

	/*
	 * public void onSuccess(JSONObject messageJson) {
	 * 
	 * Log.v(command.getName(), "onSuccess"); try { String type =
	 * messageJson.getString("type");
	 * 
	 * if (type.equals("success")) { JSONObject subscriberJson =
	 * messageJson.getJSONObject("entity"); Subscriber subscriber =
	 * Json2Domain.toSubscriber(subscriberJson); command.onSuccess(subscriber);
	 * } else if (type.equals("error")) { String message =
	 * messageJson.getString("message"); command.onFail("server: " + message); }
	 * else { command.onFail("server: invalid response"); } } catch
	 * (JSONException e) { command.onFail("client: " + e.getMessage()); } catch
	 * (Exception e) { command.onFail("client: " + e.getMessage()); } }
	 */
}
