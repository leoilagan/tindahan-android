package com.ikariworks.ebookreader.tindahan.fragments;

import java.util.ArrayList;

import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.adapter.BookItemAdapter;
import com.ikariworks.ebookreader.tindahan.adapter.BookTitleListAdapter;
import com.ikariworks.ebookreader.tindahan.data.Book;
import com.ikariworks.ebookreader.tindahan.managers.BookManager;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ViewFlipper;

public class BookListFragment extends BaseFragment implements
		RadioGroup.OnCheckedChangeListener, OnItemClickListener {
	View localView;
	OnFragmentSelectListener mListener;
	GridView gridView;
	public static String TAG = "BOOKLISTFRAGMENT";
	BookItemAdapter adapter;
	ListView listBookTitles;
	ViewFlipper viewFlipper;
	RadioGroup radioGrp;
	ArrayList<Book> arryBook ;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		localView = inflater.inflate(R.layout.fragment_booklist, container,
				false);
		viewFlipper = (ViewFlipper) localView
				.findViewById(R.id.viewFlipContainer);

		gridView = (GridView) localView.findViewById(R.id.gridBooks);
		gridView.setOnItemClickListener(this);
		listBookTitles = (ListView) localView.findViewById(R.id.lstTitle);
		listBookTitles.setOnItemClickListener(this);
		String author = BookManager.getInstance().getCurrentAuthor();
		if (author != null) {
			 arryBook = BookManager.getInstance()
					.getBookByAuthor(author);
			adapter = new BookItemAdapter(arryBook, getActivity());
			gridView.setAdapter(adapter);
			listBookTitles.setAdapter(new BookTitleListAdapter(arryBook,
					getActivity()));

		}
		radioGrp = (RadioGroup) localView.findViewById(R.id.pnlBookListButtons);
		radioGrp.setOnCheckedChangeListener(this);

		return localView;
	}

	static BookListFragment instance;

	public static BookListFragment getInstance() {
		if (instance == null)
			instance = new BookListFragment();
		return instance;
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		try {
			mListener = (OnFragmentSelectListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentSelectListener");
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.btnRecent:
			viewFlipper.setDisplayedChild(0);
			break;
		case R.id.btnTitle:
			viewFlipper.setDisplayedChild(1);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position,
			long arg3) {

		int id = adapter.getId();
		switch (id) {
		case R.id.lstTitle:
		case R.id.gridBooks:
			Book mBooks =arryBook.get(position);

			bookViewing(mBooks);
			break;

		}

	}
}
