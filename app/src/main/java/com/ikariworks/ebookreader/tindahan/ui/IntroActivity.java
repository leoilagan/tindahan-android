package com.ikariworks.ebookreader.tindahan.ui;

import java.util.Locale;

import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.util.PreferenceKeys;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class IntroActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);
		RadioGroup radioGrp = (RadioGroup) findViewById(R.id.pnlLocaleSelection);

		String currentLocale = PreferenceKeys.getInstance(this).getStringData(
				"languagePref","en");
	

		if (currentLocale.equals("en")) {
			RadioButton radionBtn = (RadioButton) radioGrp.getChildAt(0);
			radionBtn.setChecked(true);
		} else {
			RadioButton radionBtn = (RadioButton) radioGrp.getChildAt(1);
			radionBtn.setChecked(true);
		}

		radioGrp.setOnCheckedChangeListener(toggleListener);
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.btnProceed:
			finish();
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			break;
		}

	}

	String currentLocale = "en";
	RadioGroup.OnCheckedChangeListener toggleListener = new RadioGroup.OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(final RadioGroup radioGroup, final int i) {
			for (int j = 0; j < radioGroup.getChildCount(); j++) {
				final RadioButton view = (RadioButton) radioGroup.getChildAt(j);
				if (view.getId() == i) {

					if (view.getText().equals("Filipino")) {
						currentLocale = "tl";

					} else if (view.getText().equals("English")) {
						currentLocale = "en";

					}
				}

			}

			PreferenceKeys.getInstance(getBaseContext()).setData(
					"languagePref", currentLocale);
			Configuration config = getBaseContext().getResources()
					.getConfiguration();
			Locale locale = new Locale(currentLocale);
			Locale.setDefault(locale);
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
			refresh();

		}

	};

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Configuration config = getBaseContext().getResources()
				.getConfiguration();
		// refresh your views here
		Locale locale = new Locale(currentLocale);
		Locale.setDefault(locale);
		config.locale = locale;
		super.onConfigurationChanged(newConfig);
	}

	private void refresh() {
		finish();
		Intent myIntent = new Intent(IntroActivity.this, IntroActivity.class);
		startActivity(myIntent);
	}

}
