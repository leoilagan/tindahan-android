package com.ikariworks.ebookreader.tindahan.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

	
	
	private static final String DATABASE_NAME = "Tindahan.db";

	private static int DATABASE_VERSION = 1;
	DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		
		db.execSQL(DBTables.CREATE_BOOK_TABLE);
		db.execSQL(DBTables.CREATE_DEVICE_TABLE);
		db.execSQL(DBTables.CREATE_BOOKDEVICE_TABLE);
		
		//skyepub table creation
		db.execSQL(DBTables.CREATE_SKYEPUB_BOOK);
		db.execSQL(DBTables.CREATE_SKYEPUB_BOOKMARK);
		db.execSQL(DBTables.CREATE_SKYEPUB_HIGHLIGHT);
		db.execSQL(DBTables.CREATE_SKYEPUB_PAGING);
		db.execSQL(DBTables.CREATE_SKYEPUB_SETTING);
		//insert default setting for ereader
		String sql = "INSERT INTO Setting(BookCode,FontName,FontSize,LineSpacing,Foreground,Background,Theme,Brightness,TransitionType) VALUES(0,'Times New Roman',2,-1,-1,-1,0,1,0)";
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + DBTables.BOOK_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + DBTables.DEVICE_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + DBTables.BOOKDEVICE_TABLE);
		Log.w("Test", "Upgrading database from " + oldVersion + " to "
				+ newVersion);
		onCreate(db);
	}
}
