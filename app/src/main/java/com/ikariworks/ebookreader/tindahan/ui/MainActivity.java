package com.ikariworks.ebookreader.tindahan.ui;

import java.util.ArrayList;
import java.util.Locale;

import com.ikariworks.ebookreader.tindahan.R;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.ikariworks.ebookreader.tindahan.db.SQLHelper;
import com.ikariworks.ebookreader.tindahan.fragments.BookListFragment;
import com.ikariworks.ebookreader.tindahan.fragments.FAQFragment;
import com.ikariworks.ebookreader.tindahan.fragments.MainFragment;
import com.ikariworks.ebookreader.tindahan.fragments.ReportBugFragment;
import com.ikariworks.ebookreader.tindahan.fragments.SyncFragment;
import com.ikariworks.ebookreader.tindahan.util.MViewPager;
import com.ikariworks.ebookreader.tindahan.util.PreferenceKeys;
import com.ikariworks.ebookreader.tindahan.util.Util;
import com.ikariworks.ebookreader.tindahan.fragments.OnFragmentSelectListener;
import com.ikariworks.ebookreader.tindahan.managers.BookManager;

import android.os.Bundle;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;

public class MainActivity extends SherlockFragmentActivity implements
		OnClickListener, ActionBar.TabListener, OnFragmentSelectListener {

	private Locale locale = null;
	ArrayList<ImageButton> btnMenuItems;
	int imgBtnIndexes[] = { R.id.btnHome, R.id.btnFAQ, R.id.btnSync,
			R.id.btnLogout, R.id.btnReportBug };
	MainFragment mainFragment;
	FAQFragment faqFragment;
	ReportBugFragment rptBugfragment;
	SyncFragment syncFragment;
	BookListFragment booklstFragment;
	MViewPager mViewPager;
	String currentTag;
	SQLHelper sqlHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		if (getSupportActionBar().isShowing())
			getSupportActionBar().hide();
		Configuration config = getBaseContext().getResources()
				.getConfiguration();

		String lang = PreferenceKeys.getInstance(this).getData("languagePref");

		if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
			locale = new Locale(lang);
			Locale.setDefault(locale);
			config.locale = locale;
			getResources().updateConfiguration(config,
					getResources().getDisplayMetrics());
		}
		setContentView(R.layout.activity_main);
		sqlHelper = new SQLHelper(this);
		sqlHelper.fetchAllTinBooks();
		btnMenuItems = new ArrayList<ImageButton>();
		mViewPager = (MViewPager) findViewById(R.id.container);

		mViewPager
				.setAdapter(new HomePagerAdapter(getSupportFragmentManager()));

		final ActionBar actionBar = getSupportActionBar();

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		actionBar.addTab(actionBar.newTab().setTabListener(this));
		actionBar.addTab(actionBar.newTab().setTabListener(this));
		actionBar.addTab(actionBar.newTab().setTabListener(this));
		actionBar.addTab(actionBar.newTab().setTabListener(this));
		actionBar.addTab(actionBar.newTab().setTabListener(this));
		initContent();
		BookManager.getInstance().setArrBooks(sqlHelper.fetchBookInformations());
	}

	private void initContent() {
		int size = imgBtnIndexes.length;
		for (int i = 0; i < size; i++) {
			ImageButton btn = (ImageButton) findViewById(imgBtnIndexes[i]);
			btn.setOnClickListener(this);
			btn.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.blackbg));

			btn.setEnabled(true);
			btnMenuItems.add(btn);

		}

		setMenuItemPressed(R.id.btnHome);
		// showFragment(R.id.container, mainFragment, MainFragment.TAG, null,
		// false);
	}

	private void setCurrentFragment(String currentFragment) {
		this.currentTag = currentFragment;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (locale != null) {
			newConfig.locale = locale;
			Locale.setDefault(locale);
			getResources().updateConfiguration(newConfig,
					getResources().getDisplayMetrics());
			setContentView(R.layout.activity_main);
		}
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.btnHome:
			setMenuItemPressed(id);
			mViewPager.setCurrentItem(0, false);
			setCurrentFragment(MainFragment.TAG);
			findViewById(R.id.pnltabheaderWithBack).setVisibility(View.GONE);

			break;
		case R.id.btnFAQ:
			setMenuItemPressed(id);
			mViewPager.setCurrentItem(1, false);
			showHeaderTitle("FAQ", false, false);
			setCurrentFragment(FAQFragment.TAG);
			break;
		case R.id.btnSync:
			setMenuItemPressed(id);
			mViewPager.setCurrentItem(2, false);
			setCurrentFragment(SyncFragment.TAG);
			showHeaderTitle(
					getResources().getString(R.string.fragment_sync_title),
					false, false);
			break;
		case R.id.btnLogout:

			findViewById(R.id.pnlLogout).setVisibility(View.VISIBLE);

			break;
		case R.id.btnReportBug:
			setMenuItemPressed(id);
			mViewPager.setCurrentItem(3, false);
			showHeaderTitle(
					getResources().getString(R.string.fragment_reportbug_title),
					true, true);
			setCurrentFragment(ReportBugFragment.TAG);
			break;
		case R.id.btnLeftButton:
			leftBtnClicked();
			break;
		case R.id.btnRightButton:

			break;
		case R.id.btnCloseLogout:
		case R.id.btnLogoNo:
			findViewById(R.id.pnlLogout).setVisibility(View.GONE);

			break;
		case R.id.btnLogoutYes:
			Util.getInstance().logout(PreferenceKeys.getInstance(this));
			finish();
			Intent introIntent = new Intent(this, IntroActivity.class);
			startActivity(introIntent);
			break;
		}

	}

	private void leftBtnClicked() {
				switch (mViewPager.getCurrentItem()) {
		case 1:
			FragmentPagerAdapter adapter = (FragmentPagerAdapter) mViewPager
					.getAdapter();
			FAQFragment fragment = (FAQFragment) adapter.getItem(mViewPager
					.getCurrentItem());
			fragment.backToHome();
			showHeaderTitle("FAQ", false, false);
			break;
		}
	}

	private void setMenuItemPressed(int id) {
		int size = btnMenuItems.size();
		for (int i = 0; i < size; i++) {
			if (id == btnMenuItems.get(i).getId()) {
				btnMenuItems.get(i).setBackgroundDrawable(
						getResources().getDrawable(R.drawable.blackbgpressed));

				btnMenuItems.get(i).setEnabled(false);
			} else {
				btnMenuItems.get(i).setBackgroundDrawable(
						getResources().getDrawable(R.drawable.blackbg));

				btnMenuItems.get(i).setEnabled(true);
			}

		}
	}

	//

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition(), false);
		// mViewPager.get

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	private class HomePagerAdapter extends FragmentPagerAdapter {
		public HomePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				if (mainFragment != null)
					return mainFragment;
				return (mainFragment = new MainFragment());

			case 1:
				if (faqFragment != null)
					return faqFragment;

				return (faqFragment = new FAQFragment());

			case 2:
				if (syncFragment != null)
					return syncFragment;

				return (syncFragment = new SyncFragment());

			case 3:
				if (rptBugfragment != null)
					return rptBugfragment;

				return (rptBugfragment = new ReportBugFragment());
			case 4:

				if (booklstFragment != null)
					return booklstFragment;
				return booklstFragment = new BookListFragment();

			}
			return null;
		}

		@Override
		public int getCount() {
			return 5;
		}
	}

	@Override
	public void selectOnClicked(int id) {
		switch (id) {
		case R.id.btnGetBook:
			mViewPager.setCurrentItem(1, false);
			setMenuItemPressed(R.id.btnFAQ);
			showHeaderTitle("FAQ", false, false);
			break;
		}

	}

	@Override
	public void selectOnItemClicked(String parent, int id, int position) {
		if (parent.equals(FAQFragment.TAG)) {
			setCurrentFragment(FAQFragment.TAG);
			switch (id) {
			case R.id.lstView:
				TextView txtView = (TextView) findViewById(R.id.txtScreenTitle);
				findViewById(R.id.btnLeftButton).setVisibility(View.VISIBLE);
				if (position == 0) {
					txtView.setText(getResources().getString(
							R.string.fragment_faq_getbook_title));

				} else {
					txtView.setText(getResources().getString(
							R.string.fragment_faq_payment_method_title));

				}
				break;
			}
		}

	}

	private void showHeaderTitle(String screenTitle, boolean showLeftButton,
			boolean showRightButton) {
		TextView txtView = (TextView) findViewById(R.id.txtScreenTitle);
		txtView.setText(screenTitle);
		findViewById(R.id.pnltabheaderWithBack).setVisibility(View.VISIBLE);
		if (showLeftButton) {
			findViewById(R.id.btnLeftButton).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.btnLeftButton).setVisibility(View.GONE);
		}

		if (showRightButton) {
			findViewById(R.id.btnRightButton).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.btnRightButton).setVisibility(View.GONE);
		}
	}

	public void showSelectCountryDlg() {
		final Dialog dlg = new Dialog(this);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.setContentView(R.layout.dialog_list_contries);
		dlg.setCancelable(false);
		ListView lstCountries = (ListView) dlg.findViewById(R.id.lstCountries);
		lstCountries.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		String[] listContent = Util.getInstance().getAllCountries();
		ArrayAdapter<String> adapter

		= new ArrayAdapter<String>(this,

		android.R.layout.simple_list_item_1,

		listContent);

		lstCountries.setAdapter(adapter);
		lstCountries.setCacheColorHint(Color.TRANSPARENT);

		Button btn = (Button) dlg.findViewById(R.id.btnCancel);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dlg.dismiss();

			}
		});
		btn = (Button) dlg.findViewById(R.id.btnDone);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dlg.dismiss();

			}
		});
		dlg.show();

	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
	
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		sqlHelper.close();
	}
	
	
	@Override
	public void selectOnItemClicked(String parent, String value, int id) {
		if (parent.equals(MainFragment.TAG)) {
			switch (id) {
			case R.id.lstAuthors:
				setCurrentFragment(BookListFragment.TAG);
				FragmentPagerAdapter adapter = (FragmentPagerAdapter) mViewPager
						.getAdapter();
				BookListFragment fragment = (BookListFragment) adapter
						.getItem(4);

				BookManager.getInstance().setCurrentAuthor(value);

				mViewPager.setCurrentItem(4, false);
				showHeaderTitle(value,
						false, false);
				break;
			}
		}

	}
}
