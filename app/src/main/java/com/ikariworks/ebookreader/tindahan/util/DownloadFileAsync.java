package com.ikariworks.ebookreader.tindahan.util;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import com.ikariworks.ebookreader.tindahan.data.Book;
import com.ikariworks.ebookreader.tindahan.db.SQLHelper;
import com.ikariworks.ebookreader.tindahan.managers.BookManager;
import com.ikariworks.ebookreader.tindahan.skyreader.ContentHandler;
import com.ikariworks.ebookreader.tindahan.ui.BookViewActivity;
import com.ikariworks.ebookreader.tindahan.ui.MagazineActivity;
import com.skytree.epub.BookInformation;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;


public class DownloadFileAsync extends AsyncTask<String, Integer, String> {

	//Context context;
	ProgressDialog dialog;
	SQLHelper sqlHelper;
	String bookID;
	Activity activity;
	Book bookItem;
	public DownloadFileAsync(Activity activity, Book bookItem) {
		this.activity = activity;
		
		this.bookItem = bookItem;
		dialog = new ProgressDialog(activity);
		Util.getInstance().makeDirectory(activity,"downloads");
		Util.getInstance().makeDirectory(activity,"books");
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setMessage("Downloading");
		dialog.show();
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		int progressStatus = values[0];
		dialog.setProgress(progressStatus);
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		unzipBook(result);
	}

	
	
	
	@Override
	protected String doInBackground(String... aurl) {
		int count;
		String fileName = aurl[0].substring(aurl[0].lastIndexOf('/') + 1);
		try {
			URL url = new URL(aurl[0]);
			URLConnection conexion = url.openConnection();
			conexion.connect();
			int lenghtOfFile = conexion.getContentLength();
			InputStream input = new BufferedInputStream(url.openStream());
			String path = activity.getFilesDir().getAbsolutePath()
					+ "/downloads/" + fileName;

			FileOutputStream output = new FileOutputStream(path);
			byte data[] = new byte[1024];
			long total = 0;
			while ((count = input.read(data)) != -1) {
				total += count;
				dialog.setProgress((int) ((total * 100) / lenghtOfFile));
				output.write(data, 0, count);

			}
			output.flush();
			output.close();
			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}

	class UnzipHandler implements Observer {
		@Override
		public void update(Observable observable, Object data) {
			
		    sqlHelper = new SQLHelper(activity);
			String fileName = (String)data;
	        String baseDirectory = activity.getFilesDir() + "/books";
	        ContentHandler contentListener = new ContentHandler();
	        BookInformation bi = new BookInformation(fileName,baseDirectory,contentListener);		
	        sqlHelper.updateSyncBook(1, bookID,fileName);
	        BookManager.getInstance().updateBookSaveStatus(fileName,bookItem.getId(), true);
	        BookManager.getInstance().setArrBooks(sqlHelper.fetchBookInformations());
	        
	        //messageDialog("epub downloaded successfully , ready for reading...");
	        sqlHelper.insertBook(bi);
	        startBookView(bi, bookItem.getItemType());
		}		
	}
	
	
	public void startBookView(BookInformation bi, String itemType) {
		Intent intent;
		
	
	    if(itemType.equalsIgnoreCase("magazine")){
	    	intent = new Intent(activity,MagazineActivity.class);
	    }else {
	    	intent = new Intent(activity,BookViewActivity.class);
	    }
		intent.putExtra("BOOKCODE",bi.bookCode);
		intent.putExtra("TITLE",bi.title);
		intent.putExtra("AUTHOR", bi.creator);
		intent.putExtra("BOOKNAME",bi.fileName);
		intent.putExtra("POSITION", bi.position);
		intent.putExtra("transitionType",2);
		activity.startActivity(intent);
	}
	
	public void unzipBook(String fileName) {
	 
		String targetDir = new String(activity.getFilesDir().getAbsolutePath() + "/books/" + fileName);
		targetDir = Util.removeExtention(targetDir);
		String filePath = new String(activity.getFilesDir().getAbsolutePath() + "/downloads/");

	    Unzip unzip = new Unzip(fileName, filePath, targetDir);
	    unzip.addObserver(new UnzipHandler());
	    unzip.unzip();	
	    
	}
	private void messageDialog(String message) {

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Tindahan")
				.setMessage(message)
				.setCancelable(false)
				.setNeutralButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();

							}
						});

		AlertDialog alert = builder.create();
		alert.show();

	}
	
}
