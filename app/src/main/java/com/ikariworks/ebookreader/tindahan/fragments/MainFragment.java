package com.ikariworks.ebookreader.tindahan.fragments;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.ikariworks.ebookreader.tindahan.R;
import com.ikariworks.ebookreader.tindahan.adapter.AuthorItemAdapter;
import com.ikariworks.ebookreader.tindahan.adapter.BookItemAdapter;
import com.ikariworks.ebookreader.tindahan.adapter.BookTitleListAdapter;
import com.ikariworks.ebookreader.tindahan.adapter.CloudBookListAdapter;
import com.ikariworks.ebookreader.tindahan.data.Author;
import com.ikariworks.ebookreader.tindahan.data.Book;
import com.ikariworks.ebookreader.tindahan.db.DBAdapter;
import com.ikariworks.ebookreader.tindahan.db.SQLHelper;
import com.ikariworks.ebookreader.tindahan.managers.BookManager;
import com.ikariworks.ebookreader.tindahan.net.command.APIRequestCallBack;
import com.ikariworks.ebookreader.tindahan.net.requestor.APIRequester;
import com.ikariworks.ebookreader.tindahan.ui.BookViewActivity;
import com.ikariworks.ebookreader.tindahan.ui.EPubActivity;
import com.ikariworks.ebookreader.tindahan.ui.MagazineActivity;
import com.ikariworks.ebookreader.tindahan.util.Consts;
import com.ikariworks.ebookreader.tindahan.util.DownloadFileAsync;
import com.ikariworks.ebookreader.tindahan.util.PreferenceKeys;
import com.ikariworks.ebookreader.tindahan.util.SyncBooks;
import com.ikariworks.ebookreader.tindahan.util.Util;
import com.skytree.epub.BookInformation;

public class MainFragment extends BaseFragment implements OnClickListener,
		OnItemClickListener, RadioGroup.OnCheckedChangeListener, Observer,
		APIRequestCallBack {
	View localView;
	OnFragmentSelectListener mListener;
	public static String TAG = "MAINFRAGMENT";

	PreferenceKeys prefs;
	GridView gridView;
	ListView lstAuthors, lstBooks;
	ViewFlipper viewFlipper;
	RadioGroup radioGrp;
	ArrayList<Author> arryAuthor;
	APIRequester apirequester;
	String paypilNo;
	String otpKey;
	String deviceID;
	SQLHelper sqlHelper;
	Button btnGetBook;
	TextView txt;
	ArrayList<Book> arryBook;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		prefs = PreferenceKeys.getInstance(getActivity());
		apirequester = new APIRequester(this);
		paypilNo = prefs.getData("paypilNo");
		deviceID = Util.getInstance().getIMEI(getActivity());
		otpKey = prefs.getData("otpKey");

		sqlHelper = new SQLHelper(getActivity());
		SyncBooks.getInstance().addObserver(this);

		apirequester.getSyncList(paypilNo, deviceID, otpKey);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		localView = inflater.inflate(R.layout.fragment_home, container, false);
		btnGetBook = (Button) localView.findViewById(R.id.btnGetBook);
		btnGetBook.setOnClickListener(this);
		txt = (TextView) localView.findViewById(R.id.txtNoBook);

		viewFlipper = (ViewFlipper) localView
				.findViewById(R.id.viewFlipContainer);

		gridView = (GridView) localView.findViewById(R.id.gridBooks);

		arryBook = BookManager.getInstance().getAllSyncedBooks1();

		gridView.setAdapter(new BookItemAdapter(arryBook, getActivity()));
		gridView.setOnItemClickListener(this);

		lstAuthors = (ListView) localView.findViewById(R.id.lstAuthors);
		arryAuthor = BookManager.getInstance().getAllAuthors();
		lstAuthors.setAdapter(new AuthorItemAdapter(arryAuthor, getActivity()));
		lstAuthors.setOnItemClickListener(this);

		lstBooks = (ListView) localView.findViewById(R.id.lstTitle);
		lstBooks.setAdapter(new BookTitleListAdapter(arryBook, getActivity()));
		lstBooks.setOnItemClickListener(this);

		radioGrp = (RadioGroup) localView.findViewById(R.id.pnlLocaleSelection);
		radioGrp.setOnCheckedChangeListener(this);

		int var = View.VISIBLE;
		if (arryBook.size() != 0) {
			var = View.GONE;
		}
		btnGetBook.setVisibility(var);

		txt.setVisibility(var);

		return localView;
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		try {
			mListener = (OnFragmentSelectListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentSelectListener");
		}
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.btnGetBook:
			mListener.selectOnClicked(id);
			break;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position,
			long arg3) {

		int id = adapter.getId();
		switch (id) {
		case R.id.lstTitle:
		case R.id.gridBooks:
			Book mBooks = BookManager.getInstance().getAllSyncedBooks1()
					.get(position);
			
			bookViewing(mBooks);
			break;
		case R.id.lstAuthors:
			String authorname = arryAuthor.get(position).getName();
			mListener.selectOnItemClicked(TAG, authorname, R.id.lstAuthors);
			break;
		
			

		}

	}

	
	
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.btnRecent:
			viewFlipper.setDisplayedChild(0);
			break;
		case R.id.btnAuthor:
			viewFlipper.setDisplayedChild(1);
			break;
		case R.id.btnTitle:
			viewFlipper.setDisplayedChild(2);
			break;

		}
	}

	@Override
	public void update(Observable observable, Object data) {
		arryBook = BookManager.getInstance().getAllSyncedBooks1();
		gridView.setAdapter(new BookItemAdapter(arryBook, getActivity()));
		arryAuthor = BookManager.getInstance().getAllAuthors();
		lstAuthors.setAdapter(new AuthorItemAdapter(arryAuthor, getActivity()));
		lstBooks.setAdapter(new BookTitleListAdapter(arryBook, getActivity()));

		int var = View.VISIBLE;
		if (arryBook.size() != 0) {
			var = View.GONE;
		}
		btnGetBook.setVisibility(var);

		txt.setVisibility(var);
	}

	@Override
	public void onStartRequest(int mode) {

	}

	@Override
	public void onFinishRequest(int mode) {

	}

	@Override
	public void onFailRequest(String message) {

	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj) {
		try {
			//Log.i("Test", "onSuccessRequest=" + jsonObj.toString());
			switch (mode) {
			case Consts.API_MODE_GETSYNCLIST:
				JSONArray jsonArryData = jsonObj.getJSONArray("data");
				int size = jsonArryData.length();
				for (int i = 0; i < size; i++) {
					JSONObject jsonParent = jsonArryData.getJSONObject(i);
					JSONObject jsonItem = jsonParent.getJSONObject("item");
					Book book = new Book();
					book.setId(jsonItem.getString("bookId"));
					book.setTitle(jsonItem.getString("title"));
					book.setAuthor(jsonItem.getString("author"));
					book.setPublisher(jsonItem.getString("publisher"));
					book.setImage(jsonItem.getString("imgUrl"));
					book.setItemType(jsonItem.getString("itemType"));
					book.setMax(jsonItem.getInt("max"));
					JSONArray jsonDevices = jsonItem.getJSONArray("devices");
					for (int j = 0; j < jsonDevices.length(); j++) {
						String deviceID = jsonDevices.getString(j);

						boolean isExisted = book.addDevice(deviceID);

						if (!isExisted) {
							sqlHelper.insertTinBookDevice(book.getId(),
									deviceID);
						}
					}
					boolean isExisted = sqlHelper.insertTinBook(book,
							book.getId());
					if (!isExisted)
						BookManager.getInstance().add(book);

				}
				SyncBooks.getInstance().forceUpdate();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Override
	public void onSuccessRequest(int mode, JSONObject jsonObj, Bundle bundle) {

	}

	private void messageDialog(String message) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Tindahan")
				.setMessage(message)
				.setCancelable(false)
				.setNeutralButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();

							}
						});

		AlertDialog alert = builder.create();
		alert.show();

	}

	

}
